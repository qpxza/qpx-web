const path = require('path');

module.exports = {
  entry: {
    "js/accounts": './src/js/apps/accounts/index.js',
    "js/dashboard": './src/js/apps/dashboard/index.js',
    "img/favicon": './src/assets/img/favicon.ico',
    "img/logo": './src/assets/img/logo.png',
  },
  output: {
    path: path.resolve(__dirname, 'static'),
    publicPath: '',
  },
  mode: 'production',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          filename: 'js/[name].js'
        }
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(ico|png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: 'img/[name].[ext]',
        },
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  plugins: [],
};
