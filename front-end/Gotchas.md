### 1. ErrorMessages component

When displaying validation errors, ensure that the enclosing SemanticUI react Form has the 'error' prop set to true
