import React, { useState, useEffect, Fragment } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import {
  Checkbox,
  Icon,
  Progress,
  Table,
}
  from 'semantic-ui-react'
import axios from '../../utils/axios';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';

export const ViewCompletedReports = () => {

  const { id } = useParams();

  const [makeRequest, report, loading, error] = useRequest();

  useEffect(() => makeRequest({ url: `/api/v1/reports/${id}/` }), [null]);

  return (
    report &&
    <>
      <Table color={'purple'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell colSpan={3}>{report.name}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell>Branch: {report.branch?.name}</Table.Cell>
            <Table.Cell>Date: {report.date}</Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>

      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Item</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
            <Table.HeaderCell>Value</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>

          {report?.items?.map((s, index) => {
            return (
              <Table.Row key={index} active>
                <Table.Cell>{s.name}</Table.Cell>
                <Table.Cell>-</Table.Cell>
                <Table.Cell>{s.value}</Table.Cell>
              </Table.Row>
            )
          })}
        </Table.Body>
      </Table>
      <ErrorMessages error={error} />
    </>
  )
}
