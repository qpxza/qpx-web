import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Icon, Table, Pagination, } from 'semantic-ui-react';
import axios from '../../utils/axios';
import { MobileView } from '../common';

export const ReportsHome = (props) => {

  const [state, setState] = useState({
    data: null,
    activePage: 1
  });

  useEffect(() => {
    axios.get(`/api/v1/reports/?page=${state.activePage}`)
      .then((response) => {
        setState(prevState => ({
          ...prevState,
          data: response
        }));
      })
      .catch(function (error) {
        alert(error);
      });
  }, [
    state.activePage,
  ]);

  const history = useHistory();

  const getRows = () => {

    const response = state?.data;

    let rows = [];

    if (props.screenSize === 'desktop') {

      for (const item of response?.data?.results || []) {

        rows.push(

          <Table.Row key={item.id}>
            <Table.Cell>{item.name}</Table.Cell>
            <Table.Cell>
              <Link to={`/branches/${item.branch.id}/`} target="_blank">{item.branch.name}</Link>
            </Table.Cell>
            <Table.Cell>{new Date(item.created_at).toISOString().substring(0, 10)}</Table.Cell>
            <Table.Cell>{new Date(item.updated_at).toISOString().substring(0, 10)}</Table.Cell>
            <Table.Cell collapsing>
              <Button.Group
                size={'tiny'}>
                <Button
                  icon
                  color='green'
                  as={Link}
                  to={`/reports/${item.id}/`}>

                  <Icon name='eye' />
                </Button>
                <Button
                  icon
                  color='blue'
                  as={Link}
                  to={`/reports/${item.id}/edit`}>

                  <Icon name='pencil' />
                </Button>
                <Button
                  size={'tiny'}
                  color='red'
                  icon='times'
                  onClick={() => deleteReport(item)} />
              </Button.Group>
            </Table.Cell>
          </Table.Row>

        );
      }

      return rows;
    }

    for (const item of response?.data?.results || []) {

      rows.push({
        heading: item.name,
        items: [{
          icon: 'code branch',
          content: <Link to={`/branches/${item.branch.id}/`} target="_blank">{item.branch.name}</Link>
        }, {
          icon: 'calendar',
          content: `Date Created: ${new Date(item.created_at).toISOString().substring(0, 10)}`
        }, {
          icon: 'calendar',
          content: `Date Updated: ${new Date(item.updated_at).toISOString().substring(0, 10)}`
        }],
        buttons: [{
          color: 'green',
          content: 'View',
          icon: 'eye',
          link: `/reports/${item.id}/`
        }, {
          color: 'blue',
          content: 'Edit',
          icon: 'pencil',
          link: `/reports/${item.id}/edit`
        }]
      });
    }

    return rows;
  }

  const rows = getRows();

  const deleteReport = (checklistItem) => {

    axios.delete(`/api/v1/reports/${checklistItem.id}/`)
      .then(() => window.location.reload())
      .catch(function (error) {
        alert(error);
      });
  }

  return (
    <>
      {props.screenSize === 'desktop' ?
        <Table color={'purple'} celled>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Report Name</Table.HeaderCell>
              <Table.HeaderCell>Branch</Table.HeaderCell>
              <Table.HeaderCell>Created On</Table.HeaderCell>
              <Table.HeaderCell>Last Updated</Table.HeaderCell>
              <Table.HeaderCell>Actions</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {rows}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell>
                <Button
                  as={Link}
                  to='/reports/create'
                  color='green'
                  size='tiny'
                  icon='upload'
                  content='Upload new' />
              </Table.HeaderCell>
              <Table.HeaderCell colSpan='5'>
                <Pagination
                  size='tiny'
                  floated='right'
                  activePage={state.activePage}
                  ellipsisItem={null}
                  siblingRange={2}
                  totalPages={(state?.data?.data && Math.ceil(state.data.data.count / 10)) || 1}
                  onPageChange={(event, data) => setState(prevState => ({
                    ...prevState,
                    activePage: data.activePage
                  }))} />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table >

        : <MobileView
          heading={'Reports'}
          list={rows} />
      }
    </>
  );
}
