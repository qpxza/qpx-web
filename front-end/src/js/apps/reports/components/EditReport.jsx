import _, { get, update } from 'lodash';
import React, { useState, useEffect, Fragment } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Form, Input, Button, Icon, Table, Select } from 'semantic-ui-react'
import { ErrorMessages } from '../../common/ErrorMessages';
import axios from '../../../utils/axios';
import { useRequest } from '../../../utils';

export const EditReport = () => {

  const { id } = useParams();

  const [getReport] = useRequest();

  const [getBranches] = useRequest();

  const [report, setReport] = useState({ items: [] });

  const [branches, setBranches] = useState([]);

  const history = useHistory();

  const [updateReport, _report, loading, error] = useRequest();


  useEffect(() => {
    getReport({ url: `/api/v1/reports/${id}/` })
      .then((res) => res.successful && setReport(res.data));
    getBranches({ url: '/api/v1/branches/' })
      .then((res) => res.successful && setBranches(formatBranches(res.data?.results || [])));
  }, [null]);

  const formatBranches = (branches) => (branches.map((b => ({
    key: b.id,
    text: b.name,
    value: b.id
  }))));

  const addItem = () => {


    setReport({
      ...report,
      items: report.items.concat([{
        name: '',
        value: 0,
        date: new Date().toISOString().split('T')[0],
      }])
    });

  }

  const removeItem = (item) => {

    setReport({
      ...report,
      items: report.items.filter(it => it !== item),
    });
  }

  const submit = () => {

    if (!report.branch) {
      return alert(`You need to select a branch first.`);
    }


    for (const item of report.items) {

      if (!item.name || isNaN(item.value)) {
        alert(`You can't submit an empty property.`);
        return;
      }
    }

    const params = {
      ...report,
      date: new Date().toISOString().split('T')[0],
    };

    updateReport({ url: `/api/v1/reports/${id}/`, method: 'PUT', data: params })
      .then((res) => res.successful && history.push('/reports/'))
  }

  return (
    <Form size='tiny' error={!!error} loading={loading}>
      <Table color={'purple'} compact>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Add Items To Report</Table.HeaderCell>
            <Table.HeaderCell>
              <Form.Group widths='equal'>
                <Form.Field
                  control={Input}
                  value={report?.name || ''}
                  onChange={((event, data) => setReport({ ...report, name: data.value }))} />
                <Form.Field
                  control={Select}
                  options={branches}
                  placeholder='Select branch'
                  value={report?.branch}
                  onChange={((event, data) => setReport({ ...report, branch: data.value }))} />
              </Form.Group>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        {report &&

          <Table.Body>
            {
              report.items.map((item, i) => (
                <Table.Row key={i}>
                  <Table.Cell colSpan='2'>
                    <Form.Group widths='equal'>
                      <Form.Field
                        control={Input}
                        placeholder='Property name'
                        value={item.name}
                        error={!item.name}
                        onChange={(event, data) => {
                          item.name = data.value;

                          setReport({
                            ...report,
                            items: report.items.map(item => item),
                          });
                        }} />
                      <Form.Field
                        control={Input}
                        placeholder='Property value'
                        value={item.value}
                        onChange={(event, data) => {
                          item.value = data.value;

                          setReport({
                            ...report,
                            items: report.items.map(item => item),
                          });
                        }}
                        action={{
                          color: 'red',
                          icon: 'times',
                          title: 'Remove',
                          size: 'tiny',
                          onClick: () => removeItem(item)
                        }}
                      />
                    </Form.Group>
                  </Table.Cell>
                </Table.Row>
              ))
            }
          </Table.Body>
        }


        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan='4'>
              <Button
                floated='left'
                icon
                labelPosition='left'
                primary
                size='tiny'
                onClick={addItem}>
                <Icon name='plus' /> Add Field
					    </Button>
              <Button
                floated='right'
                icon
                color={'green'}
                labelPosition='left'
                size='tiny'
                onClick={submit}>
                <Icon name='save' /> Submit
					</Button>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <ErrorMessages error={error} />
    </Form>
  );
};
