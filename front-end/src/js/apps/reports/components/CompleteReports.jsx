import React, { useState, useEffect, Fragment } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import { useDropzone } from 'react-dropzone';
import { Form, Input, Button, Icon, Table, Select } from 'semantic-ui-react'
import axios from '../../../utils/axios';
import _ from 'lodash';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export const CompleteReport = () => {

  const [state, setState] = useState({
    name: '',
    items: [],
    selectedBranch: null
  });

  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    onDrop: files => {
      if (!files || files.length == 0) {
        return;
      }

      const payload = new FormData();

      payload.append('report', files[0]);
      axios.post('/api/v1/reports/parse/', payload, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
        .then((res) => {
          setState(prevState => ({
            ...prevState,
            name: res.data?.name || '',
            items: state.items.concat((res.data?.items || []))
          }))
        })
        .catch((err) => {
          console.log(err);
        })
    }
  });

  const id = useQuery().get('template_id');

  useEffect(() => {

    async function getTemplate() {
      axios.get(`/api/v1/reports/templates/${id}`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            name: response.data.name || '',
            items: response.data.items.map(key => ({ name: key, value: 0 })),
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    if (id) {
      getTemplate();
    }

    async function getBranches() {

      axios.get('/api/v1/branches/')
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            branches: response
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranches();

  }, []);

  const history = useHistory();

  const getBranches = () => {

    const branches = [];

    if (state?.branches?.data?.results) {

      for (const branch of state?.branches?.data?.results) {

        branches.push({
          key: branch.id,
          text: branch.name,
          value: branch.id
        });
      }
    }

    return branches;
  }

  const addItem = (item) => {
    setState(prevState => ({
      ...prevState,
      items: state.items.concat(item || { name: '', value: '' }),
    }));
  };

  const removeItem = (item) => {
    setState(prevState => ({
      ...prevState,
      items: state.items.filter(i => i != item),
    }));
  };

  const updateValue = (item, key, value) => {
    item[key] = value;

    setState(prevState => ({
      ...prevState,
      items: state.items.map(i => i),
    }));
  }

  const submitData = () => {

    if (!state.selectedBranch) {
      return alert(`You need to select a branch first.`);
    }

    for (const item of state.items) {
      if (!item.name) {
        alert(`You can't submit an empty property.`);
        return;
      }
    }

    const params = {
      name: state.name,
      date: new Date().toISOString().split('T')[0],
      branch: state.selectedBranch,
      items: state.items,
    };

    axios.post('/api/v1/reports/', params)
      .then(() => {
        history.push('/reports/');
      })
      .catch(function (error) {
        alert(error);
      });
  }


  return (
    <Form size='tiny'>
      <Table color={'purple'} compact>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell colSpan='4'>
              Add Items To Report
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          <Table.Row>
            <Table.Cell colSpan='2'>
              <Form.Field
                fluid
                required={true}
                control={Input}
                placeholder='Report name'
                value={state.name}
                onChange={(event, data) => {
                  setState(prevState => ({
                    ...prevState,
                    name: data.value
                  }))
                }} />
            </Table.Cell>
            <Table.Cell colSpan='2'>
              <Form.Field
                required={true}
                control={Select}
                options={getBranches()}
                placeholder='Branch'
                onChange={(event, data) => {
                  setState(prevState => ({
                    ...prevState,
                    selectedBranch: data.value
                  }))
                }} />
            </Table.Cell>
          </Table.Row>
          {
            state.items.map((item, i) => (
              <Table.Row key={i}>
                <Table.Cell colSpan='3'>
                  <Form.Group widths='equal'>
                    <Form.Field
                      required={true}
                      control={Input}
                      placeholder='Property name'
                      value={item.name}
                      onChange={(event, data) => updateValue(item, 'name', data.value)} />

                    <Form.Field
                      required={true}
                      control={Input}
                      placeholder='Property value'
                      value={item.value}
                      onChange={(event, data) => updateValue(item, 'value', data.value)}
                      action={{
                        color: 'red',
                        icon: 'times',
                        title: 'Remove',
                        onClick: () => removeItem(item)
                      }} />
                  </Form.Group>
                </Table.Cell>
              </Table.Row>
            )
            )
          }
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan='2'>
              <Button
                icon='plus'
                color={'blue'}
                labelPosition='left'
                size='tiny'
                colSpan='2'
                content='Add item'
                onClick={(event) => addItem()} />
            </Table.HeaderCell>
            <Table.HeaderCell colSpan='2'>
              <Button
                floated='right'
                icon
                color={'green'}
                labelPosition='left'
                size='tiny'
                content='Submit'
                onClick={submitData} />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <div {...getRootProps({ className: 'dropzone' })}>
        <input {...getInputProps()} />
        <p>Drag and drop your King POS daily report file, or click to select the file and upload</p>
      </div>
    </Form>
  )
}
