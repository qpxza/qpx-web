import React from 'react'
import { Form, Button, Icon, Table, Segment, Input, Select } from 'semantic-ui-react'

export const Items = (props) => {

  const dropDownOptions = [
    { key: 1, text: 'Range', value: 'range' },
    { key: 2, text: 'Checkbox', value: 'checkbox' },
  ]

  return (
    <Table.Row>
      <Table.Cell>
        <Form.Field
          required={true}
          control={Input}
          placeholder='Property name'
          value={props?.item?.name && props.item.name}
          onChange={(event, data) => props.onPropertyValueChanged({ name: data.value })} />
      </Table.Cell>
      <Table.Cell >
        <Button
          icon
          color='red'
          size='tiny'
          onClick={() => props.onRemoveClick(props.item)}>
          <Icon name='trash alternate outline' />
        </Button>
      </Table.Cell>
    </Table.Row>
  );
}

