import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { Button, Table, Pagination, } from 'semantic-ui-react';
import axios from '../../utils/axios';
import { MobileView } from '../common';

export const ReportsTemplateHome = (props) => {

  const [state, setState] = useState({
    data: null,
    activePage: 1
  });

  useEffect(() => {
    axios.get(`/api/v1/reports/templates/?page=${state.activePage}`)
      .then((response) => {
        setState(prevState => ({
          ...prevState,
          data: response
        }));
      })
      .catch(function (error) {
        alert(error);
      });
  }, [
    state.activePage,
  ]);

  const history = useHistory();

  const getRows = () => {

    const response = state?.data;

    let rows = [];

    if (props.screenSize === 'desktop') {

      for (const item of response?.data?.results || []) {

        rows.push(

          <Table.Row key={item.id}>
            <Table.Cell>{item.name}</Table.Cell>
            <Table.Cell>{new Date(item.created_at).toISOString().substring(0, 10)}</Table.Cell>
            <Table.Cell>{new Date(item.updated_at).toISOString().substring(0, 10)}</Table.Cell>
            <Table.Cell collapsing>
              <Button.Group
                size='tiny'>
                <Button
                  as={Link}
                  to={`/reports/templates/${item.id}/edit`}
                  icon='pencil'
                  color='blue' />
                <Button
                  as={Link}
                  to={`/reports/create?template_id=${item.id}`}
                  icon='check'
                  color='green'
                  title='Complete report' />
                <Button
                  color='red'
                  icon='times'
                  onClick={() => deleteChecklistItem(item)} />
              </Button.Group>
            </Table.Cell>
          </Table.Row>

        );
      }

      return rows;
    }

    for (const item of response?.data?.results || []) {

      rows.push({
        heading: item.name,
        items: [{
          icon: 'calendar',
          content: `Date Created: ${new Date(item.created_at).toISOString().substring(0, 10)}`
        }, {
          icon: 'calendar',
          content: `Date Updated: ${new Date(item.updated_at).toISOString().substring(0, 10)}`
        }],
        buttons: [{
          color: 'green',
          content: 'View',
          icon: 'eye',
          link: `/reports/create?template_id=${item.id}`
        }, {
          color: 'blue',
          content: 'Edit',
          icon: 'pencil',
          link: `/reports/templates/${item.id}/edit`
        }]
      });
    }

    return rows;
  }

  const rows = getRows();

  const deleteChecklistItem = (checklistItem) => {

    axios.delete(`/api/v1/reports/templates/${checklistItem.id}/`)
      .then(() => window.location.reload())
      .catch(function (error) {
        alert(error);
      });
  }

  return (
    <>
      {props.screenSize === 'desktop' ?
        <Table color={'purple'} celled>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Report Name</Table.HeaderCell>
              <Table.HeaderCell>Created On</Table.HeaderCell>
              <Table.HeaderCell>Last Updated</Table.HeaderCell>
              <Table.HeaderCell>Action</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {rows}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan='1'>
                <Button
                  size='tiny'
                  color='teal'
                  content='Create New Template'
                  icon='add'
                  labelPosition='left'
                  onClick={() => history.push('/reports/template/create/')} />
              </Table.HeaderCell>
              <Table.HeaderCell colSpan='6'>
                <Pagination
                  size='tiny'
                  floated='right'
                  activePage={state.activePage}
                  ellipsisItem={null}
                  siblingRange={2}
                  totalPages={(state?.data?.data && Math.ceil(state.data.data.count / 10)) || 1}
                  onPageChange={(event, data) => setState(prevState => ({
                    ...prevState,
                    activePage: data.activePage
                  }))} />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
        :
        <>
          <MobileView
            heading={'Checklist Templates'}
            list={rows}
            buttons={[{
              color: 'teal',
              content: 'Create New Template',
              icon: 'add',
              link: '/checklists/templates/create'
            }]} />

          <br />

          <Pagination
            fluid
            activePage={state.activePage}
            ellipsisItem={null}
            siblingRange={2}
            totalPages={(state?.data?.data && Math.ceil(state.data.data.count / 10)) || 1}
            onPageChange={(event, data) => setState(prevState => ({
              ...prevState,
              activePage: data.activePage
            }))} />
        </>
      }
    </>
  );
}
