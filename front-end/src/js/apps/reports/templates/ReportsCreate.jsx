import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Form, Input, Button, Icon, Table } from 'semantic-ui-react'
import axios from '../../../utils/axios';

export const ReportsCreate = () => {

  const [state, setState] = useState({
    data: null,
    items: [],
    name: null
  });

  const history = useHistory();

  const submitReport = () => {

    if (state.name === null) {
      alert('Your report requires a name.');

      return;
    }

    if (state.items.length === 0) {
      alert('You need to have at least 1 section.');

      return;
    }

    if (state.items[0] === '') {
      alert(`You can't submit an empty property.`);

      return;
    }

    for (const item of state.items) {

      if (item === '') {
        alert(`You can't submit an empty property.`);
        return;
      }
    }

    const params = {
      name: state.name,
      items: state.items
    };

    axios.post('/api/v1/reports/templates/', params)
      .then(() => {
        history.push('/reports/templates/');
      })
      .catch(function (error) {
        alert(error);
      });

  }

  const addItem = () => {

    const items = state.items;
    items.push('');
    setState(prevState => ({
      ...prevState,
      items: [...items]
    }));

  }

  const removeItem = (item) => {

    setState((prevState) => ({
      ...prevState,
      items: prevState.items.filter(it => it !== item),
    }));
  }

  const updateItem = (index, data) => {

    let items = state.items;

    items.splice(index, 1, data);

    setState((prevState) => ({
      ...prevState,
      items: [...items],
    }));
  }

  const getRows = () => state.items.map((item, index) => {

    return (

      <Table.Row key={index}>
        <Table.Cell colSpan='2'>
          <Form.Field
            required={true}
            control={Input}
            placeholder={'Property name'}
            value={item}
            action={{
              color: 'red',
              icon: 'times',
              title: 'Remove',
              onClick: () => removeItem(item)
            }}
            onChange={(event, data) => updateItem(index, data.value)} />
        </Table.Cell>
      </Table.Row>
    );
  });

  return (
    <Form size='tiny'>
      <Table color={'purple'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Add Items To Report</Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            <Table.HeaderCell>
              <Form.Field
                required={true}
                control={Input}
                placeholder='Report Name'
                onChange={(event, data) => state.name = data.value} />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {state.items && getRows()}
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell>
              <Button
                floated='left'
                icon
                labelPosition='left'
                primary
                size='tiny'
                onClick={addItem}>
                <Icon name='plus' /> Add Field
					    </Button>
              <Button
                floated='right'
                icon
                color={'green'}
                labelPosition='left'
                size='tiny'
                onClick={submitReport}>
                <Icon name='save' /> Submit
					    </Button>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
    </Form>
  )
}
