import React, { useEffect, useState } from 'react'
import { useParams, useHistory, Link } from 'react-router-dom';
import { Button, Table, Pagination, Segment } from 'semantic-ui-react';
import { useRequest } from '../../utils/hooks';

export const NotificationsHome = () => {

  const [makeRequest, data, loading, error] = useRequest();

  useEffect(
    () => makeRequest({ url: `/api/v1/notifications/` }),
    []
  );

  const getItems = () => {

    let items = [];

    if (!data || data?.results.length === 0) {

      items.push(
        <Button key={0} color={'orange'} fluid>There are no notifications</Button>
      );

      return items;
    }

    for (const item of data?.results || []) {

      items.push(
        <>
          <Button key={item.id} color={'green'} as={Link} to={item?.url || '/'} fluid>{item.body}</Button>
          <br />
        </>
      );
    }

    return items;
  }

  const items = getItems();

  return (
    <>
      <Segment
        raised
        textAlign={'center'}
        content={'These are your notifications'} />

      {items.length > 2

        ?
        <Segment
          loading={loading}>
          {items}
        </Segment>
        :

        items
      }


    </>
  )
};
