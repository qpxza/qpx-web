import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Button, Header, Image, Modal, Form, Select, Input, Segment, Dropdown, Table, Icon } from 'semantic-ui-react'
import axios from '../../utils/axios';
import { useRequest } from '../../utils/hooks';
import { ErrorMessages } from '../common/ErrorMessages';

export const AddProduct = (props) => {

  const [makeRequest, data, loading, error] = useRequest();

  const history = useHistory();

  const [state, setState] = useState({
    name: null,
    description: null,
    qty: null,
    unit: null,
    sku: null,
    price: null,
    supplier: null,
    suppliers: null
  });

  useEffect(() => {

    async function getSuppliers() {

      axios.get('/api/v1/suppliers/')
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            suppliers: response.data.results
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getSuppliers();

  }, []);

  function handleChange(event, { name, value }) {

    const values = value;
    const names = event.target.name || name;

    setState({
      ...state,
      [names]: values
    });
  }

  const units = () => {

    const measurements = ['mg', 'g', 'kg', 'ml', 'l'];
    let processedItems = [];

    for (const item of measurements) {

      processedItems.push({
        key: item,
        value: item,
        text: item,
      });
    }

    return processedItems;
  }

  const getSuppliers = () => {

    const suppliers = [];

    if (state?.suppliers) {

      for (const supplier of state?.suppliers) {

        suppliers.push({
          key: supplier.id,
          text: supplier.name,
          value: supplier.id
        });
      }
    }

    return suppliers;
  }

  const submitOrder = () => {

    makeRequest({
      method: 'POST',
      url: '/api/v1/products/',
      data: { ...state }
    }).then((response) => {

      if (response.successful) {
        history.push('/catalog');
      } else {

        setState((prevState) => ({
          ...prevState,
          error: true
        }))
      }
    });
  }

  return (
    <Segment>
      <Form
        size={'tiny'}
        error={!!error}
        loading={loading}>

        <Form.Group widths={'equal'}>
          <Form.Field
            required={true}
            control={Input}
            label={'Product Name'}
            placeholder={'Product name'}
            name={'name'}
            onChange={handleChange} />

          <Form.Field
            required={true}
            control={Input}
            label={'Description'}
            placeholder={'Description'}
            name={'description'}
            onChange={handleChange} />
        </Form.Group>

        <Form.Group widths={'6'}>
          <Form.Input
            required={true}
            control={Input}
            label={'Quantity'}
            placeholder={'Quantity'}
            name={'qty'}
            onChange={handleChange} />

          <Form.Field
            required={true}
            control={Select}
            label={'Unit of measurement'}
            placeholder={'Unit of measurement'}
            options={units()}
            name={'unit'}
            onChange={handleChange} />
        </Form.Group>

        <Form.Group>
          <Form.Field
            required={true}
            control={Input}
            label={'SKU'}
            placeholder={'SKU'}
            name={'sku'}
            onChange={handleChange} />

          <Form.Field
            required={true}
            control={Input}
            label={'Price'}
            placeholder={'Price'}
            name={'price'}
            type={'number'}
            onChange={handleChange} />

          <Form.Field
            required={true}
            control={Select}
            label={'Supplier'}
            options={getSuppliers()}
            placeholder={'Supplier'}
            name={'supplier'}
            onChange={handleChange} />

        </Form.Group>

        <Form.Group>
          <Form.Field>
            <Button
              floated={'right'}
              icon={'plus'}
              labelPosition={'left'}
              onClick={submitOrder}
              content={'Submit'} />
          </Form.Field>
        </Form.Group>
      </Form>
      <ErrorMessages error={error} />
    </Segment>
  );
}
