import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Icon, Table, Checkbox, Form, Select } from 'semantic-ui-react';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';
import { CreateOrderModal } from './CreateOrder';
import { MobileView } from '../common';

export const Catalog = (props) => {

  const [state, setState] = useState({
    selectedElements: [],
    showModal: false,
    suppliers: null,
    supplier: null,
    products: null,
  });

  const [makeRequest, data, loading, error] = useRequest();

  useEffect(() => {

    makeRequest({ url: '/api/v1/products/' }).then((response) => {

      if (response.successful) {
        setState(prevState => ({
          ...prevState,
          products: response.data
        }));
      } else {

        setState((prevState) => ({
          ...prevState,
          error: true
        }));
      }
    })

    makeRequest({
      method: 'GET',
      url: '/api/v1/suppliers/',
    }).then((response) => {

      if (response.successful) {
        setState(prevState => ({
          ...prevState,
          suppliers: response.data.results
        }));
      } else {

        setState((prevState) => ({
          ...prevState,
          error: true
        }))
      }
    });

  }, []);

  const history = useHistory();

  const getSuppliers = () => {

    const suppliers = [];

    if (state?.suppliers) {

      for (const supplier of state?.suppliers) {

        suppliers.push({
          key: supplier.id,
          text: supplier.name,
          value: supplier.id
        });
      }
    }

    suppliers.push({
      key: null,
      text: 'Reset Suppliers',
      value: null
    });

    return suppliers;
  }

  const getRows = () => {

    let rows = [];
    let items = state?.products || [];

    for (let i = 0; i < items.length; i++) {

      if (state.supplier && state.supplier !== items[i]?.supplier?.id) {
        continue;
      }

      if (props.screenSize === 'desktop') {
        rows.push(

          <Table.Row
            key={items[i].id}
            disabled={state.supplier && state.supplier !== items[i]?.supplier?.id}
            negative={state.supplier === items[i]?.supplier?.id && state.selectedElements.includes(i) ? true : null}
            onClick={() => {

              if (!state.supplier) {

                setState(prevState => ({
                  ...prevState,
                  supplier: items[i]?.supplier?.id,
                  selectedElements: []
                }));
              }


              if (state.selectedElements.includes(i)) {

                setState(prevState => ({
                  ...prevState,
                  selectedElements: prevState.selectedElements.filter(value => value !== i),
                }));

                return;
              }

              setState(prevState => ({
                ...prevState,
                selectedElements: [...prevState.selectedElements, i],
              }));
            }}>
            <Table.Cell collapsing>
              <Button basic icon={'add to cart'} color={state.selectedElements.includes(i) && state.supplier === items[i]?.supplier?.id ? 'green' : 'grey'} />
            </Table.Cell>
            <Table.Cell>{items[i].name}</Table.Cell>
            <Table.Cell>{items[i].description}</Table.Cell>
            <Table.Cell>{items[i].qty}</Table.Cell>
            <Table.Cell>{`R ${items[i].price}`}</Table.Cell>
            <Table.Cell>{items[i]?.supplier?.name}</Table.Cell>
            <Table.Cell collapsing>
              <Button.Group size='tiny'>
                <Button
                  as={Link}
                  to={`/products/${items[i].id}/edit`}
                  color='blue'
                  icon='pencil' />
                <Button
                  color='red'
                  icon='times'
                  onClick={() => deleteProduct(items[i].id)} />
              </Button.Group>
            </Table.Cell>
          </Table.Row>
        );

        continue;
      }

      rows.push({
        heading: items[i].name,
        isHighlighted: state.supplier === items[i]?.supplier?.id && state.selectedElements.includes(i),
        items: [{
          icon: 'calendar',
          content: `Description: ${items[i].description}`
        }, {
          icon: 'calendar',
          content: `Stock On Hand: ${items[i].qty}`
        }, {
          icon: 'calendar',
          content: `Price Per Item: ${`R ${items[i].price}`}`
        }, {
          icon: 'calendar',
          content: `Supplier: ${items[i]?.supplier?.name}`
        }],
        buttons: [{
          color: 'blue',
          content: 'Edit',
          icon: 'add',
          link: `/products/${items[i].id}/edit`
        }, {
          color: 'green',
          content: 'Add to Cart',
          icon: 'add',
          onClick: () => {

            if (!state.supplier) {

              setState(prevState => ({
                ...prevState,
                supplier: items[i]?.supplier?.id,
                selectedElements: []
              }));
            }


            if (state.selectedElements.includes(i)) {

              setState(prevState => ({
                ...prevState,
                selectedElements: prevState.selectedElements.filter(value => value !== i),
              }));

              return;
            }

            setState(prevState => ({
              ...prevState,
              selectedElements: [...prevState.selectedElements, i],
            }));
          }
        }]
      });
    }

    return rows;
  }

  const rows = getRows();

  return (
    <>
      {props.screenSize === 'desktop'

        ?
        <>
          <Table color={'purple'} celled>
            <Table.Header fullWidth>
              <Table.Row>
                <Table.HeaderCell></Table.HeaderCell>
                <Table.HeaderCell>Name</Table.HeaderCell>
                <Table.HeaderCell>Description</Table.HeaderCell>
                <Table.HeaderCell>Stock on hand</Table.HeaderCell>
                <Table.HeaderCell>Price Per Unit</Table.HeaderCell>
                <Table.HeaderCell>
                  <Form.Field
                    required={true}
                    control={Select}
                    options={getSuppliers()}
                    placeholder={'Supplier'}
                    name={'supplier'}
                    value={state.supplier}
                    clearable
                    onChange={(e, { name, value }) => {

                      setState(prevState => ({
                        ...prevState,
                        supplier: value,
                        selectedElements: []
                      }))
                    }} />
                </Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {rows}
            </Table.Body>

            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell colSpan='2'>
                  <Button
                    onClick={() => setState(prevState => ({
                      ...prevState,
                      showModal: true
                    }))}
                    color='green'
                    content='View Cart'
                    icon='plus'
                    labelPosition='left' />

                </Table.HeaderCell>
                <Table.HeaderCell colSpan='6'>
                  <Button
                    onClick={() => history.push('/products/add')}
                    color='brown'
                    content={'Add Products'}
                    icon={'box'}
                    floated={'right'}
                    labelPosition={'left'} />
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>

          {state.showModal &&

            <CreateOrderModal
              showModal={state.showModal}
              data={state.products.filter((item, index) => state.selectedElements.indexOf(index) > -1)}
              items={state.selectedElements}
              supplier={state.supplier}
              onClose={() => setState(prevState => ({
                ...prevState,
                selectedElements: [],
                supplier: null,
                showModal: false
              }))} />
          }
          <ErrorMessages error={error} />
        </>
        :
        <>

          <MobileView
            heading={'Catalog'}
            topChildren={
              <Form.Field
              fluid
              required={true}
              control={Select}
              options={getSuppliers()}
              placeholder={'Supplier'}
              name={'supplier'}
              value={state.supplier}
              clearable
              onChange={(e, { name, value }) => {

                setState(prevState => ({
                  ...prevState,
                  supplier: value,
                  selectedElements: []
                }))
              }} />
            }
            list={rows}
            buttons={[{
              color: 'green',
              content: 'Add Products',
              icon: 'add',
              link: '/products/add'
            }, {
              color: 'blue',
              content: 'View Cart',
              icon: 'add',
              onClick: () => setState(prevState => ({
                ...prevState,
                showModal: true
              }))
            }]} />

          {state.showModal &&

            <CreateOrderModal
              showModal={state.showModal}
              data={state.products.filter((item, index) => state.selectedElements.indexOf(index) > -1)}
              items={state.selectedElements}
              supplier={state.supplier}
              onClose={() => setState(prevState => ({
                ...prevState,
                selectedElements: [],
                supplier: null,
                showModal: false
              }))} />
          }
          <ErrorMessages error={error} />
        </>
      }
    </>
  )
};
