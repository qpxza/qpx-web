import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Button, Modal, Form, Input, Table, Select } from 'semantic-ui-react'
import { useRequest } from '../../utils/hooks';
import { ErrorMessages } from '../common/ErrorMessages';

export const CreateOrderModal = (props) => {

  const [open, setOpen] = useState(true);
  const [state, setState] = useState({
    data: props.data,
    items: {},
    selectedBranch: null,
    description: 'A new order',
    runningTotal: 0
  });

  const [makeRequest, data, loading, error] = useRequest();
  const [requestBranches, branchData, ,] = useRequest();

  const history = useHistory();

  useEffect(
    () => requestBranches({ url: '/api/v1/branches/' }),
    []
  );

  const submitOrder = () => {

    let items = [];

    for (const [k, v] of Object.entries(state.items)) {
      items.push({
        qty: v,
        product: k
      });
    }

    makeRequest({
      method: 'POST',
      url: '/api/v1/orders/',
      data: {
        supplier: props.supplier,
        description: state.description,
        items: items,
        branch: state.selectedBranch
      }
    }).then((response) => {

      if (response.successful) {
        props.onClose();
      } else {

        setState((prevState) => ({
          ...prevState,
          error: response
        }))
      }
    });
  }

  const getBranches = () => {

    const branches = [];

    if (branchData?.results) {

      for (const branch of branchData?.results) {

        branches.push({
          key: branch.id,
          text: branch.name,
          value: branch.id
        });
      }
    }

    return branches;
  }

  const generateTotals = () => {

    let total = 0;

    for (const item of state?.data) {
      if (state.items[item.id]) {
        total += (state.items[item.id] || 0) * parseFloat(item.price)
      }
    }

    return total;
  }

  const getRows = () => {

    let rows = [];

    for (const item of state?.data || []) {

      rows.push(

        <Table.Row key={`${item.name}${item.description}`}>
          <Table.Cell>{item.name}</Table.Cell>
          <Table.Cell>{item.description}</Table.Cell>
          <Table.Cell>{item.qty}</Table.Cell>
          <Table.Cell>{`R ${item.price}`}</Table.Cell>
          <Table.Cell>
            <Form.Field
              control={Input}
              error={isNaN(item.qty) || item.qty === ''}
              type={'number'}
              defaultValue={state.items[item.id] || 0}
              onChange={(event, data) => setState((prevState) => ({ ...prevState, items: { ...state.items, [item.id]: data.value } }))} />
          </Table.Cell>
          <Table.Cell>{item.supplier.name}</Table.Cell>
          <Table.Cell>{`R ${(state.items[item.id] || 0) * parseFloat(item.price)}`}</Table.Cell>
          <Table.Cell collapsing>
            <Button.Group size='tiny'>
              <Button
                color='red'
                icon='times'
                onClick={() => setState((prevState) => ({ ...prevState, data: state.data.filter(row => row !== item) }))} />
            </Button.Group>
          </Table.Cell>
        </Table.Row>
      );
    }

    rows.push(
      <Table.Row key={'totalRow'}>
        <Table.Cell colSpan={'7'} textAlign={'right'}>{`R ${generateTotals()}`}</Table.Cell>
        <Table.Cell></Table.Cell>
      </Table.Row>
    );

    return rows;
  }

  const rows = getRows();
  const branchOptions = getBranches();

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={props.showModal}>

      <Modal.Header>
        Confirm Your Order
      </Modal.Header>
      <Modal.Content >
        <Table celled compact definition>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Description</Table.HeaderCell>
              <Table.HeaderCell>Stock on hand</Table.HeaderCell>
              <Table.HeaderCell>Price Per Unit</Table.HeaderCell>
              <Table.HeaderCell>Quantity</Table.HeaderCell>
              <Table.HeaderCell>Supplier</Table.HeaderCell>
              <Table.HeaderCell>Total Price</Table.HeaderCell>
              <Table.HeaderCell></Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {rows}
          </Table.Body>
        </Table>
      </Modal.Content>
      <Modal.Actions>
        <div style={{ display: 'flex', flex: '1', flexDirection: 'row', float: 'left' }}>
          <Form.Field
            required={true}
            control={Select}
            options={branchOptions}
            placeholder={'Branch'}
            onChange={(event, data) => {
              setState({
                ...state,
                selectedBranch: data.value
              })
            }} />
          <Form.Field
            required={true}
            control={Input}
            placeholder='Description'
            value={state.comment}
            onChange={(event, data) => {
              setState({
                ...state,
                description: data.value
              })
            }} />
        </div>
        <Button.Group>
          <Button
            content={'Cancel'}
            color={'red'}
            onClick={() => props.onClose()} />

          <Button
            content={'Submit'}
            onClick={() => submitOrder()}
            positive />
        </Button.Group>
      </Modal.Actions>
      <ErrorMessages error={state.error} />
    </Modal>
  );
}
