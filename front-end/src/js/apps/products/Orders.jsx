import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Icon, Table, Pagination, } from 'semantic-ui-react';
import { localTime } from '../../utils/time';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';
import { MobileView } from '../common';

export const Orders = () => {

  const [activePage, setActivePage] = useState(1);
  const [state, setState] = useState({ selectedElements: [] });

  const [makeRequest, data, loading, error] = useRequest();

  useEffect(
    () => makeRequest({ url: `/api/v1/checklists/templates/?page=${activePage}` }),
    [activePage]
  );

  const history = useHistory();

  const getRows = () => {

    let rows = [];

    for (const item of data?.results || []) {

      rows.push(

        <Table.Row
          key={item.id}
          positive={state.selectedElements.includes(item.id) ? true : null}
          onClick={() => {

            if (state.selectedElements.includes(item.id)) {

              setState(prevState => ({
                ...prevState,
                selectedElements: prevState.selectedElements.filter(value => value !== item.id)
              }));

              return;
            }

            setState(prevState => ({
              ...prevState,
              selectedElements: [...prevState.selectedElements, item.id]
            }));
          }}>
          <Table.Cell>{item.name}</Table.Cell>
          <Table.Cell>Some lekker tamato sauce</Table.Cell>
          <Table.Cell>24</Table.Cell>
          <Table.Cell>R32.95</Table.Cell>
          <Table.Cell collapsing>
            <Button.Group size='tiny'>
              <Button
                as={Link}
                to={`/checklists/templates/${item.id}/edit`}
                color='blue'
                icon='pencil' />
              <Button
                color='red'
                icon='times'
                onClick={() => deleteChecklistItem(item)} />
            </Button.Group>
          </Table.Cell>
        </Table.Row>
      );
    }

    return rows;
  }

  const rows = getRows();

  const deleteChecklistItem = (checklistItem) => {
    const params = {
      url: `/api/v1/checklists/templates/${''}/`,
      method: 'delete',
    };

    makeRequest(params)
      .then((res) => res.successful && window.location.reload());
  }

  return (
    <>
      <Table color={'purple'} celled>
        <Table.Header fullWidth>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Description</Table.HeaderCell>
            <Table.HeaderCell>Stock on hand</Table.HeaderCell>
            <Table.HeaderCell>Price</Table.HeaderCell>
            <Table.HeaderCell>Actions</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {rows}
        </Table.Body>

        <Table.Footer fullWidth>
          <Table.Row>
            <Table.HeaderCell colSpan='1'>
              <Button
                as={Link}
                size='tiny'
                color='green'
                content='Create New Order'
                icon='dollar'
                labelPosition='left'
                to={'/orders/create'} />

            </Table.HeaderCell>
            <Table.HeaderCell colSpan='6'>
              <Pagination
                size='tiny'
                floated='right'
                activePage={activePage}
                ellipsisItem={null}
                siblingRange={2}
                totalPages={Math.ceil((data?.count || 10) / 10)}
                onPageChange={(event, data) => setActivePage(data.activePage)} />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <ErrorMessages error={error} />
    </>
  )
};
