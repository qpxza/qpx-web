import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Icon, Table, Pagination, } from 'semantic-ui-react';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';
import { ConfirmationModal } from './ConfirmationModal';
import { MobileView } from '../common';

export const Orders = (props) => {

  const [activePage, setActivePage] = useState(1);
  const [showModal, setModal] = useState(false);
  const [state, setState] = useState({
    supplier: null,
    id: null,
    status: null,
    description: null
  });

  const [makeRequest, data, loading, error] = useRequest();

  useEffect(
    () => makeRequest({ url: `/api/v1/orders/?page=${activePage}` }),
    [activePage]
  );

  const history = useHistory();

  const getStatusIcon = (status) => {

    switch (status) {
      case 1:

        return <Icon size={'large'} color={'orange'} name={'clock outline'} />;
        ;
      case 2:

        return <Icon size={'large'} color={'green'} name={'check circle outline'} />;
      case 4:

        return <Icon size={'large'} color={'red'} name={'times circle outline'} />;

      default:
        return <Icon size={'large'} color={'grey'} name={'question circle outline'} />;
    }
  }

  const getStatusColor = (status) => {

    switch (status) {
      case 1:

        return 'orange';
      case 2:

        return 'green';
      case 4:

        return 'red';

      default:
        return 'grey';
    }
  }

  const showConfirmation = (status, id, supplier, description) => {

    setModal(true);

    setState((prevState) => ({
      ...prevState,
      status,
      id,
      supplier,
      description
    }))
  }

  const getOrders = () => {

    let tables = [];

    if (props.screenSize === 'desktop') {
      for (const item of data?.results || []) {

        tables.push(

          <Table.Row key={item.id}>
            <Table.Cell>{getStatusIcon(item.status)}{` Order Number: ${item.id}`}</Table.Cell>
            <Table.Cell>{item?.branch?.name}</Table.Cell>
            <Table.Cell>{item?.supplier?.name}</Table.Cell>
            <Table.Cell>{item.description}</Table.Cell>
            <Table.Cell>R {item.total}</Table.Cell>
            <Table.Cell>{`Status: ${item.get_status_display}`}</Table.Cell>
            <Table.Cell collapsing>
              <Button.Group size='small'>
                <Button
                  title={'Confirm Order'}
                  icon={'check circle outline'}
                  positive
                  onClick={() => showConfirmation(2, item.id, item.supplier.id, item.description)} />
                <Button
                  title={'View Order'}
                  icon={'eye'}
                  color={'blue'}
                  onClick={() => history.push(`orders/${item.id}`)} />
                <Button
                  title={'Discard Order'}
                  color='red'
                  icon={'trash alternate outline'}
                  negative
                  onClick={() => showConfirmation(4, item.id, item.supplier.id, item.description)} />

              </Button.Group>
            </Table.Cell>
          </Table.Row>
        );
      }

      return tables;
    }

    for (const item of data?.results || []) {

      tables.push({
        heading: `Order Number: ${item.id}`,
        isHighlighted: true,
        highligtedColor: getStatusColor(item.status),
        items: [{
          icon: 'calendar',
          content: `Stock On Hand: ${item?.branch?.name}`
        }, {
          icon: 'calendar',
          content: `Price Per Item: ${item?.supplier?.name}`
        }, {
          icon: 'calendar',
          content: `Supplier: ${item.description}`
        }, {
          icon: 'calendar',
          content: `Supplier: ${item.total}`
        }, {
          icon: 'calendar',
          content: `Supplier: ${item.get_status_display}`
        }],
        buttons: [{
          color: 'blue',
          content: 'View Order',
          icon: 'eye',
          link: `orders/${item.id}`
        }, {
          color: 'green',
          content: 'Confirm Order',
          icon: 'check',
          onClick: () => showConfirmation(2, item.id, item.supplier.id, item.description)
        }, {
          color: 'red',
          content: 'Discard Order',
          icon: 'trash alternate outline',
          onClick: () => showConfirmation(4, item.id, item.supplier.id, item.description)
        }]
      });
    }

    return tables;
  }

  const orders = getOrders();

  return (
    <>
      {props.screenSize === 'desktop'
        ?
        <>
          <Table color={'purple'} loading={loading.toString()}>
            <Table.Header fullWidth >
              <Table.Row>
                <Table.HeaderCell>Order Number</Table.HeaderCell>
                <Table.HeaderCell>Branch</Table.HeaderCell>
                <Table.HeaderCell>Supplier</Table.HeaderCell>
                <Table.HeaderCell>Description</Table.HeaderCell>
                <Table.HeaderCell>Total</Table.HeaderCell>
                <Table.HeaderCell>Status</Table.HeaderCell>
                <Table.HeaderCell>Actions</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {orders}
            </Table.Body>

            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell colSpan='1'>
                  <Button
                    size='tiny'
                    color='green'
                    content='Create New Order'
                    icon='plus'
                    labelPosition='left'
                    onClick={() => history.push('/catalog')} />

                </Table.HeaderCell>
                <Table.HeaderCell colSpan='6'>
                  <Pagination
                    size='tiny'
                    floated='right'
                    activePage={activePage}
                    ellipsisItem={null}
                    siblingRange={2}
                    totalPages={Math.ceil((data?.count || 10) / 10)}
                    onPageChange={(event, data) => setActivePage(data.activePage)} />
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
          <ErrorMessages error={error} />

          {showModal &&
            <ConfirmationModal
              showModal={showModal}
              onClose={() => setModal(false)}
              supplier={state.supplier}
              id={state.id}
              description={state.description}
              status={state.status} />
          }
        </>

        :

        <>

          <MobileView
            heading={'Orders'}
            list={orders}
            buttons={[{
              color: 'green',
              content: 'Create New Order',
              icon: 'add',
              link: '/catalog'
            }]} />

          {showModal &&
            <ConfirmationModal
              showModal={showModal}
              onClose={() => setModal(false)}
              supplier={state.supplier}
              id={state.id}
              description={state.description}
              status={state.status} />
          }

        </>
      }
    </>
  )
};
