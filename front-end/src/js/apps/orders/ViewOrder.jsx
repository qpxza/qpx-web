import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom';
import { Button, Table, Pagination, } from 'semantic-ui-react';
import { useRequest } from '../../utils/hooks';
import { MobileView } from '../common';

export const ViewOrder = (props) => {

  const { id } = useParams();

  const [makeRequest, data, loading, error] = useRequest();

  useEffect(
    () => makeRequest({ url: `/api/v1/orders/${id}` }),
    []
  );

  const history = useHistory();

  const getItems = () => {

    let items = [];

    if (props.screenSize === 'desktop') {

      if (!data || data.items.length === 0) {

        items.push(
          <Table.Row key={1}>
            <Table.Cell colSpan={'6'}>
              Unfortunately there are no items in this order.
            </Table.Cell>
          </Table.Row>
        );

        return items;
      }

      for (const item of data?.items || []) {

        items.push(

          <Table.Row key={item.id}>
            <Table.Cell>{item?.product?.name}</Table.Cell>
            <Table.Cell>{item.qty}</Table.Cell>
            <Table.Cell>R {item?.product?.price}</Table.Cell>
            <Table.Cell>{`R ${parseInt(item.qty) * parseFloat(item?.product?.price)}`}</Table.Cell>
            <Table.Cell>{item?.product?.sku}</Table.Cell>
          </Table.Row>
        );
      }

      return items;
    }

    for (const item of data?.items || []) {

      items.push({
        heading: item?.product?.name,
        items: [{
          icon: 'calendar',
          content: `Quantity Ordered: ${item.qty}`
        }, {
          icon: 'calendar',
          content: `Price Per Item: ${item?.product?.price}`
        }, {
          icon: 'calendar',
          content: `Total Cost: R ${parseInt(item.qty) * parseFloat(item?.product?.price)}`
        }],
        buttons: []
      });

    }

    return items;
  }

  const items = getItems();

  return (

    <>
      {props.screenSize === 'desktop'

        ?
        <>
          <Table color={'purple'} loading={loading}>
            <Table.Header fullWidth >
              <Table.Row>
                <Table.HeaderCell>{`Order Number: ${data?.id || 0}`}</Table.HeaderCell>
                <Table.HeaderCell>{data?.branch?.name}</Table.HeaderCell>
                <Table.HeaderCell>{data?.supplier?.name}</Table.HeaderCell>
                <Table.HeaderCell>R {data?.total}</Table.HeaderCell>
                <Table.HeaderCell>{data?.get_status_display}</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
          </Table>
          <br />
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>{'Product'}</Table.HeaderCell>
                <Table.HeaderCell>{'Quantity'}</Table.HeaderCell>
                <Table.HeaderCell>{'Price Per Unit'}</Table.HeaderCell>
                <Table.HeaderCell>{'Total Price'}</Table.HeaderCell>
                <Table.HeaderCell>{'SKU'}</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {items}
            </Table.Body>

            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell colSpan={'5'}>
                  <Button
                    floated={'left'}
                    size='small'
                    color='blue'
                    content='Back To Orders'
                    icon='arrow left'
                    labelPosition='left'
                    onClick={() => history.push('/orders')} />

                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
        </>

        :

        <>
          <MobileView
            heading={'View Orders'}
            list={items}
            buttons={[{
              color: 'blue',
              icon: 'arrow left',
              content: 'Go Back To Orders',
              link: '/orders'
            }]} />
        </>

      }
    </>
  )
};
