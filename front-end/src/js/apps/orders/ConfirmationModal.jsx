import React, { useState } from 'react'
import { useHistory } from 'react-router-dom';
import { Button, Modal, Form, Input, Table } from 'semantic-ui-react'
import { useRequest } from '../../utils/hooks';
import { ErrorMessages } from '../common/ErrorMessages';

export const ConfirmationModal = (props) => {

  const [open, setOpen] = useState(true);

  const [makeRequest, data, loading, error] = useRequest();

  const history = useHistory();

  const updateOrder = () => {

    makeRequest({
      method: 'PUT',
      url: `/api/v1/orders/${props.id}/`,
      data: {
        supplier: props.supplier,
        description: props.description,
        status: props.status
      }
    }).then((response) => {

      if (response.successful) {
        props.onClose();
      }
    });
  }

  return (
    <Modal
      basic={props.status === 2 ? false : true}
      onClose={() => props.onClose()}
      onOpen={() => setOpen(true)}
      open={props.showModal}>

      <Modal.Header>
        {props.status === 2 ? 'Complete Order' : 'DISCARD ORDER'}
      </Modal.Header>
      <Modal.Content >
        {props.status === 2 ? 'Are you sure the order can be marked as complete?' : 'Do you really want to discard this order?'}
      </Modal.Content>
      <Modal.Actions>
        <Button
          content={'Cancel'}
          color={'red'}
          onClick={() => props.onClose()} />

        <Button
          type={'submit'}
          content={'Submit'}
          labelPosition={'right'}
          icon={'check circle outline'}
          onClick={() => updateOrder()}
          positive />
      </Modal.Actions>
      <ErrorMessages error={error} />
    </Modal>
  );
}
