import React, { useEffect, useState } from 'react'
import {
  Button,
  Form,
  Input,
  Segment,
  Select,
} from 'semantic-ui-react';
import axios from '../../utils/axios';
import { useHistory, useParams } from 'react-router-dom';
import { isEqual } from 'lodash'
import { useRequest } from '../../utils';
import { ErrorMessages } from '../common/ErrorMessages';

export const EditSupplier = () => {

  const { id } = useParams();

  const history = useHistory();

  const [supplier, setSupplier] = useState({
    id: null,
    name: '',
    description: '',
    address_line_1: '',
    address_line_2: '',
    city: '',
    country: 'ZAF',
    postal_code: '',
    email: '',
    phone: '',
    cellphone: '',
    contacts: [],
  });

  const [updateSupplier, , updating, error] = useRequest();

  const [fetchSupplier, , loading] = useRequest();

  useEffect(() => {
    function getSupplier() {
      fetchSupplier({ url: `/api/v1/suppliers/${id}/` })
        .then(res => res.successful && setSupplier(res.data))
    }

    // having an id parameter implies we are editing the supplier
    if (id) {
      getSupplier();
    }
  }, []);

  const defaultOnChangeHandler = (field) => {
    return (event, data) => setSupplier({
      ...supplier,
      [field]: data.value
    });
  };

  const submit = () => {
    const params = {
      ...supplier,
    };

    const config = id
      ? { url: `/api/v1/suppliers/${id}/`, data: params, method: 'PUT' }
      : { url: `/api/v1/suppliers/`, data: params, method: 'POST' };


    updateSupplier(config)
      .then((res) => res.successful && history.push('/suppliers'));
  }

  return (
    <Segment>
      <Form
        size='tiny'
        error={!!error}
        loading={loading || updating}>

        <Form.Group widths={'equal'}>
          <Form.Field
            required={true}
            control={Input}
            label='Name'
            placeholder='Supplier name'
            defaultValue={supplier.name}
            onChange={defaultOnChangeHandler('name')} />
        </Form.Group>

        <Form.Group widths={'equal'}>
          <Form.Input
            control={Input}
            label='Email'
            placeholder='Email'
            defaultValue={supplier.email}
            onChange={defaultOnChangeHandler('email')} />
          <Form.Field
            control={Input}
            label='Phone'
            placeholder='Supplier Phone Number'
            defaultValue={supplier.phone}
            onChange={defaultOnChangeHandler('phone')} />
          <Form.Field
            control={Input}
            label='Cell Number'
            placeholder='Cell Number'
            defaultValue={supplier.cellphone}
            onChange={defaultOnChangeHandler('cellphone')} />
        </Form.Group>

        <Form.Group>
          <Form.Field
            control={Input}
            width={6}
            label='City'
            placeholder='City'
            defaultValue={supplier.address_line_1}
            onChange={defaultOnChangeHandler('city')} />
          <Form.Field
            control={Input}
            width={10}
            label='Address line 1'
            placeholder='22 example st'
            defaultValue={supplier.address_line_1}
            onChange={defaultOnChangeHandler('address_line_1')} />
        </Form.Group>

        <Form.Group>
          <Form.Field
            control={Input}
            width={12}
            label='Address line 2'
            placeholder='Building, complex, floor, unit'
            defaultValue={supplier.address_line_2}
            onChange={defaultOnChangeHandler('address_line_2')} />
          <Form.Field
            control={Input}
            width={4}
            label='Postal code'
            placeholder='0001'
            defaultValue={supplier.postal_code}
            onChange={defaultOnChangeHandler('postal_code')} />
        </Form.Group>

        <Form.Group widths={'equal'}>
          <Form.TextArea
            label='Description'
            placeholder='Supplier description...'
            defaultValue={supplier.description} onChange={defaultOnChangeHandler('description')} />
        </Form.Group>
        <Button
          onClick={submit}
          size='tiny'
          color='green'>
          Submit
        </Button>
        <ErrorMessages error={error} />
      </Form>
    </Segment>
  );
};
