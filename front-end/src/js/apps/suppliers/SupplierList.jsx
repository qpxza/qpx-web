import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Button, Icon, Table, Pagination, Card } from 'semantic-ui-react'
import { localTime, useQuery, useRequest } from '../../utils';
import { MobileView } from '../common';

export const SupplierList = (props) => {

  const [activePage, setActivePage] = useState(useQuery().get('page') || 1);

  const [getSuppliers, data, loading, error] = useRequest();
  const [deleteSupplier, , deleting, deleteError] = useRequest();

  useEffect(() => {
    getSuppliers({ url: `/api/v1/suppliers/?page=${activePage}` });
  }, [activePage])

  const removeSupplier = (id) => {
    if (confirm('Are you sure?')) {
      deleteSupplier({ url: `/api/v1/suppliers/${id}/`, method: 'DELETE' })
        .then((res) => {
          if (res.successful) {
            window.location.reload();
          } else {
            alert('Unable to delete supplier. Please try again');
          }
        });
    }
  };

  const generateRows = () => {

    const rows = [];

    for (const supplier of data?.results || []) {

      rows.push({
        heading: supplier.name,
        items: [{
          icon: 'calendar',
          content: `Date Created: ${localTime(supplier.created_at)}`
        }, {
          icon: 'calendar',
          content: `Date Updated: ${localTime(supplier.updated_at)}`
        }, {
          icon: 'calendar',
          content: `Description: ${supplier.description}`
        }, {
          icon: 'calendar',
          content: `Address: ${supplier.address}`
        }, {
          icon: 'calendar',
          content: `Email: ${supplier.email}`
        },
        {
          icon: 'calendar',
          content: `Contact Number: ${supplier.phone}`
        }],
        buttons: [{
          color: 'blue',
          content: 'Edit',
          icon: 'add',
          link: `/suppliers/${supplier.id}/edit`
        }]
      });

    }

    return rows;
  }

  const rows = generateRows();

  return (

    <>
      {props.screenSize === 'desktop'

        ? <Table
          color={'purple'}
          celled>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Description</Table.HeaderCell>
              <Table.HeaderCell>Address</Table.HeaderCell>
              <Table.HeaderCell>E-mail address</Table.HeaderCell>
              <Table.HeaderCell>Phone</Table.HeaderCell>
              <Table.HeaderCell textAlign='right'>Action</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {(data?.results || []).map((supplier, i) =>
              <Table.Row key={i}>
                <Table.Cell>{supplier.name}</Table.Cell>
                <Table.Cell>{supplier.description}</Table.Cell>
                <Table.Cell>{supplier.address}</Table.Cell>
                <Table.Cell>{supplier.email}</Table.Cell>
                <Table.Cell>{supplier.phone}</Table.Cell>
                <Table.Cell textAlign='right'>
                  <Button.Group size='tiny'>
                    <Button
                      as={Link}
                      to={`/suppliers/${supplier.id}/edit`}
                      primary
                      icon='pencil'
                      size='tiny'
                      title='Edit supplier' />
                    <Button
                      negative
                      icon='trash alternate outline'
                      size='tiny'
                      title='Delete supplier'
                      onClick={() => removeSupplier(supplier.id)} />
                  </Button.Group>
                </Table.Cell>
              </Table.Row>
            )}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan='3'>
                <Button
                  as={Link}
                  to='/suppliers/create'
                  icon
                  labelPosition='left'
                  primary
                  size='tiny'
                  loading={loading}>
                  <Icon name='building' /> Add Supplier
                </Button>

              </Table.HeaderCell>
              <Table.HeaderCell colSpan='3'>
                <Pagination
                  className='right floated'
                  defaultActivePage={activePage}
                  totalPages={Math.ceil((data?.count || 0) / 10)}
                  onPageChange={(event, data) => setActivePage(data.activePage)} />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>

        :

        <MobileView
          heading={'Supplier List'}
          list={rows}
          buttons={[{
            color: 'teal',
            content: 'Add new supplier',
            icon: 'add',
            link: '/suppliers/create'
          }]} />
      }
    </>
  );
}
