import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { Button, Checkbox, Icon, Table, Pagination, Card, List } from 'semantic-ui-react'

export const MobileCards = (props) => {

  useEffect(() => {

  },[props]);

  const generateListItems = (items) => {

    let list = [];

    for (const item of items) {
      list.push(
        <List.Item icon={item.icon} content={item.content} />
      );
    }

    return list;
  }

  const generateButtons = (items) => {

    let buttons = [];

    for (const item of items) {
      
      if(item.onClick && typeof item.onClick === 'function'){
        buttons.push(
          <Button
            basic
            color={item.color}
            onClick={() => item.onClick()}
            content={item.content} />
        );

        continue;
      }

      buttons.push(
        <Button
          basic
          color={item.color}
          as={Link}
          to={item.link}
          content={item.content} />
      );
    }

    return buttons;
  }

  const renderCards = () => {

    let cards = [];

    if(!props.list || props.list.length === 0){
      return (
        <Card/>
      )
    }

    for (const item of props.list) {

      cards.push(
        <Card fluid style={ item.isHighlighted ? { 'boxShadow': `0 0 10px ${item.highligtedColor || 'rgba(81, 203, 238, 1)'}` } : {}}>
          <Card.Content>
            <Card.Header>
              {item.heading}
            </Card.Header>
            <Card.Description>
              <List>
                {generateListItems(item?.items)}
              </List>
            </Card.Description>

          </Card.Content>
          {item?.buttons &&
            <Card.Content extra>
              <Button.Group fluid compact size={'tiny'}>
                {generateButtons(item?.buttons)}
              </Button.Group>
            </Card.Content>
          }
        </Card>
      );
    }

    return cards;
  }

  return (
    <>
      {renderCards()}
    </>
  );
}