
import React from 'react';
import { Message } from 'semantic-ui-react';
import _ from 'lodash';

const extractMessages = (messages, keys = []) => {
    return Object.entries(messages).map(([name, errors]) => {
        if (typeof errors === 'string') {
            return errors;
        }

        return errors.map((messages, i) => {
            if (typeof messages === 'string') {
                return messages + '..' + [...keys, name].join('.');
            }

            return extractMessages(messages, [...keys, name, i + 1]); // use i + 1 to count from 1 like humans do
        });
    });
};

export const ErrorMessages = ({ error }) => {
    if (!error) {
        return <></>;
    }

    if (error?.response?.statusCode === 401 || error?.response?.statusCode === 403) {
        return (<Message
            error
            header='Authentication or authorisation error'
            icon='warning'
            content={error.response.data?.detail || 'You are either not allowed to view this resource or logged out on another page'}
        />);
    }

    const message = error?.message || 'An unknown error occured. We don\'t know that happened';
    const errors = typeof error?.response?.data === 'object' ? error?.response?.data || {} : {};

    let messages = [];

    try {
        messages = _.flattenDeep(extractMessages(errors, []));
    } catch (e) {
        console.error('Unable to extract error messages', e);
    }

    return (
        <Message
            error
            header={message}
            list={messages} />
    );
}
