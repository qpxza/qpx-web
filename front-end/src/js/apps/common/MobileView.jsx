import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { Button, Checkbox, Icon, Table, Pagination, Card, List } from 'semantic-ui-react'
import { MobileCards } from './MobileCards';


export const MobileView = (props) => {

  const generateButtons = () => {

    let buttons = [];

    for (const item of props.buttons) {

      if (item.onClick && typeof item.onClick === 'function') {

        buttons.push(
          <Button
            icon={item.icon}
            content={item.content}
            color={item.color}
            onClick={() => item.onClick()} />
        );

        continue;
      }

      buttons.push(
        <Button
          as={Link}
          to={item.link}
          icon={item.icon}
          content={item.content}
          color={item.color} />
      );
    }

    return buttons;
  }

  return (
    <>
      {props?.buttons &&
        <Button.Group fluid>
          {generateButtons()}
        </Button.Group>
      }

      {!!props.topChildren &&
        <>
          <br />
          {props.topChildren}

        </>
      }
      <br />
      <br />
      <Card.Group>
        <MobileCards id={props.id} heading={props.heading} list={props.list} />
      </Card.Group >
    </>
  );
}