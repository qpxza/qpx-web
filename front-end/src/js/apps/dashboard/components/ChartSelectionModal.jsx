import React, { useEffect, useState } from 'react'
import { Button, Header, Image, Modal, Form, Select, Input, Segment, Dropdown } from 'semantic-ui-react'
import axios from '../../../utils/axios';
import { useRequest } from '../../../utils/hooks';

export const ChartSelectionModal = (props) => {

  const [open, setOpen] = useState(true);

  const [makeRequest, data, loading, error] = useRequest();

  const [state, setState] = useState({
    selectedChart: null,
    reportItem: null,
    chartComplexity: null,
    reportItems: null,
    widget: null,
    error: false,
    searchQuery: '',
    searchValue: ''
  });

  useEffect(() => {

    function getBranches() {

      axios.get('/api/v1/reports/items/names/')
        .then((response) => {

          setState(prevState => ({
            ...prevState,
            reportItems: response?.data
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranches();

  }, []);

  const charts = [
    { key: 'bar', value: 'bar', text: 'Bar Chart' },
    { key: 'line', value: 'line', text: 'Line Chart' },
    { key: 'pie', value: 'pie', text: 'Pie Chart' },
  ];

  const submitWidget = () => {

    makeRequest({
      method: 'POST',
      url: '/api/v1/widgets/',
      data: {
        'name': state.widget,
        'widget_type': state.selectedChart,
        'definition': state.reportItem
      }
    }).then((response) => {

      if (response.successful) {
        props.onClose() && props.onClose();
      } else {

        setState((prevState) => ({
          ...prevState,
          error: true
        }))
      }
    });
  }

  const getReportItems = () => {

    const items = state.reportItems;

    let list = [];

    for (const item of items) {

      list.push({
        key: item.name,
        value: item.name,
        text: item.name
      });

    }

    return list;
  }

  return (
    <Modal
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={props.showModal}>

      <Modal.Header>Create a widget</Modal.Header>
      <Modal.Content image>
        <Modal.Description>
          <Header>Select the type of widget you need: </Header>
          <Form.Field
            required={true}
            control={Select}
            options={charts}
            placeholder={'Widget Type'}
            onChange={(event, data) => setState((prevState) => ({
              ...prevState,
              selectedChart: data.value
            }))} />

          <Form.Group inline >

            <br />

            <Form.Field
              error={state.error}
              control={Input}
              placeholder={'Widget Name'}
              onChange={(event, data) => setState((prevState) => ({
                ...prevState,
                widget: data.value
              }))} />

            <br />

            <Dropdown
              fluid
              selection
              multiple={true}
              search={true}
              options={state.reportItems && getReportItems()}
              placeholder={'Item to report on'}
              onChange={(event, data) => setState({
                ...state,
                reportItem: data.value
              })}
              onSearchChange={(event, data) => setState({
                ...state,
                searchQuery: data.value
              })}
            />

          </Form.Group>

        </Modal.Description>
      </Modal.Content>
      <Modal.Actions>
        <Button
          content={'Cancel'}
          color={'red'}
          onClick={() => props.onClose && props.onClose()} />

        <Button
          type={'submit'}
          content={'Submit'}
          labelPosition={'right'}
          icon={'check circle outline'}
          onClick={() => submitWidget()}
          positive />
      </Modal.Actions>
    </Modal>
  );
}

