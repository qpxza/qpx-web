export const colors = [
  '#ff0000',
  '#ff9700',
  '#ababab',
  '#ffe800',
  '#00ffc1',
  '#0291de',
  '#0059ff',
  '#9700ff',
  '#e800ff',
  '#ff00a6',
  '#007118',
  '#C70039',
  '#581845',
  '#E2498A',
  '#133336',
  '#ED7D08',
  '#aaffc3',
  '#808000',
  '#ffd8b1',
  '#000080',
  '#808080',
  '#ffffff',
];

export const mapInterval = (interval) => {

  switch (interval) {
    case 'day':
      return { 'weekday': 'short' };
    case 'week':
      return { 'day': 'numeric', 'month': 'numeric' };
    case 'month':
      return { 'month': 'short', 'year': 'numeric' }
    default:
      return interval;
  }
}

export const generateGraph = (type, items, interval = { 'weekday': 'short' }) => {

  let config = {};
  let cleanedConfig = [];

  if (type !== 'pie') {


    for (const [key, value] of Object.entries(items)) {

      for (const [date, val] of Object.entries(value)) {

        config = {
          ...config,
          [date]: {
            ...config[date],
            reportTime: new Date(date).toLocaleString('en-GB', mapInterval(interval)),
            [key]: val,
          }
        }
      }
    }

    for (const [key, value] of Object.entries(config)) {
      cleanedConfig.push({
        ...value
      });
    }

  }

  if (type === 'pie') {

    let colorIndex = 0;

    for (const [key, value] of Object.entries(items)) {

      const valueOverPeriod = 0;

      for (const [date, val] of Object.entries(value)) {

        valueOverPeriod += val;
      }

      cleanedConfig.push({
        name: key,
        value: valueOverPeriod,
        fill: colors[colorIndex]
      });

      colorIndex = colorIndex >= 21 ? 0 : colorIndex + 1;
    }
  }

  return cleanedConfig;
}