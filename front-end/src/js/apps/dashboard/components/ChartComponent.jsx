import React, { useEffect, useState } from 'react'
import { Segment, Button, Icon, Label, Card, Form, Select } from 'semantic-ui-react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  Tooltip,
  ResponsiveContainer,
  LineChart,
  CartesianGrid,
  Legend,
  Line,
  Pie,
  PieChart
} from 'recharts';
import { useRequest } from '../../../utils/hooks';
import { generateGraph, colors } from './handleCharts';
import { getLastMonth, getLastSevenDays, getLastWeek, getYearToDate } from '../../../utils/time';
import moment from 'moment';

export const ChartComponent = (props) => {

  const [state, setState] = useState({
    info: null,
    items: [],
    stateChange: false,
    startDate: props.from,
    endDate: props.to,
  });

  const [makeRequest, data, loading, error] = useRequest();

  useEffect(() => {

    function getInfo() {

      let query = '?';

      for (let i = 0; i < props?.definition?.length; i++) {

        query += `name=${props?.definition[i]}${i === props?.definition.length - 1 ? '' : '&'}`
      }

      makeRequest({
        method: 'GET',
        url: `/api/v1/reports/items/summary/${query}`,
        params: {
          name: props.name,
          from_date: state.stateChange ? state.startDate : props.from,
          to_date: state.stateChange ? state.endDate : props.to,
          branches: props.branch
        }
      }).then((response) => {
        if (response.successful) {

          setState(prevState => ({
            ...prevState,
            stateChange: false,
            items: generateGraph(props.type, response?.data?.data, response?.data?.interval)
          }));
        }
      })
    }

    getInfo();

  }, [props.from, props.to, state.startDate, state.endDate, props.branch]);

  const getChart = (type) => {

    let items = [];
    let colorIndex = 0;

    //Default to bar chart
    switch (type) {

      case 'line':

        for (let i = 0; i < props.definition.length; i++) {

          items.push(
            <Line key={i} dataKey={props.definition[i]} fill={colors[colorIndex]} stroke={colors[colorIndex]} />
          );

          colorIndex = colorIndex >= 21 ? 0 : colorIndex + 1;
        }

        return (
          <LineChart key={props.id} data={state.items}>
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey={'reportTime'} interval={0} />
            <YAxis />
            <Tooltip />
            <Legend />
            {items}
          </LineChart>
        );

      case 'pie':
        return (
          <PieChart key={props.id}>
            <Tooltip />
            <Legend
              payload={
                state.items.map(
                  item => ({
                    id: item.name,
                    type: 'square',
                    value: `${item.name} (${item.value})`,
                    color: item.fill
                  })
                )
              } />
            <Pie data={state.items} dataKey={'value'} nameKey={'name'} cx="50%" cy="50%" label legendType={'rect'} />
          </PieChart>
        );

      default:

        for (let i = 0; i < props.definition.length; i++) {

          items.push(
            <Bar key={i} dataKey={props.definition[i]} fill={colors[colorIndex]} />
          );

          colorIndex = colorIndex >= 21 ? 0 : colorIndex + 1;
        }

        return (
          <BarChart key={props.id} data={state.items}>
            <XAxis dataKey={'reportTime'} interval={0} />
            <YAxis />
            <Tooltip />
            <Legend />
            {items}
          </BarChart>
        );
    }
  }

  const removeWidget = () => {

    makeRequest({
      url: `/api/v1/widgets/${props.id}`,
      method: 'DELETE',
    }).then((res) => res.successful && window.location.reload());
  }

  const dropDownOptions = [{
    key: 'daily',
    text: 'Last Day',
    value: 'daily'
  }, {
    key: 'weekly',
    text: 'Last Week',
    value: 'weekly'
  }, {
    key: 'last7Days',
    text: 'Last 7 days',
    value: 'last7Days'
  }, {
    key: 'monthly',
    text: 'Last Month',
    value: 'monthly'
  }, {
    key: 'yearly',
    text: 'Last Year to Date',
    value: 'yearly'
  }, {
    key: null,
    text: <strong>Reset</strong>,
    value: null
  }];

  const handleDate = (timeSpan) => {

    switch (timeSpan) {
      case 'monthly':
        let [startDate, endDate] = getLastMonth();

        setState((prevState) => ({
          ...prevState,
          stateChange: true,
          startDate,
          endDate
        }));
        break;
      case 'last7Days':
        [startDate, endDate] = getLastSevenDays();
        setState((prevState) => ({
          ...prevState,
          stateChange: true,
          startDate,
          endDate
        }));
        break;
      case 'weekly':
        [startDate, endDate] = getLastWeek();
        setState((prevState) => ({
          ...prevState,
          stateChange: true,
          startDate,
          endDate
        }));
        break;
      case 'daily':
        setState((prevState) => ({
          ...prevState,
          stateChange: true,
          startDate: moment().format('YYYY-MM-DD'),
          endDate: moment().format('YYYY-MM-DD')
        }));
        break;
      case 'yearly':
        [startDate, endDate] = getYearToDate();
        setState((prevState) => ({
          ...prevState,
          stateChange: true,
          startDate,
          endDate
        }));
        break;

      default:
        break;
    }
  }

  return (
    <div style={{ display: 'flex' }}>
      <Card
        style={{
          minWidth: `${(state.items.length <= 7 || props.type === 'pie') ? '49%' : '100%'}`,
          margin: '10px 0px 10px 0',
          padding: '0 10px 10px',
          flexGrow: 1,
        }}>
        <Card.Content style={{ display: 'flex', flexDirection: 'row' }}>
          <Form.Field
            style={{ flex: '1' }}
            required={true}
            control={Select}
            options={dropDownOptions}
            placeholder='Range'
            onChange={(data, { value }) => handleDate(value)} />
          <Card.Header style={{ display: 'flex', alignContent: 'center', justifyContent: 'center', flex: '1' }}>{props.name}</Card.Header>
          {props.screenSize === 'desktop' &&
            <Button style={{ display: 'flex' }} color={'red'} icon={'times'} onClick={removeWidget} />
          }
        </Card.Content>
        <Card.Content>
          <ResponsiveContainer minHeight={330} maxHeight={350}>
            {getChart(props.type)}
          </ResponsiveContainer>
        </Card.Content>
      </Card>
    </div>
  );
}
