
import React from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import { Navigation } from '../../components/Navigation';

export const App = () => {

  const store = createStore(() => { })

  return (

    <Provider store={store}>
      <Navigation />
    </Provider>
  );
}