import React, { useEffect, useState } from 'react';
import { Button, Table, Grid, Form, Select, Card } from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import { getEndOfWeek, getLastMonth, getLastSevenDays, getLastWeek, getStartOfWeek, getYearToDate } from '../../utils/time';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';
import { ChartSelectionModal, ChartComponent } from './components';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';

import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

export const Dashboard = (props) => {

  const [startOfWeek, endOfWeek] = getLastSevenDays();

  const [state, setState] = useState({
    showModal: false,
    showCards: true,
    kpis: null,
    startDate: startOfWeek,
    endDate: endOfWeek,
    selectedBranch: null
  });

  const [makeRequest, data, loading, error] = useRequest();
  const [updateOrder, _data, _loading, _error] = useRequest();

  useEffect(() => {

    function getKPIs() {

      makeRequest({
        method: 'GET',
        url: '/api/v1/widgets/',
      }).then((response) => {
        response.successful &&

          setState(prevState => ({
            ...prevState,
            kpis: response?.data?.results?.sort(function (a, b) {
              return a.sequence - b.sequence;
            })
          }));
      });
    }

    getKPIs();


    async function getBranches() {

      makeRequest({
        method: 'GET',
        url: '/api/v1/branches/'
      })
        .then((response) => {

          response.successful &&

            setState(prevState => ({
              ...prevState,
              branches: response
            }));
        });
    }

    getBranches();

  }, [_data]);

  const getBranches = () => {

    const branches = [];

    if (state?.branches?.data?.results) {

      for (const branch of state?.branches?.data?.results) {

        branches.push({
          key: branch.id,
          text: branch.name,
          value: branch.id
        });
      }
    }

    branches.push({
      key: null,
      text: <strong>Show All</strong>,
      value: null
    });

    return branches;
  }

  const handleOnDragEnd = (result) => {
    if (!result.destination) {
      return;
    }

    const rawItems = Array.from(state.kpis);

    const items = rawItems.map((item) => item.id);
    const [reorderedItem] = items.splice((result.source.index - 1), 1);

    items.splice((result.destination.index - 1), 0, reorderedItem);

    updateOrder({
      method: 'PUT',
      url: `/api/v1/widgets/reorder/`,
      data: items
    });
  }

  const dropDownOptions = [{
    key: 'daily',
    text: 'Last Day',
    value: 'daily'
  }, {
    key: 'weekly',
    text: 'Last Week',
    value: 'weekly'
  }, {
    key: 'last7Days',
    text: 'Last 7 days',
    value: 'last7Days'
  }, {
    key: 'monthly',
    text: 'Last Month',
    value: 'monthly'
  }, {
    key: 'yearly',
    text: 'Last Year to Date',
    value: 'yearly'
  }, {
    key: null,
    text: <strong>Reset</strong>,
    value: null
  }];

  const handleDate = (timeSpan) => {

    switch (timeSpan) {
      case 'monthly':
        let [startDate, endDate] = getLastMonth();
        console.log(startDate, endDate)
        setState((prevState) => ({
          ...prevState,
          startDate,
          endDate
        }));
        break;
      case 'last7Days':
        [startDate, endDate] = getLastSevenDays();
        setState((prevState) => ({
          ...prevState,
          startDate,
          endDate
        }));
        break;
      case 'weekly':
        [startDate, endDate] = getLastWeek();
        setState((prevState) => ({
          ...prevState,
          startDate,
          endDate
        }));
        break;
      case 'daily':
        setState((prevState) => ({
          ...prevState,
          startDate: moment().format('YYYY-MM-DD'),
          endDate: moment().format('YYYY-MM-DD')
        }));
        break;
      case 'yearly':
        [startDate, endDate] = getYearToDate();
        setState((prevState) => ({
          ...prevState,
          startDate,
          endDate
        }));
        break;

      default:
        break;
    }
  }

  return (
    <>
      <Card fluid>


        <Card.Content extra>
          <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', flexWrap: 'wrap', gap: '10px' }}>
            <Form.Field
              style={{ minWidth: props.screenSize === 'mobi' && '87vw' }}
              required={true}
              control={Select}
              options={dropDownOptions}
              placeholder='Range'
              onChange={(data, { value }) => handleDate(value)} />
            <Form.Field
              style={{ minWidth: props.screenSize === 'mobi' && '87vw' }}
              required={true}
              control={Select}
              options={getBranches()}
              placeholder='Branch'
              onChange={(event, data) => {
                setState({
                  ...state,
                  selectedBranch: data.value
                })
              }} />
            <Button
              style={{ minWidth: props.screenSize === 'mobi' && '87vw' }}
              size={'small'}
              color={'teal'}
              content={'Add a new KPI chart'}
              icon={'add'}
              floated={'right'}
              labelPosition={'left'}
              onClick={() => setState(prevState => ({
                ...prevState,
                showModal: true
              }))} />
          </div>
        </Card.Content>
      </Card>


      {state.showCards && state.kpis &&
        <DragDropContext onDragEnd={handleOnDragEnd}>
          <Droppable droppableId="chartItems">
            {(provided) =>
              <ul style={{
                listStyleType: 'none',
                columnCount: state.kpis.length >= 2 && (props.screenSize !== 'desktop' ? 1 : 2),
                padding: 0,
              }} className={'chartItems'} {...provided.droppableProps} ref={provided.innerRef}>
                {state.kpis.map(({ id, sequence, name, widget_type, definition }, index) => {
                  return (
                    <Draggable key={sequence} draggableId={sequence.toString()} index={sequence}>
                      {(provided) => (
                        <li className={'fancySmancyList'} style={{ breakInside: 'avoid', WebkitColumnBreakInside: 'avoid' }} ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps}>
                          <ChartComponent
                            screenSize={props.screenSize}
                            index={index}
                            length={state.kpis.length}
                            definition={definition}
                            id={id}
                            key={id}
                            name={name}
                            from={state.startDate}
                            to={state.endDate}
                            branch={state.selectedBranch}
                            type={widget_type} />
                        </li>
                      )}
                    </Draggable>
                  );
                })}
                {provided.placeholder}
              </ul>
            }
          </Droppable>
        </DragDropContext>
      }

      {state.showModal &&

        <ChartSelectionModal
          showModal={state.showModal}
          onClose={() => setState(prevState => ({
            ...prevState,
            showModal: false
          }))} />
      }

      <ErrorMessages error={error} />
    </>
  )
};
