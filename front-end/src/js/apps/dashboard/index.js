import React from 'react'
import ReactDOM from "react-dom";
import { App } from "./App";
import '../../utils/sentry';

ReactDOM.render(<App />, document.getElementById("dashboard-app"));
