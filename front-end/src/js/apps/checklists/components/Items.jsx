import React from 'react'
import { Form, Button, Icon, Table, Segment, Input, Select, Label, Popup } from 'semantic-ui-react'

export const Items = (props) => {

  const dropDownOptions = [
    { key: 1, text: 'Range', value: 'range' },
    { key: 2, text: 'Checkbox', value: 'checkbox' },
  ]

  const weightsInfo = (
    <>
      <strong>Maximum Weight</strong> should be greather than&nbsp;
      <strong>Minimum weight</strong>. <br /><br /> NOTE: for fields with critical deviations, the&nbsp;
      <strong>Minimum Weight</strong> must be a negative value and&nbsp;
      the <strong>Maximum Weight</strong> must be zero .eg min=-10 and max=0. <br />
    </>
  );

  const minError = isNaN(props.item.min) || props.item.min === '' || props.item.min > props.item.max;
  const maxError = isNaN(props.item.max) || props.item.max === '' || props.item.min > props.item.max;

  return (
    <Table.Row>
      <Table.Cell>
        <Form.Field
          control={Input}
          placeholder='Sub Section'
          error={!props.item.subsection}
          value={props.item.subsection || ''}
          onChange={(event, data) => props.onPropertyValueChanged({ subsection: data.value })} />
      </Table.Cell>
      <Table.Cell>
        <Form.Field
          control={Input}
          placeholder='Property name'
          error={!props.item.name}
          value={props.item.name || ''}
          onChange={(event, data) => props.onPropertyValueChanged({ name: data.value })} />
      </Table.Cell>
      <Table.Cell>
        <Form.Field
          control={Input}
          placeholder='Description'
          error={!props.item.description}
          value={props.item.description}
          onChange={(event, data) => props.onPropertyValueChanged({ description: data.value })} />
      </Table.Cell>
      <Table.Cell>
        <Form.Field
          control={Input}
          error={minError}
          type='number'
          value={props.item.min}
          icon={<Popup
            content={weightsInfo}
            position='top center'
            trigger={<Icon link name='info' />} />}
          onChange={(event, data) => props.onPropertyValueChanged({ min: data.value })} />
      </Table.Cell>
      <Table.Cell>
        <Form.Field
          control={Input}
          error={maxError}
          value={props.item.max}
          type='number'
          icon={<Popup
            content={weightsInfo}
            position='top center'
            trigger={<Icon link name='info' />} />}
          onChange={(event, data) => props.onPropertyValueChanged({ max: data.value })} />

      </Table.Cell>
      <Table.Cell>
        <Form.Field
          disabled
          control={Select}
          options={dropDownOptions}
          value={props.item.item_type || 'range'}
          placeholder='Range'
          onChange={(event, data) => props.onPropertyValueChanged({ item_type: data.value })} />
      </Table.Cell>
      <Table.Cell >
        <Button.Group
          size='tiny'
          floated='right'>
          <Button
            icon
            color='blue'
            title='Move item up'
            onClick={() => props.onMoveUpClick(props.item, props.section)}>
            <Icon name='angle double up' />
          </Button>
          <Button
            icon
            color='red'
            title='Remove item'
            onClick={() => props.onRemoveClick(props.item, props.section)}>
            <Icon name='trash alternate outline' />
          </Button>
        </Button.Group>
      </Table.Cell>
    </Table.Row>
  );
}

