import React, { useState } from 'react'
import { Form, Button, Table, Segment, Input, Select, Checkbox, Tab, Label, Icon } from 'semantic-ui-react'
import _ from 'lodash';

export const ViewItems = (props) => {
  const [editing, setEditing] = useState(false);

  const handleScoreChange = (value) => {
    setEditing(false);
    props.onPropertyValueChanged({ value });
  };

  const overRangeLimit = Math.abs(props.item.max - props.item.min) > 16;

  return (
    <Table.Row>
      {!editing || props.item.item_type !== 'range'

        ? <>
          <Table.Cell>
            {props?.item?.subsection && props.item.subsection}
          </Table.Cell>
          <Table.Cell>
            {props?.item?.name && props.item.name}
          </Table.Cell>
          <Table.Cell>
            {props?.item?.description && props.item.description}
          </Table.Cell>
          <Table.Cell>
            {props.item.item_type === 'range' &&
              <Form.Input
                inline
                width={12}
                min={props.item.min}
                max={props.item.max}
                value={props.item.value}
                onChange={(event, data) => {
                  props.onPropertyValueChanged({ value: data.value })
                }}
                step={1}
                type='number'
                icon={!overRangeLimit && <Icon
                  link
                  name='pencil'
                  onClick={(event) => setEditing(true)} />} />
            }
            {
              props.item.item_type !== 'range' &&
              <Checkbox
                onChange={(event, data) => props.onPropertyValueChanged({ value: data.checked ? props.item.max : props.item.min })} />
            }
          </Table.Cell>
          <Table.Cell textAlign='center'>{props.item.min} <i>{'->'}</i> {props.item.max}</Table.Cell>
          <Table.Cell textAlign='right'>
            <Form.Field
              control={Input}
              value={props.item.comment || ''}
              placeholder='Comment'
              onChange={(event, data) => props.onPropertyValueChanged({ comment: data.value })} />
          </Table.Cell>
        </>
        :
        <Table.Cell
          colSpan={6}
          onMouseLeave={() => setEditing(false)}>
          <Button.Group
            fluid>
            {
              _.range(props.item.min, props.item.max + 1)
                .map(v => (
                  <Button
                    color={v > 0 ? 'green' : 'red'}
                    active={v === props.item.value}
                    onClick={() => handleScoreChange(v)}
                    key={v}
                    autoFocus={v === props.item.value}>
                    {v}
                  </Button>
                ))
            }
          </Button.Group>
        </Table.Cell>
      }
    </Table.Row >
  );
}

