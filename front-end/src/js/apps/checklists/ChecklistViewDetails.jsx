import React, { useState, useEffect } from 'react'
import { Link, useHistory, useParams } from 'react-router-dom'
import {} from 'react-dom';
import {
  Button,
  Checkbox,
  Icon,
  Progress,
  Table,
}
  from 'semantic-ui-react'
import axios from '../../utils/axios';

export const ChecklistViewDetails = () => {

  const { id } = useParams();

  const [state, setState] = useState({
    loading: false,
    checklist: null,
    showDetail: false
  });


  useEffect(() => {
    axios.get(`/api/v1/checklists/${id}/`)
      .then((response) => {
        setState({
          ...state,
          checklist: response.data
        })
      })
      .catch(function (error) {
        alert(error);
      });
  }, []);

  return (
    state.checklist &&
    <>
      <div style={{ textAlign: 'right' }}>
        <Button.Group
          size='tiny'>    
          <Button
            as={Link}
            color='blue'
            icon='pencil'
            to={`/checklists/${state.checklist.id}/edit`}
            title='Edit Checklist'/>
          <Button
            as='a'
            color='purple'
            icon='file pdf outline'
            href={`/checklists/${state.checklist.id}/pdf/?business_id=${BUSINESS_ID}`}/>
        </Button.Group>
      </div>
      <Progress
        percent={state.checklist.score}
        success={state.checklist.passed}
        error={!state.checklist.passed}
        size='small'>
        {state.checklist.score}% - {state.checklist.passed ? 'Passed!' : 'Failed'}
      </Progress>
      <Table color={state.checklist.passed ? 'green' : 'red'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell colSpan={3}>{state.checklist.name} - {state.checklist.description}</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell colSpan={3}>Comment: {state.checklist.comment}</Table.Cell>
          </Table.Row>
          <Table.Row>
            <Table.Cell>Branch: {state.checklist.branch.name}</Table.Cell>
            <Table.Cell>Date: {state.checklist.date_completed.substring(0, 10)}</Table.Cell>
            <Table.Cell>
              Score: {state.checklist.score}% <Icon color={state.checklist.passed ? 'green' : 'red'} name={state.checklist.passed ? 'checkmark' : 'closed'} />
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>

      <Checkbox label='Show detail'
        checked={state.showDetail}
        onChange={(data) => setState({ ...state, showDetail: !state.showDetail })} />

      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Section</Table.HeaderCell>
            <Table.HeaderCell>Item</Table.HeaderCell>
            <Table.HeaderCell>Description</Table.HeaderCell>
            <Table.HeaderCell>Comment</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Min</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Max</Table.HeaderCell>
            <Table.HeaderCell textAlign="right">Score</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {state.checklist.sections.map((s, index) => {
            const min_total = s.items.reduce((acc, i) => i.min + acc, 0);
            const max_total = s.items.reduce((acc, i) => i.max + acc, 0);
            const total_score = s.items.reduce((acc, i) => i.value + acc, 0);
            const pass_ratio = s.pass_score / 100;
            const pass_score = pass_ratio * max_total;
            const error = total_score < pass_score;


            return (
              <React.Fragment key={index}>
                <Table.Row title={`Minimum pass score for this section is ${s.pass_score}% [${pass_score}]`} active>
                  <Table.Cell>{s.name}</Table.Cell>
                  <Table.Cell>-</Table.Cell>
                  <Table.Cell>{s.description}</Table.Cell>
                  <Table.Cell>{s.comment}</Table.Cell>
                  <Table.Cell textAlign="right">{min_total}</Table.Cell>
                  <Table.Cell textAlign="right">{max_total}</Table.Cell>
                  <Table.Cell textAlign="right">
                    {total_score} &nbsp;<Icon color={error ? 'red' : 'green'} name={error ? 'close' : 'checkmark'} />
                  </Table.Cell>
                </Table.Row>

                {state.showDetail && s.items.map((i, index) => {
                  const error = i.value < i.max * pass_ratio

                  return (
                    <Table.Row key={`item-${index}`}>
                      <Table.Cell>{index + 1}</Table.Cell>
                      <Table.Cell>{i.name}</Table.Cell>
                      <Table.Cell>{i.description}</Table.Cell>
                      <Table.Cell>{i.comment}</Table.Cell>
                      <Table.Cell textAlign="right">{i.min}</Table.Cell>
                      <Table.Cell textAlign="right">{i.max}</Table.Cell>
                      <Table.Cell textAlign="right">
                        {i.value} &nbsp;<Icon color={error ? 'red' : 'green'} name={error ? 'close' : 'checkmark'} />
                      </Table.Cell>
                    </Table.Row>
                  )
                })}
              </React.Fragment>
            )
          })}
        </Table.Body>
      </Table>
    </>
  )
}
