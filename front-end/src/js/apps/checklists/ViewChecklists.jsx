import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { Button, Icon, Table, Pagination } from 'semantic-ui-react'
import axios from '../../utils/axios';
import { MobileView } from '../common';

export const ViewChecklists = (props) => {

  const [state, setState] = useState({
    data: null,
    activePage: 1
  });

  useEffect(() => {
    axios.get(`/api/v1/checklists/?page=${state.activePage}`)
      .then((response) => {
        setState(prevState => ({
          ...prevState,
          data: response
        }));
      })
      .catch(function (error) {
        alert(error);
      });
  }, [
    state.activePage,
  ]);

  const history = useHistory();

  const deleteChecklistItem = (checklistItem) => {
    if (!confirm('Are you sure?')) {
      return;
    }

    axios.delete(`/api/v1/checklists/${checklistItem.id}/`)
      .then(() => window.location.reload())
      .catch(function (error) {
        alert(error);
      });
  }

  const generatePDF = (checklistItem) => {
    return;
  }

  const getRows = () => {

    const response = state?.data;

    let rows = [];

    if (props.screenSize === 'desktop') {
      for (const item of response?.data?.results || []) {

        rows.push(

          <Table.Row key={item.id}>
            <Table.Cell>{item.name}</Table.Cell>
            <Table.Cell>
              <Link to={`/branches/${item.branch.id}`} target="_blank">{item.branch.name}</Link>
            </Table.Cell>
            <Table.Cell>{new Date(item.start_date).toISOString().substring(0, 10)}</Table.Cell>
            <Table.Cell>{new Date(item.date_completed).toISOString().substring(0, 10)}</Table.Cell>
            <Table.Cell>
              {item.score}% <Icon name={item.passed ? 'check' : 'times'} color={item.passed ? 'green' : 'red'} />
            </Table.Cell>
            <Table.Cell>{item.comment}</Table.Cell>
            <Table.Cell singleLine>
              <Button.Group size='tiny'>
                <Button icon color='green' as={Link} to={`/checklists/${item.id}`}>
                  <Icon name='eye' />
                </Button>
                <Button icon color='blue' as={Link} to={`/checklists/${item.id}/edit`}>
                  <Icon name='pencil' />
                </Button>
                <Button icon color='red' onClick={() => deleteChecklistItem(item)}>
                  <Icon name='trash alternate outline' />
                </Button>
                <Button
                  as='a'
                  color='purple'
                  icon='file pdf outline'
                  href={`/checklists/${item.id}/pdf/?business_id=${BUSINESS_ID}`} />
              </Button.Group>
            </Table.Cell>
          </Table.Row>

        );
      }

      return rows;
    }

    for (const item of response?.data?.results || []) {

      rows.push({
        heading: item.name,
        items: [{
          icon: 'code branch',
          content: <Link to={`/branches/${item.branch.id}`} target="_blank">{item.branch.name}</Link>
        }, {
          icon: 'calendar',
          content: `Date Started: ${new Date(item.start_date).toISOString().substring(0, 10)}`
        }, {
          icon: 'calendar',
          content: `Date Completed: ${new Date(item.date_completed).toISOString().substring(0, 10)}`
        }],
        buttons: [{
          color: 'green',
          content: 'View',
          icon: 'eye',
          link: `/checklists/${item.id}`
        }, {
          color: 'blue',
          content: 'Edit',
          icon: 'pencil',
          link: `/checklists/${item.id}/edit`
        }]
      });
    }

    return rows;
  }

  const rows = getRows();

  return (
    <>
      {props.screenSize === 'desktop'
        ?
        <Table color={'purple'} celled>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Checklist Name</Table.HeaderCell>
              <Table.HeaderCell>Branch</Table.HeaderCell>
              <Table.HeaderCell>Start Date</Table.HeaderCell>
              <Table.HeaderCell>Completed Date</Table.HeaderCell>
              <Table.HeaderCell>Score</Table.HeaderCell>
              <Table.HeaderCell>Comment</Table.HeaderCell>
              <Table.HeaderCell>View</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {rows}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan='7'>
                <Pagination
                  floated='right'
                  activePage={state.activePage}
                  ellipsisItem={null}
                  siblingRange={2}
                  totalPages={(state?.data?.data && Math.ceil(state.data.data.count / 10)) || 1}
                  onPageChange={(event, data) => setState(prevState => ({
                    ...prevState,
                    activePage: data.activePage
                  }))} />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
        :
        <>
          <MobileView
            heading={'Checklists'}
            list={rows} />

          <br />

          <Pagination
            fluid
            activePage={state.activePage}
            ellipsisItem={null}
            siblingRange={2}
            totalPages={(state?.data?.data && Math.ceil(state.data.data.count / 10)) || 1}
            onPageChange={(event, data) => setState(prevState => ({
              ...prevState,
              activePage: data.activePage
            }))} />

        </>
      }
    </>
  )
}
