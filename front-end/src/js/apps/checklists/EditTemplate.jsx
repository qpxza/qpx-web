import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Form, Button, Table, Input, Label } from 'semantic-ui-react'
import axios from '../../utils/axios';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';
import { Items } from './components/Items';
import { toast } from 'react-toastify';

export const EditTemplate = () => {

  const [state, setState] = useState({
    name: '',
    pass_score: 50.0,
    sections: [],
  });

  const history = useHistory();

  const { id } = useParams();

  const [updateChecklist, _checklist, updating, error] = useRequest();

  const addSection = () => {

    const section = {
      name: '',
      description: '',
      // provide a default item for the user under the new section
      items: [{
        subsection: '',
        name: '',
        description: '',
        item_type: 'range',
        min: '',
        max: '',
      }],
      pass_score: 50,
    };

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.concat(section),
    }));
  };

  const moveUp = (item, items) => {
    const index = items.indexOf(item);
    const newIndex = Math.max(index - 1, 0);

    if (newIndex < 0) {
      return;
    }

    items[index] = items[newIndex];
    items[newIndex] = item;

    return items;
  };

  const moveSectionUp = (section) => {
    setState((prevState) => ({
      ...prevState,
      sections: moveUp(section, prevState.sections).map(s => s),
    }));
  };

  const removeSection = (section) => {
    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.filter(s => s != section),
    }));
  };

  const addItem = (section) => {

    section.items.push({
      subsection: section.items.length > 0 ? section.items[section.items.length - 1].subsection : '',
      name: '',
      description: '',
      item_type: 'range',
      min: '',
      max: '',
    });

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.map(s => s),
    }));
  }

  const moveItemUp = (item, section) => {
    section.items = moveUp(item, section.items).map(s => s);

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.map(s => s),
    }));
  };

  const removeItem = (item, section) => {
    section.items.splice(section.items.indexOf(item), 1);

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.map(section => section),
    }));
  }

  const saveForm = () => {
    if (!state.name) {
      toast.error('Your form requires a name.');
      return;
    }

    if (state.sections.length === 0) {
      toast.error('You need to have at least 1 section.');

      return;
    }

    const invalidItems = {
      subsection: '',
      name: '',
      description: '',
      item_type: '',
      min: '',
      max: ''
    }

    for (const section of state.sections) {

      if (section.items.length === 0) {
        toast.error(`You need to have at least 1 item in section "${section.name}"`)
        return;
      }

      for (const { min, max, name } of section.items) {

        if (isNaN(min) || isNaN(max) || min > max) {
          toast.error(`Item "${name}" error: minimum weight must not be greater than maximum weight`);
          return;
        }
      }

      for (const [key, value] of Object.entries(section)) {
        if (value === invalidItems[key]) {

          toast.error('All field need to be completed');
          return;
        }
      }
    }

    const params = {
      id: id,
      name: state.name,
      pass_score: state.pass_score,
      sections: state.sections
    };
    const promise = !id
      ? updateChecklist({ url: '/api/v1/checklists/templates/', data: params, method: 'post' }) // new checklist template
      : updateChecklist({ url: `/api/v1/checklists/templates/${id}/`, data: params, method: 'put' }); // existing checklist template

    promise.then((res) => res.successful && history.push('/checklists/templates/'));
  }

  const renderItems = (section) => section.items.map((item, index) => (

    <Items
      key={index}
      section={section}
      item={item}
      onMoveUpClick={moveItemUp}
      onRemoveClick={removeItem}
      onPropertyValueChanged={(data) => {

        Object.assign(item, data);

        setState({
          ...state,
          sections: state.sections.map(s => s)
        });
      }} />
  ));

  const renderSections = () => state.sections.map((section, index) => (

    <React.Fragment key={index}>
      <Table.Row active>
        <Table.Cell>
          <Form.Field
            control={Input}
            placeholder='Section Name'
            error={!section.name}
            value={section.name}
            onChange={(event, data) => {

              Object.assign(section, { name: data.value });

              setState({
                ...state,
                sections: state.sections.map(section => section)
              })
            }} />

        </Table.Cell>
        <Table.Cell
          colSpan='2'>
          <Form.Field
            control={Input}
            error={!section.description}
            value={section.description}
            placeholder='Description'
            onChange={(event, data) => {

              Object.assign(section, { description: data.value });

              setState({
                ...state,
                sections: state.sections.map(section => section)
              })
            }} />
        </Table.Cell>
        <Table.Cell singleLine colSpan='7'>
          <Button.Group
            size='tiny'
            floated='right'>
            <Button
              icon='angle double up'
              color='grey'
              title='Move section up'
              onClick={() => moveSectionUp(section)} />
            <Button
              color='red'
              title='Remove section'
              floated='right'
              icon='trash alternate outline'
              size='tiny'
              onClick={() => removeSection(section)} />
          </Button.Group>
        </Table.Cell>
      </Table.Row>
      <Table.Row>
        <Table.Cell>Sub Section</Table.Cell>
        <Table.Cell>Name</Table.Cell>
        <Table.Cell>Description</Table.Cell>
        <Table.Cell>Minimum Weight</Table.Cell>
        <Table.Cell>Maximum Weight</Table.Cell>
        <Table.Cell>Type</Table.Cell>
        <Table.Cell>
          <Button
            color='green'
            icon='add'
            size='tiny'
            floated='right'
            title='Create New Item'
            onClick={() => addItem(section)} />
        </Table.Cell>
      </Table.Row>

      {renderItems(section)}

    </React.Fragment>
  ));

  const addDefaults = () => {
    // add a default section with a single item
    setTimeout(addSection, 0);
    // hiding a potential race condition bug here but I won't bother looking into react's state management
    setTimeout(() => state.sections.length && addItem(state.sections[0]),);
  }

  useEffect(function () {
    function getChecklist() {

      axios.get(`/api/v1/checklists/templates/${id}/`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            name: response.data.name,
            pass_score: response.data.pass_score,
            sections: response?.data?.sections
          }));

        })
        .catch(function (error) {
          alert(error);
        });
    }

    if (id) {
      getChecklist();
    } else {
      addDefaults();
    }
  }, []);

  return (
    <Form
      size='tiny'
      loading={updating}
      error={!!error}>
      <Table color={'purple'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell colSpan='5'>{id ? 'Edit' : 'Create'} Checklist Template</Table.HeaderCell>
            <Table.HeaderCell colSpan='2'>
              <Form.Input
                fluid
                labelPosition='left'
                min={0}
                max={100}
                name='hide'
                error={isNaN(state.pass_score)}
                onChange={(event, data) => {
                  setState({
                    ...state,
                    pass_score: data.value
                  });
                }}
                step={1}
                type='number'
                value={state.pass_score}>
                <Label>Minimum score required(%): </Label>
                <input />
              </Form.Input>
            </Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            <Table.HeaderCell colSpan='7'>
              <Form.Field
                control={Input}
                placeholder='Template name'
                error={!state.name}
                value={state.name}
                labelPosition='left'
                onChange={(event, data) => {
                  setState({
                    ...state,
                    name: data.value
                  })
                }}>
                <Label>Checklist template name: </Label>
                <input />
              </Form.Field>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {renderSections()}
        </Table.Body>


        <Table.Footer >
          <Table.Row>
            <Table.HeaderCell colSpan='7'>
              <Button
                color='green'
                content='Create New Section'
                floated='left'
                icon='add'
                labelPosition='left'
                size='tiny'
                onClick={addSection} />

              <Button
                color='green'
                content='Submit Checklist'
                floated='right'
                icon='save'
                labelPosition='left'
                size='tiny'
                onClick={saveForm} />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <ErrorMessages error={error} />
    </Form >
  )
}
