import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Form, Button, Table, Input, Select } from 'semantic-ui-react'
import { useRequest } from '../../utils';
import axios from '../../utils/axios';
import { useQuery } from '../../utils/hooks';
import { ErrorMessages } from '../common/ErrorMessages';
import { ViewItems } from './components/ViewItems';

export const CompleteChecklist = () => {

  const [state, setState] = useState({
    name: null,
    comment: '',
    pass_score: 0,
    sections: [],
    branches: null,
    selectedBranch: null,
    template: null,
  });

  const id = useQuery().get('template_id');
  const history = useHistory();
  const [createChecklist, checklist, creating, error] = useRequest();

  useEffect(() => {

    function getChecklist() {

      axios.get(`/api/v1/checklists/templates/${id}/`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            name: response.data?.name,
            description: response.data?.description,
            pass_score: response.data?.pass_score,
            sections: response.data?.sections,
            template: response.data,
          }));

        })
        .catch(function (error) {
          alert(error);
        });
    }

    getChecklist();

    async function getBranches() {

      axios.get('/api/v1/branches/')
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            branches: response
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranches();

  }, []);

  const getDraftKey = () => {
    return location.href + state.template?.updated_at;
  };

  useEffect(() => {
    function restoreDraft() {
      if (!state.template) {
        return;
      }

      const key = getDraftKey();
      const draft = localStorage.getItem(key)

      if (!draft) {
        return;
      }

      if (confirm('We found a draft for this checklist. Do you want to continue were you left off?')) {
        setState(prevState => ({ ...prevState, ...JSON.parse(draft) }));
      }

      localStorage.removeItem(key);
    }

    restoreDraft();
  }, [state.template]);

  const getBranches = () => {

    const branches = [];

    if (state?.branches?.data?.results) {

      for (const branch of state?.branches?.data?.results) {

        branches.push({
          key: branch.id,
          text: branch.name,
          value: branch.id
        });
      }
    }

    return branches;
  }

  const removeItem = (item) => {

    const sections = state.sections;

    for (const section of sections) {

      const index = section.items.indexOf(item);

      if (index > -1) {
        section.items.splice(index, 1);
      }
    }

    setState(prevState => ({
      ...prevState,
      sections: state.sections.map(section => section),
    }));
  }


  const saveDraft = () => {
    const draft = {
      sections: state.sections,
      selectedBranch: state.selectedBranch,
      comment: state.comment,
      pass_score: state.pass_score,
    };

    localStorage.setItem(getDraftKey(), JSON.stringify(draft));
    alert('Draft succesfully saved!')
  };

  const saveForm = () => {

    if (state.name === null) {
      alert('Your form requires a name.');

      return;
    }

    if (state.sections.length === 0) {
      alert('You need to have at least 1 section.');

      return;
    }

    const invalidItems = {
      name: '',
      description: '',
      subsection: '',
      item_type: '',
      min: '',
      max: ''
    }

    for (const section of state.sections) {

      if (section.items.length === 0) {
        alert('You need to have at least 1 item in a section.')
        return;
      }

      for (const [key, value] of Object.entries(section)) {
        if (value === invalidItems[key]) {

          alert('All fields need to be completed');
          return;
        }
      }
    }

    const payload = {
      name: state.name,
      template: id,
      branch: state.selectedBranch,
      date_completed: new Date().toISOString(),
      start_date: new Date().toISOString(),
      comment: state.comment,
      sections: state.sections,
      description: 'Checklist description',
      pass_score: state.pass_score,
    };

    createChecklist({
      url: '/api/v1/checklists/',
      method: 'POST',
      data: payload,
    }).then((res) => res.successful && history.push('/checklists/'));
  };

  const renderItems = (items) => items.map((item, i) => (

    <ViewItems
      key={i}
      item={item}
      onRemoveClick={removeItem}
      onPropertyValueChanged={(data) => {

        Object.assign(item, data);

        setState({
          ...state,
          sections: state.sections.map(section => section)
        })
      }} />
  ));

  const renderSections = () => state.sections.map((section, i) => (
    <Table.Body key={i}>
      <Table.Row active>
        <Table.Cell>
          {section.name}
        </Table.Cell>
        <Table.Cell>
          {section.description}
        </Table.Cell>
        <Table.Cell colSpan={'4'} >
          <Form.Input
            placeholder='Comment'
            value={section.comment || ''}
            onChange={(event, data) => {
              section.comment = data.value;

              setState({
                ...state,
                sections: state.sections.map(section => section)
              });
            }} />
        </Table.Cell>
      </Table.Row>
      {renderItems(section.items)}

    </Table.Body>
  ));

  return (

    <Form size='mini' error={!!error}>
      <Table color={'purple'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Complete a {state.name} Checklist</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <Form.Group>
                <Form.Field
                  width={8}
                  required={true}
                  control={Select}
                  options={getBranches()}
                  placeholder='Branch'
                  onChange={(event, data) => {
                    setState({
                      ...state,
                      selectedBranch: data.value
                    })
                  }} />
                <Form.Field
                  width={8}
                  required={true}
                  control={Input}
                  placeholder='Comment'
                  value={state.comment}
                  onChange={(event, data) => {
                    setState({
                      ...state,
                      comment: data.value
                    })
                  }} />
              </Form.Group>
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>

      <Table>
        <Table.Header>
          <Table.Row active>
            <Table.HeaderCell>Sub Section</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Description</Table.HeaderCell>
            <Table.HeaderCell>Score</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
            <Table.HeaderCell colSpan='2'>Comment</Table.HeaderCell>

          </Table.Row>
        </Table.Header>
        {renderSections()}
        <Table.Footer>
          <Table.Row>
            <Table.Cell colSpan='7'>
              <Button
                type='button'
                size='tiny'
                color='green'
                content='Submit Checklist'
                floated='right'
                icon='save'
                labelPosition='left'
                loading={creating}
                onClick={saveForm} />
              <Button
                size='tiny'
                color='blue'
                content='Save draft'
                floated='right'
                icon='save'
                labelPosition='left'
                onClick={saveDraft} />
            </Table.Cell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <ErrorMessages error={error} />
    </Form>
  )
}
