import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Form, Button, Table, Input, Select } from 'semantic-ui-react'
import axios from '../../utils/axios';
import { useRequest } from '../../utils/hooks'; 
import { ViewItems } from './components/ViewItems';
import { ErrorMessages } from '../common/ErrorMessages';

export const EditChecklist = () => {

  const [state, setState] = useState({
    name: null,
    template: null,
    sections: [],
    branches: null,
    selectedBranch: null,
    pass_score: 0,
  });

  const { id } = useParams();
  const history = useHistory();
  const [updateChecklist, checklist, updating, error] = useRequest();
  const [getTemplate, template, loadingTemplate, templateError] = useRequest();

  useEffect(() => {

    function getChecklist() {

      getTemplate({url: `/api/v1/checklists/${id}/`})
        .then((response) => {
          response.successful && setState(prevState => ({
            ...prevState,
            name: response.data?.name,
            template: response.data?.template,
            comment: response.data?.comment,
            selectedBranch: response.data?.branch?.id,
            description: response.data?.description,
            pass_score: response.data?.pass_score,
            sections: response.data?.sections
          }));

        });
    }

    getChecklist();

    async function getBranches() {

      axios.get('/api/v1/branches/')
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            branches: response
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranches();

  }, []);

  const getBranches = () => {

    const branches = [];

    if (state?.branches?.data?.results) {

      for (const branch of state?.branches?.data?.results) {

        branches.push({
          key: branch.id,
          text: branch.name,
          value: branch.id
        });
      }
    }

    return branches;
  }

  const removeItem = (item) => {

    const sections = state.sections;

    for (const section of sections) {

      const index = section.items.indexOf(item);

      if (index > -1) {
        section.items.splice(index, 1);
      }
    }

    setState((prevState) => ({
      ...prevState,
      sections: state.sections.map(section => section),
    }));
  }

  const saveForm = () => {

    if (state.name === null) {
      alert('Your form requires a name.');

      return;
    }

    if (state.sections.length === 0) {
      alert('You need to have at least 1 section.');

      return;
    }

    const invalidItems = {
      name: '',
      description: '',
      subsection: '',
      item_type: '',
      min: '',
      max: ''
    }

    for (const section of state.sections) {

      if (section.items.length === 0) {
        alert('You need to have at least 1 item in a section.')
        return;
      }

      for (const [key, value] of Object.entries(section)) {
        if (value === invalidItems[key]) {

          alert('All fields need to be completed');
          return;
        }
      }
    }

    const payload = {
      name: state.name,
      template: state.template,
      branch: state.selectedBranch,
      date_completed: new Date().toISOString(),
      start_date: new Date().toISOString(),
      comment: state.comment,
      sections: state.sections,
      description: 'Checklist description',
      pass_score: state.pass_score,
    };

    updateChecklist({ url: `/api/v1/checklists/${id}/`, method: 'PUT', data: payload })
      .then((res) => res.successful && history.push('/checklists/'))
  }

  const renderItems = (items) => items.map((item, i) => (

    <ViewItems
      item={item}
      onRemoveClick={removeItem}
      key={i}
      onPropertyValueChanged={(data) => {

        Object.assign(item, data);

        setState({
          ...state,
          sections: state.sections.map(section => section)
        })
      }} />
  ));

  const renderSections = () => state.sections.map((section, i) => (

    <Table.Body key={i}>
      <Table.Row active>
        <Table.Cell>
          {section.name}
        </Table.Cell>
        <Table.Cell colSpan='4'>
          {section.description}
        </Table.Cell>
        <Table.Cell textAlign='right'>
          <Form.Input
            placeholder='Comment'
            defaultValue={section.comment}
            onChange={(event, data) => {

              const item = section;
              item.comment = data.value;

              setState({
                ...state,
                sections: state.sections.map(section => section)
              });
            }} />
        </Table.Cell>
      </Table.Row>
      {renderItems(section.items)}

    </Table.Body>
  ));

  return (

    <Form size='tiny' error={!!error} loading={updating || loadingTemplate}>
      <Table color={'purple'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Complete a {state.name} Checklist</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <Form.Group>
                <Form.Field
                  width={8}
                  required={true}
                  control={Select}
                  options={getBranches()}
                  value={state.selectedBranch}
                  onChange={(event, data) => {
                    setState({
                      ...state,
                      selectedBranch: data.value
                    })
                  }} />
                <Form.Field
                  width={8}
                  required={true}
                  control={Input}
                  placeholder='Comment'
                  defaultValue={state?.comment}
                  onChange={(event, data) => {
                    setState({
                      ...state,
                      comment: data.value
                    })
                  }} />
              </Form.Group>
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>

      <Table>
        <Table.Header>
          <Table.Row active>
            <Table.HeaderCell>Sub Section</Table.HeaderCell>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Description</Table.HeaderCell>
            <Table.HeaderCell>Score</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
            <Table.HeaderCell colSpan='2'>Comment</Table.HeaderCell>

          </Table.Row>
        </Table.Header>
        {renderSections()}
        <Table.Footer>
          <Table.Row>
            <Table.Cell colSpan='7'>
              <Button
                size='tiny'
                color='green'
                content='Submit Checklist'
                floated='right'
                icon='save'
                labelPosition='left'
                loading={updating || loadingTemplate}
                onClick={saveForm} />
            </Table.Cell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <ErrorMessages error={error || templateError}/>
    </Form>
  )
}
