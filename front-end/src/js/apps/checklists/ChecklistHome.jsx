import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Icon, Table, Pagination, List, } from 'semantic-ui-react';
import { localTime } from '../../utils/time';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';
import { MobileView } from '../common';

export const ChecklistHome = (props) => {

  const [activePage, setActivePage] = useState(1);

  const [makeRequest, data, loading, error] = useRequest();

  useEffect(
    () => makeRequest({ url: `/api/v1/checklists/templates/?page=${activePage}` }),
    [activePage]
  );

  const history = useHistory();

  const getRows = () => {

    let rows = [];
    if (props.screenSize === 'desktop') {
      for (const item of data?.results || []) {

        rows.push(
          <Table.Row key={item.id}>
            <Table.Cell>{item.name}</Table.Cell>
            <Table.Cell title={localTime(item.created_at)}>{localTime(item.created_at)}</Table.Cell>
            <Table.Cell title={localTime(item.updated_at)}>{localTime(item.updated_at)}</Table.Cell>
            <Table.Cell collapsing>
              <Button.Group size='tiny'>
                <Button
                  as={Link}
                  to={`/checklists/templates/${item.id}/edit`}
                  color='blue'
                  icon='pencil' />
                <Button
                  as={Link}
                  to={`/checklists/create?template_id=${item.id}`}
                  color='green'
                  icon='check' />
                <Button
                  color='red'
                  icon='times'
                  onClick={() => deleteChecklistItem(item)} />
              </Button.Group>
            </Table.Cell>
          </Table.Row>
        );
      }

      return rows;
    }

    for (const item of data?.results || []) {

      rows.push({
        heading: item.name,
        items: [{
          icon: 'calendar',
          content: `Date Created: ${localTime(item.created_at)}`
        }, {
          icon: 'calendar',
          content: `Date Updated: ${localTime(item.updated_at)}`
        }],
        buttons: [{
          color: 'blue',
          content: 'Edit',
          icon: 'add',
          link: `/checklists/templates/${item.id}/edit`
        }, {
          color: 'green',
          content: 'Complete',
          icon: 'check',
          link: `/checklists/create?template_id=${item.id}`
        }]
      });

    }

    return rows;
  }

  const rows = getRows();

  const deleteChecklistItem = (checklistItem) => {
    if (!confirm('Are you sure?')) {
      return;
    }

    const params = {
      url: `/api/v1/checklists/templates/${checklistItem.id}/`,
      method: 'delete',
    };

    makeRequest(params)
      .then((res) => res.successful && window.location.reload());
  }

  return (
    <>
      {props.screenSize === 'desktop'
        ?
        <>
          <Table color={'purple'} celled>
            <Table.Header fullWidth>
              <Table.Row>
                <Table.HeaderCell>Checklist Name</Table.HeaderCell>
                <Table.HeaderCell>Created On</Table.HeaderCell>
                <Table.HeaderCell>Last Updated</Table.HeaderCell>
                <Table.HeaderCell>Action</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {rows}
            </Table.Body>

            <Table.Footer fullWidth>
              <Table.Row>
                <Table.HeaderCell colSpan='1'>
                  <Button
                    as={Link}
                    size='tiny'
                    color='teal'
                    content='Create New Template'
                    icon='add'
                    labelPosition='left'
                    to={'/checklists/templates/create'} />

                </Table.HeaderCell>
                <Table.HeaderCell colSpan='6'>
                  <Pagination
                    size='tiny'
                    floated='right'
                    activePage={activePage}
                    ellipsisItem={null}
                    siblingRange={2}
                    totalPages={Math.ceil((data?.count || 10) / 10)}
                    onPageChange={(event, data) => setActivePage(data.activePage)} />
                </Table.HeaderCell>
              </Table.Row>
            </Table.Footer>
          </Table>
          <ErrorMessages error={error} />
        </>

        : <MobileView
          heading={'Checklist Templates'}
          list={rows}
          buttons={[{
            color: 'teal',
            content: 'Create New Template',
            icon: 'add',
            link: '/checklists/templates/create'
          }]} />
      }
    </>
  )
};
