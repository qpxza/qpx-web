import React, { useEffect, useState } from 'react'
import {
  Button,
  Form,
  Icon,
  Input,
  Select,
  Segment,
} from 'semantic-ui-react'
import axios from '../../utils/axios';
import { useHistory } from 'react-router-dom';
import { create, isEqual } from 'lodash'
import { useRequest } from '../../utils';
import { ErrorMessages } from '../common/ErrorMessages';


export const CreateUser = () => {

  const history = useHistory();

  const defaultState = {
    email: '',
    firstName: '',
    surname: '',
    cellNumber: '',
    idNumber: '',
    role: null,
    roleItems: [],
    roleList: [],
  }

  const [state, setState] = useState({
    ...defaultState
  });

  const [createUser, user, creating, error] = useRequest();

  useEffect(() => {
    function getRoles() {

      axios.get('/api/v1/groups/')
        .then((response) => {

          formatRoles(response);
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getRoles();

  }, [])

  const formatRoles = (response) => {

    let dropdownItems = [];

    for (const item of response?.data?.results) {
      dropdownItems.push({
        key: item.id,
        value: item.id,
        text: item.name
      })
    }

    setState(prevState => ({
      ...prevState,
      roleItems: dropdownItems
    }));
  }

  const registerUser = () => {

    if (isEqual(state, defaultState)) {

      return;
    }

    const params = {
      username: state.email,
      email: state.email,
      first_name: state.firstName,
      last_name: state.surname,
      cell_number: state.cellNumber,
      id_number: state.idNumber,
      groups: [state.role]
    };

    createUser({ url: '/api/v1/users/', data: params, method: 'POST' })
      .then((res) => res.successful && history.push('/users'))
      .catch(function (error) {
        alert(error);
      });
  }

  return (
    <Segment>
      <Form
        size='tiny'
        error={!!error}
        loading={creating}>
        <Form.Group widths={'equal'}>
          <Form.Field
            required={true}
            control={Input}
            label='First name'
            placeholder='First name'
            onChange={(event, data) => setState({
              ...state,
              firstName: data.value
            })} />
          <Form.Field
            required={true}
            control={Input}
            label='Last name'
            placeholder='Last name'
            onChange={(event, data) => setState({
              ...state,
              surname: data.value
            })} />
        </Form.Group>

        <Form.Group widths={'equal'}>
          <Form.Input
            required={true}
            control={Input}
            label='Email'
            placeholder='Email'
            onChange={(event, data) => setState({
              ...state,
              email: data.value
            })} />
          <Form.Field
            required={true}
            control={Input}
            label='Cell Number'
            placeholder='Cell Number'
            onChange={(event, data) => setState({
              ...state,
              cellNumber: data.value
            })} />
        </Form.Group>

        <Form.Group>
          <Form.Field
            required={true}
            control={Input}
            label='ID Number'
            placeholder='ID Number'
            width={8}
            onChange={(event, data) => setState({
              ...state,
              idNumber: data.value
            })} />

          <Form.Field
            required={true}
            control={Select}
            label='Role'
            options={state.roleItems}
            placeholder='Role'
            onChange={(event, data) => setState({
              ...state,
              role: data.value
            })} />

        </Form.Group>

        <Form.Field>
          <br />
          <Button
            icon
            labelPosition='left'
            primary
            size='tiny'
            onClick={registerUser}>

            <Icon name='user' />
              Submit
          </Button>
        </Form.Field>
        <ErrorMessages error={error} />
      </Form>
    </Segment>
  );
};
