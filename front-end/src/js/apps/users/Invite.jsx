import React, { useEffect, useState } from 'react'
import {
  Button,
  Form,
  Grid,
  Message,
  Segment,
} from 'semantic-ui-react'
import { useRequest } from '../../utils';
import { validateEmail } from '../../utils/helpers';
import { ErrorMessages } from '../common/ErrorMessages';
import {toast} from 'react-toastify';

export const Invite = () => {

  const [email, setEmail] = useState('');

  const [sendInvite, _, loading, error] = useRequest();

  const invite = () => {

    const data = { email };
  
    sendInvite({url: '/api/v1/users/invite/', method: 'POST', data})
      .then((res) => {
        if(res.successful){
          setEmail('');
          toast.success('Invitation sent!');
        }
      })
  };

  return (
    <Segment>
      <Message
        icon='info'
        header='Invite a user to your business'
        content='Ensure that the user you are attempting to invite is an existing QPX user' />
      <Form size='tiny'
        error={!!error}
        loading={loading}
        onSubmit={invite}>
        <Form.Input
          error={validateEmail(email)}
          loading={loading}
          icon='mail'
          iconPosition='left'
          floated='center'
          label='Email address'
          onChange={(_, data) => setEmail(data.value)}
          placeholder='anne@qpx.co.za' />
        <Form.Button size='tiny' content='Invite' color='green' />
        <ErrorMessages error={error} />
      </Form>
    </Segment>
  );
};
