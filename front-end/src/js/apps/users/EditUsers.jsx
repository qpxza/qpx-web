import React, { useEffect, useState } from 'react'
import {
  Button,
  Form,
  Icon,
  Input,
  List,
  Message,
  Segment,
  Select,
} from 'semantic-ui-react'
import axios from '../../utils/axios';
import { useHistory, useParams } from 'react-router-dom';
import { useRequest } from '../../utils';
import { ErrorMessages } from '../common/ErrorMessages';

export const EditUser = () => {

  const { id } = useParams();

  const history = useHistory();

  const defaultState = {
    email: null,
    firstName: null,
    surname: null,
    cellNumber: null,
    idNumber: null,
    role: null,
    roleItems: [],
    showPermissions: false,
  }

  const [state, setState] = useState({
    ...defaultState,
    userDetails: null
  });

  const [updateUser, user, updating, error] = useRequest();

  useEffect(() => {

    function getUsers() {

      axios.get(`/api/v1/users/${id}`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            userDetails: response
          }));

          formatRole(response?.data?.groups[0]);
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getUsers();

  }, [])

  useEffect(() => {
    function getRoles() {
      axios.get('/api/v1/groups/')
        .then((response) => {
          formatRoles(response);
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getRoles();

  }, [])

  const formatRole = (response) => {
    if (!response) {
      return;
    }

    let dropdownItems = [];

    dropdownItems.push({
      key: response.id,
      value: response.id,
      text: response.name,
    });

    setState(prevState => ({
      ...prevState,
      role: response.id,
    }));

  }

  const formatRoles = (response) => {

    let dropdownItems = [];

    for (const item of response?.data?.results) {
      dropdownItems.push({
        key: item.id,
        value: item.id,
        text: item.name,
        permissions: item.permissions,
      })
    }

    setState(prevState => ({
      ...prevState,
      roleItems: dropdownItems
    }));
  }

  const registerUser = () => {
    const params = {
      username: state.email || state?.userDetails?.data?.email,
      email: state.email || state?.userDetails?.data?.email,
      first_name: state.firstName || state?.userDetails?.data?.first_name,
      last_name: state.surname || state?.userDetails?.data?.last_name,
      cell_number: state.cellNumber || state?.userDetails?.data?.cell_number,
      id_number: state.idNumber || state?.userDetails?.data?.id_number,
      groups: [state.role] || state?.userDetails?.data?.groups[0].id,
    };

    updateUser({ url: `/api/v1/users/${id}/`, data: params, method: 'PUT' })
      .then((res) => res.successful && history.push('/users'))
      .catch(function (error) {
        alert(error);
      });
  }

  const role = state.roleItems.find(i => i.value === state.role);

  return (
    <Segment>
      <Form
        size='tiny'
        error={!!error}
        loading={updating}>
        <Form.Group widths={'equal'}>
          <Form.Field
            required={true}
            control={Input}
            label='First name'
            placeholder='First name'
            defaultValue={state?.userDetails?.data?.first_name}
            onChange={(event, data) => setState({
              ...state,
              firstName: data.value
            })} />
          <Form.Field
            required={true}
            control={Input}
            label='Last name'
            placeholder='Last name'
            defaultValue={state?.userDetails?.data?.last_name}
            onChange={(event, data) => setState({
              ...state,
              surname: data.value
            })} />
        </Form.Group>

        <Form.Group widths={'equal'}>
          <Form.Input
            required={true}
            control={Input}
            label='Email'
            placeholder='Email'
            disabled={true}
            defaultValue={state?.userDetails?.data?.email}
            onChange={(event, data) => setState({
              ...state,
              email: data.value
            })} />
          <Form.Field
            required={true}
            control={Input}
            label='Cell Number'
            placeholder='Cell Number'
            defaultValue={state?.userDetails?.data?.cell_number}
            onChange={(event, data) => setState({
              ...state,
              cellNumber: data.value
            })} />
        </Form.Group>

        <Form.Group>
          <Form.Field
            required={true}
            control={Input}
            label='ID Number'
            placeholder='ID Number'
            defaultValue={state?.userDetails?.data?.id_number}
            width={8}
            onChange={(event, data) => setState({
              ...state,
              idNumber: data.value
            })} />

          <Form.Field
            required={true}
            control={Select}
            label={{
              children: <span
                title='Toggle role permissions'
                onClick={(event, data) => setState({ ...state, showPermissions: !state.showPermissions })}>Role <Icon name='info' /></span>
            }}
            options={state.roleItems}
            value={state.role}
            onChange={(event, data) => {
              setState({
                ...state,
                role: data.value
              })
            }} />

        </Form.Group>

        <br />
        {role && state.showPermissions && role.permissions.length > 0 &&
          <Message
            header={`${state.firstName || 'Your user'}'s permissions`}
            list={role.permissions.map((p, i) => p.name)} />
        }
        <br />

        <Button
          onClick={registerUser}
          size='tiny'>
          Submit
        </Button>
        <ErrorMessages error={error} />
      </Form>
    </Segment>
  );
};
