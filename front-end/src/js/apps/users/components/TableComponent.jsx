import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { Button, Checkbox, Icon, Table, Segment, } from 'semantic-ui-react'
import axios from '../../../utils/axios';

export const TableComponent = () => {

	const [state, setState] = useState({
		data: null,
		rows: []
	});

	useEffect(() => {
		function getUsers() {
			axios.get('/api/v1/users/')
				.then((response) => {
					setState(prevState => ({
						...prevState,
						data: response
					}));
				})
				.catch(function (error) {
					alert(error);
				});
		}

		getUsers();

	}, [])

	const history = useHistory();

	const addUser = () => {
		history.push('/users/create');
	}

	const getRows = () => {

		const response = state?.data;

		let rows = [];

		for (const item of response?.data?.results || []) {

			rows.push(

				<Table.Row key={item.id}>
					<Table.Cell>
						<Checkbox toggle checked={item.is_active} size='tiny' />
					</Table.Cell>
					<Table.Cell>{`${item.first_name} ${item.last_name}`}</Table.Cell>
					<Table.Cell>{new Date(item.date_joined).toISOString().substring(0, 10)}</Table.Cell>
					<Table.Cell>{item.email}</Table.Cell>
					<Table.Cell>{item?.groups[0]?.name}</Table.Cell>
					<Table.Cell>
						<Button as={Link} to={`/users/${item.id}/edit`} primary icon size='tiny' title='Edit user'>
							<Icon name='pencil' />
						</Button>
					</Table.Cell>
				</Table.Row>

			);
		}

		return rows;
	}

	const rows = getRows();

	return (

		<Table celled compact definition size='small'>
			<Table.Header fullWidth>
				<Table.Row>
					<Table.HeaderCell>Account Status</Table.HeaderCell>
					<Table.HeaderCell>Name</Table.HeaderCell>
					<Table.HeaderCell>Registration Date</Table.HeaderCell>
					<Table.HeaderCell>E-mail address</Table.HeaderCell>
					<Table.HeaderCell>Role</Table.HeaderCell>
					<Table.HeaderCell>Manage user</Table.HeaderCell>
				</Table.Row>
			</Table.Header>

			<Table.Body>
				{rows}
			</Table.Body>

			<Table.Footer fullWidth>
				<Table.Row>
					<Table.HeaderCell colSpan='6'>
						<Button
							as={Link}
							to='/users/create'
							floated='right'
							icon
							labelPosition='left'
							primary
							size='tiny'>
							<Icon name='user plus' /> Add User
						</Button>
						<Button
							as={Link}
							to='/users/invite'
							floated='right'
							icon
							labelPosition='left'
							color='green'
							size='tiny'>
							<Icon name='user' /> Invite
						</Button>
					</Table.HeaderCell>
				</Table.Row>
			</Table.Footer>
		</Table>
	)
}
