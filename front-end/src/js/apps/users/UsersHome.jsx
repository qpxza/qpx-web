import React, { useEffect, useState } from 'react'
import { useHistory, Link } from 'react-router-dom'
import { Button, Checkbox, Icon, Table, Pagination, Card, List } from 'semantic-ui-react'
import { useQuery, useRequest } from '../../utils';

export const UsersHome = (props) => {

  const [activePage, setActivePage] = useState(useQuery().get('page') || 1);

  const [getUsers, data, loading, error] = useRequest();

  useEffect(() => {
    getUsers({ url: `/api/v1/users/?page=${activePage}` });
  }, [activePage])

  const history = useHistory();

  const getRows = () => {

    let rows = [];

    if (props.screenSize === 'desktop') {

      for (const user of data?.results || []) {

        rows.push(
          <Table.Row key={user.id}>
            <Table.Cell>
              <Checkbox toggle checked={user.is_active} size='tiny' />
            </Table.Cell>
            <Table.Cell>{`${user.first_name} ${user.last_name}`}</Table.Cell>
            <Table.Cell>{new Date(user.date_joined).toISOString().substring(0, 10)}</Table.Cell>
            <Table.Cell>{user.email}</Table.Cell>
            <Table.Cell>{user?.groups[0]?.name}</Table.Cell>
            <Table.Cell textAlign='right'>
              <Button
                as={Link}
                to={`/users/${user.id}/edit`}
                primary
                icon='pencil'
                size='tiny'
                title='Edit user' />
            </Table.Cell>
          </Table.Row>
        );
      }
    }

    if (props.screenSize !== 'desktop') {
      for (const user of data?.results || []) {
        rows.push(
          <Card fluid key={user.id}>
            <Card.Content>
              <Card.Header>
                {`${user.first_name} ${user.last_name}`}
              </Card.Header>
              <Card.Description>
                <List>
                  <List.Item icon='calendar' content={new Date(user.date_joined).toISOString().substring(0, 10)} />
                  <List.Item
                    icon='mail'
                    content={<a href={`mailto:${user.email}`}>{user.email}</a>} />
                  <List.Item icon='user' content={user?.groups[0]?.name} />
                </List>
              </Card.Description>

            </Card.Content>
            <Card.Content extra>
              <Button.Group fluid compact size={'tiny'}>
                <Button
                  basic
                  color={'green'}
                  as={Link}
                  to={`/users/${user.id}/edit`}
                  color={'blue'}
                  content={'Edit User'} />
              </Button.Group>
            </Card.Content>
          </Card>
        );
      }
    }

    return rows;
  }

  const rows = getRows();

  return (

    <>
      { props.screenSize === 'desktop' ?
        <Table
          celled
          color={'purple'}>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Account Status</Table.HeaderCell>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Registration Date</Table.HeaderCell>
              <Table.HeaderCell>E-mail address</Table.HeaderCell>
              <Table.HeaderCell>Role</Table.HeaderCell>
              <Table.HeaderCell textAlign='right'>Action</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {rows}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan='3'>
                <Button
                  as={Link}
                  to='/users/create'
                  icon
                  labelPosition='left'
                  primary
                  size='tiny'
                  loading={loading}>
                  <Icon name='user plus' /> Add User
						</Button>
                <Button
                  as={Link}
                  to='/users/invite'
                  icon
                  labelPosition='left'
                  color='green'
                  size='tiny'
                  loading={loading}>
                  <Icon name='user' /> Invite
						</Button>
              </Table.HeaderCell>
              <Table.HeaderCell colSpan='3'>
                <Pagination
                  className='right floated'
                  defaultActivePage={activePage}
                  totalPages={Math.ceil((data?.count || 0) / 10)}
                  onPageChange={(event, data) => setActivePage(data.activePage)} />
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
        :

        <>
          <Button.Group fluid>
            <Button as={Link} to={'/users/create'} icon={'user plus'} content={'Add User'} color={'blue'} />
            <Button as={Link} to={'/users/invite'} icon={'plus'} content={'Invite User'} color={'green'} />
          </Button.Group>
          <br />
          <br />
          <Card.Group>
            {rows}
          </Card.Group >
        </>
      }
    </>
  )
}
