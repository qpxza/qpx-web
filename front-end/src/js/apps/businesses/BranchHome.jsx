import React, { useEffect, useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Button, Icon, Table, Popup, Card, List } from 'semantic-ui-react'
import axios from '../../utils/axios';

export const BranchHome = (props) => {

  const [state, setState] = useState({
    data: null,
    rows: []
  });

  useEffect(() => {

    function getBranches() {

      axios.get('/api/v1/branches/')
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            data: response
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranches();

  }, [])

  const history = useHistory();

  const addBranch = () => {
    history.push('/branches/create');
  }

  const getRows = () => {

    const response = state?.data;

    let rows = [];

    if (props.screenSize === 'desktop') {
      for (const item of response?.data?.results || []) {

        rows.push(

          <Table.Row
            key={item.id}
            active={item.hq}>
            <Table.Cell>
              {item.name}&nbsp;
            {item.hq &&
                <Popup
                  content="Headquarters branch. This is your business' main branch"
                  hoverable
                  trigger={<Icon name='building outline' color='red' />} />
              }
            </Table.Cell>
            <Table.Cell>{item.phone}</Table.Cell>
            <Table.Cell>{item.email}</Table.Cell>
            <Table.Cell>{item.country}</Table.Cell>
            <Table.Cell>{item.city}</Table.Cell>
            <Table.Cell>{item.suburb}</Table.Cell>
            <Table.Cell>{item.street_address}</Table.Cell>
            <Table.Cell>{item.postal_code}</Table.Cell>
            <Table.Cell collapsing>
              <Button.Group
                size='tiny'>
                <Button
                  as={Link}
                  to={`/branches/${item.id}/edit/`}
                  color='blue'
                  icon='pencil'
                  title='Edit branch' />
                <Button
                  as={Link}
                  to={`/branches/${item.id}`}
                  color='green'
                  icon='eye'
                  title='View Branch Details' />
              </Button.Group>
            </Table.Cell>
          </Table.Row >

        );
      }
    }

    if (props.screenSize !== 'desktop') {
      for (const item of response?.data?.results || []) {
        rows.push(
          <Card fluid key={item.id}>
            <Card.Content>
              <Card.Header>
                {item.name}

                {item.hq &&
                  <Popup
                    content="Headquarters branch. This is your business' main branch"
                    hoverable
                    trigger={<Icon name='building outline' color='red' />} />
                }
              </Card.Header>
              <Card.Description>
                <List>
                  <List.Item icon='phone' content={item.phone} />
                  <List.Item
                    icon='mail'
                    content={<a href={`mailto:${item.email}`}>{item.email}</a>} />
                  <List.Item icon='map outline' content={item.country} />
                  <List.Item icon='map outline' content={item.city} />
                  <List.Item icon='map outline' content={item.suburb} />
                  <List.Item icon='map outline' content={item.street_address} />
                  <List.Item icon='map outline' content={item.postal_code} />
                </List>
              </Card.Description>

            </Card.Content>
            <Card.Content extra>
              <Button.Group fluid compact size={'tiny'}>
                <Button
                  basic
                  color={'green'}
                  as={Link}
                  to={`/branches/${item.id}/edit/`}
                  color={'blue'}
                  content={'Edit branch'}>
                </Button>
                <Button
                  basic
                  color={'red'}
                  as={Link}
                  to={`/branches/${item.id}`}
                  color={'green'}
                  content={'View Details'}>
                </Button>
              </Button.Group>
            </Card.Content>
          </Card>
        )

      }
    }
    return rows;
  }

  const rows = getRows();

  return (

    <>
      { props.screenSize === 'desktop' ?
        < Table color={'purple'} celled>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Branch Name</Table.HeaderCell>
              <Table.HeaderCell>Phone Number</Table.HeaderCell>
              <Table.HeaderCell>E-mail address</Table.HeaderCell>
              <Table.HeaderCell>Country</Table.HeaderCell>
              <Table.HeaderCell>City</Table.HeaderCell>
              <Table.HeaderCell>Suburb</Table.HeaderCell>
              <Table.HeaderCell>Street Address</Table.HeaderCell>
              <Table.HeaderCell>Postal Code</Table.HeaderCell>
              <Table.HeaderCell>Action</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {rows}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan='9'>
                <Button
                  floated='right'
                  icon
                  labelPosition='left'
                  primary
                  size='tiny'
                  onClick={addBranch}>
                  <Icon name='plus' /> Add A Branch
					</Button>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table >

        :

        <>
          <Button primary icon={'plus'} fluid content={'Add Branch'} onClick={addBranch} />
          <br />
          <Card.Group>
            {rows}
          </Card.Group >
        </>
      }
    </>
  )
}
