import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Button, Icon, Table, Dropdown, Card, List } from 'semantic-ui-react'
import axios from '../../utils/axios';

export const BranchDetails = (props) => {

  const { id } = useParams();

  const [state, setState] = useState({
    data: null,
    users: null,
    rows: [],
    searchValue: null,
    searchQuery: null
  });

  useEffect(() => {

    async function getBranchDetails() {

      axios.get(`/api/v1/branches/${id}/users`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            data: response
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranchDetails();

  }, [])

  useEffect(() => {

    async function getUsers() {
      const config = {
        params: {
          branch_not_in: id,
          page_size: 500
        }
      };

      axios.get('/api/v1/users/', config)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            users: response
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getUsers();

  }, [])

  const getRows = () => {

    const response = state?.data;

    let rows = [];

    if (props.screenSize === 'desktop') {

      for (const item of response?.data?.results || []) {

        rows.push(

          <Table.Row key={item.id}>
            <Table.Cell>{`${item.first_name} ${item.last_name}`}</Table.Cell>
            <Table.Cell>{item.cell_number}</Table.Cell>
            <Table.Cell>{item.email}</Table.Cell>
            <Table.Cell>{item?.groups[0]?.name}</Table.Cell>
            <Table.Cell collapsing>
              <Button negative onClick={() => deleteUserFromBranch(item.id)}>Remove User</Button>
            </Table.Cell>
          </Table.Row>

        );
      }
    }

    if (props.screenSize !== 'desktop') {
      for (const item of response?.data?.results || []) {
        rows.push(
          <Card fluid key={item.id}>
            <Card.Content>
              <Card.Header>
                {item.first_name} {item.last_name}
              </Card.Header>
              <Card.Description>
                <List>
                  <List.Item icon='phone' content={item.cell_number} />
                  <List.Item
                    icon='mail'
                    content={<a href={`mailto:${item.email}`}>{item.email}</a>} />
                  <List.Item
                    icon='user'
                    content={item?.groups[0]?.name} />
                </List>
              </Card.Description>

            </Card.Content>
            <Card.Content extra>
              <Button.Group fluid compact size={'tiny'}>
                <Button
                  basic
                  color='red'
                  content='Remove User'
                  onClick={() => deleteUserFromBranch(item.id)}>
                </Button>
              </Button.Group>
            </Card.Content>
          </Card>
        )

      }
    }

    return rows;
  }

  const getUsers = () => {

    const users = [];

    if (state?.users?.data?.results) {

      for (const item of state?.users?.data?.results) {

        users.push({
          key: item.id,
          text: `${item.first_name} ${item.last_name} (${item.email})`,
          value: item.id
        });
      }
    }

    return users;
  }

  const updateUsers = () => {

    axios.post(`/api/v1/branches/${id}/users/add/`, state.searchValue)
      .catch(function (error) {
        alert(error);
      });
  }

  const deleteUserFromBranch = (userId) => {

    const config = {
      data: [userId]
    };

    axios.delete(`/api/v1/branches/${id}/users/remove/`, config)
      .then(() => window.location.reload())
      .catch(function (error) {
        alert(error);
      });
  }

  const rows = getRows();

  const users = getUsers();

  return (
    <>
      {props.screenSize === 'desktop'

        ?
        <Table color={'purple'} celled>
          <Table.Header fullWidth>
            <Table.Row>
              <Table.HeaderCell>Employee Name</Table.HeaderCell>
              <Table.HeaderCell>Phone Number</Table.HeaderCell>
              <Table.HeaderCell>E-mail address</Table.HeaderCell>
              <Table.HeaderCell>Role</Table.HeaderCell>
              <Table.HeaderCell>Remove user</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {rows}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell colSpan='9'>
                <Dropdown
                  fluid
                  selection
                  multiple={true}
                  search={true}
                  options={users}
                  placeholder='Add Users'
                  onChange={(event, data) => setState({
                    ...state,
                    searchValue: data.value
                  })}
                  onSearchChange={(event, data) => setState({
                    ...state,
                    searchQuery: data.value
                  })}
                />
              </Table.HeaderCell>
            </Table.Row>

            <Table.Row>
              <Table.HeaderCell colSpan='9'>
                <Button
                  floated='right'
                  icon
                  labelPosition='left'
                  primary
                  onClick={updateUsers}>
                  <Icon name='user' /> Add Users
					</Button>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>

        :
        <Card.Group>
          {rows}
        </Card.Group>
      }
    </>
  )
}
