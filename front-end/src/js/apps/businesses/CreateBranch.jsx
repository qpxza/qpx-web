import React, { useEffect, useState } from 'react'
import {
  Button,
  Checkbox,
  Form,
  Icon,
  Input,
  Select,
  Segment,
} from 'semantic-ui-react';
import { ErrorMessages } from './../common/ErrorMessages';
import axios from '../../utils/axios';
import { useHistory } from 'react-router-dom';
import { isEqual } from 'lodash'
import { useRequest } from './../../utils/hooks';

export const CreateBranch = () => {

  const history = useHistory();

  const [createBranch, data, loading, error] = useRequest();

  const defaultState = {
    name: '',
    email: '',
    city: '',
    suburb: '',
    streetAddress: '',
    postalCode: '',
    country: 'South Africa',
    hq: false,
    phoneNumber: '',
    countries: [{
      key: 'South Africa',
      value: 'South Africa',
      text: 'South Africa',
    }],
  }

  const [
    state,
    setState
  ] = useState({
    ...defaultState
  });

  const registerBusiness = () => {

    if (isEqual(state, defaultState)) {
      return;
    }

    const params = {
      name: state.name,
      email: state.email,
      city: state.city,
      suburb: state.suburb,
      street_address: state.streetAddress,
      postal_code: state.postalCode,
      country: state.country,
      hq: state.hq,
      phone: state.phoneNumber
    };

    createBranch({ url: '/api/v1/branches/', method: 'POST', data: params })
      .then((res) => {
        res.successful && history.push('/branches');
      })
      .catch(function (error) {
        alert(error);
      });

  }

  return (

    <Segment>
      <Form
        size='tiny'
        loading={loading}
        error={!!error}>
        <Form.Group widths={'equal'}>
          <Form.Field
            required={true}
            control={Input}
            label='Branch Name'
            placeholder='Ocean Basket Potchefstroom'
            onChange={(event, data) => setState({
              ...state,
              name: data.value
            })} />
          <Form.Field
            required={true}
            control={Input}
            label='City'
            placeholder='eg. Potchefstroom'
            onChange={(event, data) => setState({
              ...state,
              city: data.value
            })} />
        </Form.Group>

        <Form.Group widths={'equal'}>
          <Form.Input
            required={true}
            control={Input}
            label='Suburb'
            placeholder='eg. Sandton'
            onChange={(event, data) => setState({
              ...state,
              suburb: data.value
            })} />

          <Form.Field
            required={true}
            control={Input}
            label='Street Address'
            placeholder='eg. 1 Newton Avenue.'
            onChange={(event, data) => setState({
              ...state,
              streetAddress: data.value
            })} />

        </Form.Group>

        <Form.Group>
          <Form.Field
            required={true}
            control={Input}
            width='8'
            label='Postal Code'
            placeholder='eg. 1234.'
            onChange={(event, data) => setState({
              ...state,
              postalCode: data.value
            })} />
          <Form.Field
            required={true}
            control={Select}
            label='Country'
            options={state.countries}
            defaultValue={'South Africa'}
            placeholder='Country'
            onChange={(event, data) => {
              setState({
                ...state,
                country: data.value
              })
            }} />

        </Form.Group>
        <Form.Group widths={'equal'}>
          <Form.Field
            required={true}
            control={Input}
            label='Business telephone'
            placeholder='eg. 011 524 7895'
            onChange={(event, data) => setState({
              ...state,
              phoneNumber: data.value
            })} />

          <Form.Field
            required={true}
            control={Input}
            label='Email Address'
            placeholder='Email Address'
            onChange={(event, data) => setState({
              ...state,
              email: data.value
            })} />
        </Form.Group>
        <Form.Group>
          <Form.Field
            control={Checkbox}
            width='8'
            label={{ children: 'Head quarters of this business?' }}
            checked={state.hq}
            value={1}
            onClick={() => setState({
              ...state,
              hq: !state.hq,
            })} />
        </Form.Group>
        <br />
        <Button
          icon
          labelPosition='left'
          primary
          size='tiny'
          onClick={registerBusiness}>

          <Icon name='user' />
              Submit
          </Button>
        <ErrorMessages error={error} />
      </Form>
    </Segment>
  );
};
