import React, { useEffect, useState } from 'react'
import {
  Button,
  Checkbox,
  Form,
  Input,
  Select,
  Segment,
} from 'semantic-ui-react'
import { ErrorMessages } from './../common/ErrorMessages';
import axios from '../../utils/axios';
import { useHistory, useParams } from 'react-router-dom';
import { isEqual } from 'lodash'
import { useRequest } from './../../utils/hooks';

export const EditBranch = () => {

  const { id } = useParams();

  const history = useHistory();

  const [updateBranch, data, loading, error] = useRequest();

  const defaultState = {
    name: '',
    email: '',
    city: '',
    suburb: '',
    streetAddress: '',
    postalCode: '',
    country: 'South Africa',
    hq: false,
    phoneNumber: '',
    countries: [{
      key: 'South Africa',
      value: 'South Africa',
      text: 'South Africa',
    }],
  }

  const [
    state,
    setState
  ] = useState({
    ...defaultState,
    branchDetails: null
  });

  useEffect(() => {

    function getBranchDetails() {

      axios.get(`/api/v1/branches/${id}`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            branchDetails: response,
            hq: !!response.data?.hq, // todo: fix this dirty hack :-(
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranchDetails();

  }, [])

  useEffect(() => {
    async function getCountries() {

      require('axios').get('https://restcountries.eu/rest/v2/all?fields=name')
        .then((response) => {

          formatCountries(response.data);
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getCountries();

  }, [])

  const formatCountries = (response) => {

    let dropdownItems = [];

    for (const item of response) {
      dropdownItems.push({
        key: item.name,
        value: item.name,
        text: item.name
      });
    }

    setState(prevState => ({
      ...prevState,
      countries: dropdownItems
    }));
  }

  const submit = () => {

    if (isEqual(state, defaultState)) {

      return;
    }

    const payload = {
      name: state.name || state?.branchDetails?.data?.name,
      email: state.email || state?.branchDetails?.data?.email,
      city: state.city || state?.branchDetails?.data?.city,
      suburb: state.suburb || state?.branchDetails?.data?.suburb,
      street_address: state.streetAddress || state?.branchDetails?.data?.street_address,
      postal_code: state.postalCode || state?.branchDetails?.data?.postal_code,
      country: state.country || state?.branchDetails?.data?.country,
      phone: state.phoneNumber || state?.branchDetails?.data?.phone,
      hq: state.hq,
    };

    updateBranch({ url: `/api/v1/branches/${id}/`, method: 'PUT', data: payload })
      .then((res) => {
        res.successful && history.push('/branches');
      })
      .catch(function (error) {
        alert(error);
      });

  }

  return (

    <Segment>
      <Form
        size='tiny'
        error={!!error}
        loading={loading}>
        <Form.Group widths={'equal'}>
          <Form.Field
            required={true}
            control={Input}
            label='Branch Name'
            defaultValue={state?.branchDetails?.data?.name}
            placeholder='Ocean Basket Potchefstroom'
            onChange={(event, data) => setState({
              ...state,
              name: data.value
            })} />

          <Form.Field
            required={true}
            control={Input}
            label='City'
            defaultValue={state?.branchDetails?.data?.city}
            placeholder='eg. Potchefstroom'
            onChange={(event, data) => setState({
              ...state,
              city: data.value
            })} />
        </Form.Group>

        <Form.Group widths={'equal'}>
          <Form.Input
            required={true}
            control={Input}
            label='Suburb'
            placeholder='eg. Sandton'
            defaultValue={state?.branchDetails?.data?.suburb}
            onChange={(event, data) => setState({
              ...state,
              suburb: data.value
            })} />

          <Form.Field
            required={true}
            control={Input}
            label='Street Address'
            placeholder='eg. 1 Newton Avenue.'
            defaultValue={state?.branchDetails?.data?.street_address}
            onChange={(event, data) => setState({
              ...state,
              streetAddress: data.value
            })} />


        </Form.Group>


        <Form.Group>
          <Form.Field
            required={true}
            control={Input}
            width='8'
            label='Postal Code'
            placeholder='eg. 1234.'
            defaultValue={state?.branchDetails?.data?.postal_code}
            onChange={(event, data) => setState({
              ...state,
              postalCode: data.value
            })} />
          <Form.Field
            required={true}
            control={Select}
            label='Country'
            width='8'
            options={state.countries}
            value={state?.branchDetails?.data?.country}
            placeholder='Country'
            onChange={(event, data) => setState({
              ...state,
              country: data.value
            })} />
        </Form.Group>


        <Form.Group widths='equal'>
          <Form.Field
            required={true}
            control={Input}
            label='Business telephone'
            placeholder='eg. 011 524 7895'
            defaultValue={state?.branchDetails?.data?.phone}
            onChange={(event, data) => setState({
              ...state,
              phoneNumber: data.value
            })} />

          <Form.Field
            required={true}
            control={Input}
            label='Email Address'
            placeholder='Email Address'
            defaultValue={state?.branchDetails?.data?.email}
            onChange={(event, data) => setState({
              ...state,
              email: data.value
            })} />
        </Form.Group>

        <Form.Group>
          <Form.Field
            control={Checkbox}
            width='8'
            label={{ children: 'Head quarters of this business?' }}
            checked={state.hq}
            value={1}
            onClick={() => setState({
              ...state,
              hq: !state.hq,
            })} />
        </Form.Group>
        <br />
        <Button
          size='tiny'
          color='green'
          onClick={submit}>
          Save
          </Button>
        <ErrorMessages error={error} />
      </Form>
    </Segment>
  );
};
