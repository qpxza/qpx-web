import React, { useState } from 'react';
import axios from 'axios';
import { StyledLink } from '../../components/StyledLink';
import Cookies from 'js-cookie';

import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { getQueryParam } from '../../utils/url';
import { useRequest } from '../../utils/hooks';
import { ErrorMessages } from '../common/ErrorMessages';

const token = getQueryParam('token');

export const PasswordReset = () => {

  const [state, setState] = useState({
    email: '',
  });

  const [resetPassword, data, loading, error] = useRequest();

  const submit = () => {
    const data = {
      "email": state.email,
    };

    resetPassword({ url: '/api/v1/auth/password/reset/', method: 'POST', data });
  };

  return (
    <Grid textAlign='center' style={{ height: '100vh', backgroundColor: 'rgb(244, 244, 244)' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          Reset your password
        </Header>
        <Form size='large' error={!!error} loading={loading}>
          {!data ?
            <Segment stacked>
              <Form.Input
                required={true}
                fluid icon='user'
                iconPosition='left'
                placeholder='E-mail address'
                name='email'
                type='email'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  email: data.value
                }))} />

              <Button
                color='teal'
                fluid size='large'
                onClick={submit}>
                Reset
              </Button>
            </Segment>
            :
            <Message color='green' content={data.detail} />
          }
          <ErrorMessages error={error} />
        </Form>
        <Message>
          <a
            href='/accounts/login/' className='ui floated right'>
            Login
          </a>
          &nbsp;|&nbsp;
          {error &&
            <a
              href='/accounts/password-reset/' className='ui floated right'>
              Resend link
            </a>
          }
        </Message>
      </Grid.Column>
    </Grid>
  )
};
