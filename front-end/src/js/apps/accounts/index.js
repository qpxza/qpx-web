
import React from 'react'
import ReactDOM from 'react-dom';
import  { Registration } from './Registration';
import  { Login } from './Login';
import  { PasswordReset } from './PasswordReset';
import  { PasswordResetConfirm } from './PasswordResetConfirm';
import { BusinessSetup } from './BusinessSetup';
import '../../utils/sentry';

const login = document.getElementById('login-app');

if (login) {
    ReactDOM.render(<Login />, login);
}

const registration = document.getElementById('registration-app');

if (registration) {
    ReactDOM.render(<Registration />, registration);
}

const passwordReset = document.getElementById('password-reset-app');

if (passwordReset) {
    ReactDOM.render(<PasswordReset />, passwordReset);
}

const passwordResetConfirm = document.getElementById('password-reset-confirm-app');

if (passwordResetConfirm) {
    ReactDOM.render(<PasswordResetConfirm />, passwordResetConfirm);
}


const businessSetup = document.getElementById('setup-app');

if (businessSetup) {
    ReactDOM.render(<BusinessSetup />, businessSetup);
}
