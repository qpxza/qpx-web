import React, { useState } from 'react'
import axios from 'axios';
import Cookies from 'js-cookie';
import {
  Button,
  Form,
  Grid,
  Header,
  Segment
} from 'semantic-ui-react'

export const BusinessSetup = () => {

  const [email, setBusinessEmail] = useState('');
  const [businessName, setBusinessName] = useState('');

  const registerBusiness = () => {

    axios.post('/api/v1/businesses/', {
      email,
      name: businessName,
    }, {
      headers: {
        'X-CsrfToken': Cookies.get('csrftoken')
      }
    })
      .then(() => {
        window.location.href = '/dashboard'
      })
      .catch(function (error) {
        alert(error);
      });

  }

  return (
    <Grid textAlign='center' style={{ height: '100vh', 'background-color': 'rgb(244, 244, 244)' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          Create your business account:
        </Header>
        <Form size='large'>
          <Segment stacked>

            <Form.Input
              fluid icon='user'
              iconPosition='left'
              placeholder='Business Name'
              onChange={(event, data) => setBusinessName(data.value)} />

            <Form.Input
              fluid icon='mail'
              iconPosition='left'
              placeholder='Business Email'
              onChange={(event, data) => setBusinessEmail(data.value)} />

            <Button
              color='teal'
              fluid size='large'
              onClick={registerBusiness}>
              Complete business registration
            </Button>
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  )
};
