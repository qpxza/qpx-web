import React, { useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import Cookies from 'js-cookie';
import { StyledLink } from '../../components/StyledLink';
import {
  Button,
  Card,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from 'semantic-ui-react';

import { getScreenSize } from '../../utils';

export const Login = () => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showError, setError] = useState(false);

  const loginUser = () => {

    axios.post('/api/v1/auth/login/', {
      email,
      password
    }, {
      headers: {
        'X-CsrfToken': Cookies.get('csrftoken')
      }
    }).then(() => {
      window.location.href = '/dashboard/'
    })
      .catch(function (error) {
        setError(error.response.data.non_field_errors[0]);
      });

  }

  const size = getScreenSize();

  return (
    <>
      <div style={{ height: '100%', display: 'flex', backgroundColor: 'rgb(244, 244, 244)' }}>
        <div style={{ margin: 'auto' }}>
          <Segment raised style={{ height: '100%', width: `${size === 'mobi' ? '100vw' : '40vw'}` }}>
            <Header as='h2' color='teal' textAlign='center'
              content={'Log-in to your account'} />
            <Form size='large'>
              <Form.Input
                required={true}
                fluid icon='user'
                iconPosition='left'
                placeholder='E-mail address'
                onChange={(event, data) => setEmail(data.value)} />

              <Form.Input
                required={true}
                fluid icon='lock'
                iconPosition='left'
                placeholder='Password'
                type='password'
                onChange={(event, data) => setPassword(data.value)} />

              <Button
                color='teal'
                fluid size='large'
                onClick={loginUser}
                content={'Login'} />

              {showError &&

                <Message negative>
                  <Message.Header>{showError}</Message.Header>
                </Message>
              }
            </Form>
            <Message>
              <a
                href='/accounts/registration/'>
                Register
              </a>
              &nbsp;|&nbsp;
              <a
                href='/accounts/password-reset/'>
                Reset password
              </a>
            </Message>
          </Segment>
        </div>
      </div>
    </>
  )
};
