import React, { useState } from 'react';
import axios from 'axios';
import Cookies from 'js-cookie';

import {
  Button,
  Form,
  Grid,
  Header,
  Message,
  Segment
} from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { getQueryParam } from '../../utils/url';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';

const token = getQueryParam('token');
const uid = getQueryParam('uid');
const email = getQueryParam('email');

export const PasswordResetConfirm = () => {

  const [state, setState] = useState({
    password1: '',
    password2: '',
  });

  const [changePassword, data, loading, error] = useRequest();

  const submit = () => {
    const data = {
      "new_password1": state.password1,
      "new_password2": state.password2,
      "token": token,
      "uid": uid,
    };

    changePassword({ url: '/api/v1/auth/password/reset/confirm/', method: 'POST', data });
  };

  return (
    <Grid textAlign='center' style={{ height: '100vh', backgroundColor: 'rgb(244, 244, 244)' }} verticalAlign='middle'>
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as='h2' color='teal' textAlign='center'>
          Set your new password
        </Header>
        <Form size='large' error={!!error} loading={loading}>
          {!data ?
            <Segment stacked>
              <input type="hidden" name="email" value={email} />
              <Form.Input
                fluid
                icon='lock'
                iconPosition='left'
                placeholder='New Password'
                type='password'
                name='password1'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  password1: data.value
                }))} />

              <Form.Input
                fluid icon='lock'
                iconPosition='left'
                placeholder='Confirm Password'
                type='password'
                name='password2'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  password2: data.value
                }))} />

              <Button
                color='teal'
                fluid size='large'
                onClick={submit}>
                Change password
              </Button>
            </Segment>
            :
            <Message color='green' content={data.detail} />
          }
          <ErrorMessages error={error} />
        </Form>
        <Message>
          <a
            href='/accounts/login/' className='ui floated right'>
            Login
          </a>
          &nbsp;|&nbsp;
          <a
            href='/accounts/password-reset/' className='ui floated right'>
            Resend link
          </a>
        </Message>
      </Grid.Column>
    </Grid>
  )
};
