import React, {useState} from 'react';
import axios from 'axios';
import { StyledLink } from '../../components/StyledLink';
import Cookies from 'js-cookie';
import { 
    Button, 
    Form, 
    Grid, 
    Header, 
    Message, 
    Segment 
} from 'semantic-ui-react';
import { getScreenSize } from '../../utils';

export const Registration = () => {

  const [state, setState] = useState({
    email: '',
    password1: '',
    password2: '',
    firstName: '',
    lastName: '',
  });
  
  const [showError, setError] = useState(false);

  const registerUser = () => {

    axios.post('/api/v1/auth/registration/', {
      email: state.email,
      first_name: state.firstName,
      last_name: state.lastName,
      username: state.email,
      password1: state.password1,
      password2: state.password2
    },
    {
      headers: {
        'X-CsrfToken': Cookies.get('csrftoken')
    }})
    .then(() => {
      window.location.href = '/accounts/login/'
    })
    .catch(function (error) {
      setError(error.response.data.non_field_errors[0]);
    });

  }

  const size = getScreenSize();

  return (
    <>
      <div style={{ height: '100%', display: 'flex', backgroundColor: 'rgb(244, 244, 244)' }}>
        <div style={{ margin: 'auto' }}>
        <Segment raised style={{ height: '100%', width: `${size === 'mobi' ? '100vw' : '40vw'}` }}>

          <Header as='h2' color='teal' textAlign='center'>
            Create an account
          </Header>

          <Form size='large'>

              <Form.Input
                required={true}
                fluid icon='user' 
                iconPosition='left' 
                placeholder='E-mail address'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  email: data.value
                }))}/>

              <Form.Input
                required={true}
                fluid icon='user'
                iconPosition='left'
                placeholder='First Name'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  firstName: data.value
                }))}/>

              <Form.Input
                required={true}
                fluid icon='user'
                iconPosition='left'
                placeholder='Last Name'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  lastName: data.value
                }))}/>
              
              <Form.Input
                required={true}
                fluid icon='lock'
                iconPosition='left'
                placeholder='Confirm Password'
                type='password'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  password1: data.value
                }))}/>

              <Form.Input
                required={true}
                fluid icon='lock'
                iconPosition='left'
                placeholder='Confirm Password'
                type='password'
                onChange={(event, data) => setState(prevState => ({
                  ...prevState,
                  password2: data.value
                }))}/>

              <Button 
                color='teal' 
                fluid size='large'
                onClick={registerUser}>
                Register
              </Button>
              
            { showError && 

              <Message negative>
                <Message.Header>{showError}</Message.Header>
              </Message>
            }
            
          </Form>
          <Message>
            Already have an account? 
            
            <StyledLink 
              onClick={() => window.location.href = '/accounts/login/'}>

                Login
            </StyledLink>

          </Message>
          </Segment>
        </div>
      </div>
    </>
  );
}
