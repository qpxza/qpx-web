import React from 'react'
import { Form, Button, Icon, Table, Input } from 'semantic-ui-react'

export const Items = (props) => {

  return (
    <Table.Row>
      <Table.Cell colSpan={'7'}>
        <Form.Field
          control={Input}
          placeholder='Sub Section'
          value={props.item.subsection || props.item.name || props.item.description || ''}
          onChange={(event, data) => props.onPropertyValueChanged({ subsection: data.value, name: data.value, description: 'default'})} />
      </Table.Cell>
      <Table.Cell colSpan={'2'}>
        <Button.Group
          size='tiny'
          floated='right'>
          <Button
            icon
            color='blue'
            title='Move item up'
            onClick={() => props.onMoveUpClick(props.item, props.section)}>
            <Icon name='angle double up' />
          </Button>
          <Button
            icon
            color='red'
            title='Remove item'
            onClick={() => props.onRemoveClick(props.item, props.section)}>
            <Icon name='trash alternate outline' />
          </Button>
        </Button.Group>
      </Table.Cell>
    </Table.Row>
  );
}

