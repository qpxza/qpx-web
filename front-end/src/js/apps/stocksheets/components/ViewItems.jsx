import React from 'react'
import { Form, Table, Input } from 'semantic-ui-react'

export const ViewItems = (props) => {

  const calculateClosingStock = () => {
    let items = props.item;

    for (let i = 1; i <= 3; i++) {
      if (!parseInt(items[`v${i}`])) {
        items[`v${i}`] = 0;
      }
    }

    props.onPropertyValueChanged({ ['v4']: ((parseInt(items['v1']) + parseInt(items['v2']) - parseInt(items['v3']))) })

  }

  const calculateVariance = () => {

    let items = props.item;

    for (let i = 1; i <= 3; i++) {
      if (!parseInt(items[`v${i}`])) {
        items[`v${i}`] = 0;
      }
    }

    props.onPropertyValueChanged({ ['v5']: (parseInt(items['v4']) - (parseInt(items['v1']) + parseInt(items['v2']) - parseInt(items['v3']))) })

  }

  return (
    <Table.Row>
      <Table.Cell>
        {props.item.name}
      </Table.Cell>
      <Table.Cell key={`${props.item.name}-1`}>
        <Form.Field
          control={Input}
          value={props.item['v1']}
          placeholder='OS'
          onChange={(event, data) => {
            props.onPropertyValueChanged({ ['v1']: data.value });
            calculateClosingStock();
            calculateVariance();
          }} />
      </Table.Cell>
      <Table.Cell key={`${props.item.name}-2`}>
        <Form.Field
          control={Input}
          value={props.item['v2']}
          placeholder='REC'
          onChange={(event, data) => {
            props.onPropertyValueChanged({ ['v2']: data.value });
            calculateClosingStock();
            calculateVariance();
          }} />
      </Table.Cell>
      <Table.Cell key={`${props.item.name}-3`}>
        <Form.Field
          control={Input}
          value={props.item['v3']}
          placeholder='SOLD/ISSUED'
          onChange={(event, data) => {
            props.onPropertyValueChanged({ ['v3']: data.value });
            calculateClosingStock();
            calculateVariance();
          }} />
      </Table.Cell>
      <Table.Cell key={`${props.item.name}-4`}>
        <Form.Field
          control={Input}
          value={props.item['v4']}
          placeholder='CLOSING STOCK'
          onChange={(event, data) => {
            props.onPropertyValueChanged({ ['v4']: data.value });

            calculateVariance();
          }} />
      </Table.Cell>
      <Table.Cell key={`${props.item.name}-5`}>
        <Form.Field
          control={Input}
          value={props.item['v5']}
          placeholder='VARIANCE'
          onChange={(event, data) => {
            props.onPropertyValueChanged({ ['v5']: data.value });
          }} />
      </Table.Cell>
    </Table.Row >
  );
}

