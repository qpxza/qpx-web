import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Form, Button, Table, Input, Select } from 'semantic-ui-react'
import { useRequest } from '../../utils';
import axios from '../../utils/axios';
import { useQuery } from '../../utils/hooks';
import { ErrorMessages } from '../common/ErrorMessages';
import { ViewItems } from './components/ViewItems';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

export const StocksheetComplete = () => {

  const [state, setState] = useState({
    name: null,
    comment: '',
    sections: [],
    labels: ['OS', 'REC', 'SOLD', 'CS', 'VAR'],
    branches: null,
    selectedBranch: null,
    template: null,
    currentDate: new Date(),
  });

  const id = useQuery().get('template_id');
  const history = useHistory();
  const [createStocksheet, stocksheet, creating, error] = useRequest();

  useEffect(() => {

    function getStocksheet() {

      axios.get(`/api/v1/stocksheets/templates/${id}/`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            name: response.data?.name,
            description: response.data?.description,
            sections: response.data?.sections,
            template: response.data,
          }));

        })
        .catch(function (error) {
          alert(error);
        });
    }

    getStocksheet();

    async function getBranches() {

      axios.get('/api/v1/branches/')
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            branches: response
          }));
        })
        .catch(function (error) {
          alert(error);
        });
    }

    getBranches();

  }, []);

  const getDraftKey = () => {
    return location.href + state.template?.updated_at;
  };

  useEffect(() => {
    function restoreDraft() {
      if (!state.template) {
        return;
      }

      const key = getDraftKey();
      const draft = localStorage.getItem(key)

      if (!draft) {
        return;
      }

      if (confirm('We found a draft for this stocksheet. Do you want to continue were you left off?')) {
        setState(prevState => ({ ...prevState, ...JSON.parse(draft) }));
      }

      localStorage.removeItem(key);
    }

    restoreDraft();
  }, [state.template]);

  const getBranches = () => {

    const branches = [];

    if (state?.branches?.data?.results) {

      for (const branch of state?.branches?.data?.results) {

        branches.push({
          key: branch.id,
          text: branch.name,
          value: branch.id
        });
      }
    }

    return branches;
  }

  const removeItem = (item) => {

    const sections = state.sections;

    for (const section of sections) {

      const index = section.items.indexOf(item);

      if (index > -1) {
        section.items.splice(index, 1);
      }
    }

    setState(prevState => ({
      ...prevState,
      sections: state.sections.map(section => section),
    }));
  }


  const saveDraft = () => {
    const draft = {
      sections: state.sections,
      selectedBranch: state.selectedBranch,
      comment: state.comment,
    };

    localStorage.setItem(getDraftKey(), JSON.stringify(draft));
    alert('Draft succesfully saved!')
  };

  const saveForm = () => {

    if (state.name === null) {
      alert('Your form requires a name.');

      return;
    }

    if (state.sections.length === 0) {
      alert('You need to have at least 1 section.');

      return;
    }

    const invalidItems = {
      name: '',
      description: '',
      subsection: '',
    }

    for (const section of state.sections) {

      if (section.items.length === 0) {
        alert('You need to have at least 1 item in a section.')
        return;
      }

      for (const [key, value] of Object.entries(section)) {
        if (value === invalidItems[key]) {

          alert('All fields need to be completed');
          return;
        }
      }
    }

    const payload = {
      name: state.name,
      template: id,
      branch: state.selectedBranch,
      date_completed: new Date().toISOString(),
      start_date: new Date().toISOString(),
      comment: state.comment,
      sections: state.sections,
      description: 'Stocksheet description',
      label1: 'OS',
      label2: 'REC',
      label3: 'SOLD',
      label4: 'CS',
      label5: 'VAR',

    };

    createStocksheet({
      url: '/api/v1/stocksheets/',
      method: 'POST',
      data: payload,
    }).then((res) => res.successful && history.push('/stocksheets/'));
  };

  const renderItems = (items) => items.map((item, i) => {

    return (
      <ViewItems
        key={i}
        item={item}
        labels={state.labels.length}
        onRemoveClick={removeItem}
        onPropertyValueChanged={(data) => {

          Object.assign(item, data);

          setState({
            ...state,
            sections: state.sections.map(section => section)
          })
        }} />
    )
  });

  const renderSections = () => state.sections.map((section, i) => (
    <Table.Body key={i}>
      <Table.Row active>
        <Table.Cell>
          {section.name}
        </Table.Cell>
        {state.labels && state.labels.map((label) => (
          <Table.HeaderCell key={label} textAlign={'center'}>
            {label}
          </Table.HeaderCell>
        ))}
      </Table.Row>
      {renderItems(section.items)}

    </Table.Body>
  ));

  return (

    <Form size='mini' error={!!error}>
      <Table color={'purple'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Complete a {state.name} Stocksheet</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <Form.Group widths={'equal'}>
                <Form.Field
                  required={true}
                  control={Select}
                  options={getBranches()}
                  placeholder='Branch'
                  onChange={(event, data) => {
                    setState({
                      ...state,
                      selectedBranch: data.value
                    })
                  }} />

                <DatePicker
                  selected={state.currentDate}
                  startDate={state.currentDate}
                  onChange={(date) => {

                    setState((prevState) => ({
                      ...prevState,
                      currentDate: date,
                    }));
                  }} />
              </Form.Group>
            </Table.Cell>
          </Table.Row>
        </Table.Body>
      </Table>

      <Table>
        <Table.Header>
          <Table.Row active>
            <Table.HeaderCell colSpan='7'>Stocksheet</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        {renderSections()}
        <Table.Footer>
          <Table.Row>
            <Table.Cell colSpan='7'>
              <Button
                type='button'
                size='tiny'
                color='green'
                content='Submit Stocksheet'
                floated='right'
                icon='save'
                labelPosition='left'
                loading={creating}
                onClick={saveForm} />
              <Button
                size='tiny'
                color='blue'
                content='Save draft'
                floated='right'
                icon='save'
                labelPosition='left'
                onClick={saveDraft} />
            </Table.Cell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <ErrorMessages error={error} />
    </Form>
  )
}
