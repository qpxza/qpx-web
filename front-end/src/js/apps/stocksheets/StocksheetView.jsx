import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Table} from 'semantic-ui-react';
import { localTime, useRequest } from '../../utils';

export const StocksheetView = () => {

  const { id } = useParams();

  const [state, setState] = useState({
    stocksheet: null,
  });

  const [viewStocksheet, _stocksheetData, loading, error] = useRequest();

  useEffect(() => {

    viewStocksheet({ url: `/api/v1/stocksheets/${id}/` })
      .then((response) => {
        if (response.successful) {
          setState(prevState => ({
            ...prevState,
            stocksheet: response.data
          }));
        } else {
          setState((prevState) => ({
            ...prevState,
            error: true
          }));
        }
      });

  }, []);

  const headers = (stocksheet) => {

    let headerList = [];

    for (let i = 1; i <= 5; i++) {
      if (stocksheet[`label${i}`]) {
        headerList.push(
          <Table.HeaderCell key={stocksheet[`label${i}`]}>
            {stocksheet[`label${i}`]}
          </Table.HeaderCell>
        );
      }
    }

    return headerList;
  }

  const getValues = (item) => {

    let values = [];

    for (let i = 1; i <= 5; i++) {

      values.push(
        <Table.Cell key={`${item.name}-${i}-${item.id}`}>
          {item[`v${i}`]}
        </Table.Cell>
      );
    }

    return values;
  }

  const items = (items) => {

    let itemList = [];

    for (const item of items) {
      itemList.push(
        <Table.Row key={`row-${item.name}-${item.id}`}>
          <Table.Cell key={`cell-${item.name}-${item.id}`}>
            {item.name}
          </Table.Cell>
          {getValues(item)}
        </Table.Row>
      );
    }

    return itemList;
  }

  const body = (sections) => {

    let sectionsItems = [];

    for (const section of sections) {
      sectionsItems.push(
        <>
          <Table.Row active key={`row-${section.key}`}>
            <Table.Cell key={`section-${section.key}`}>
              {section.name}
            </Table.Cell>
            {headers(state.stocksheet)}
          </Table.Row>
          {section.items && items(section.items)}
        </>
      );
    }

    return sectionsItems;
  }

  return (

    <Table color={'purple'}>
      <Table.Header fullWidth>
        <Table.Row>
          <Table.HeaderCell colSpan={6} key={'main-row'}>
            {state?.stocksheet?.name} for: {state.stocksheet?.branch?.name} on the {localTime(state.stocksheet?.created_at)}
          </Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {state.stocksheet && body(state.stocksheet.sections)}
      </Table.Body>
    </Table>
  );
}
