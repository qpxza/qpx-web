import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Form, Button, Table, Input, Label } from 'semantic-ui-react'
import axios from '../../utils/axios';
import { ErrorMessages } from '../common/ErrorMessages';
import { useRequest } from '../../utils/hooks';
import { Items } from './components/Items';
import { toast } from 'react-toastify';

export const StocksheetCreate = () => {

  const [state, setState] = useState({
    name: '',
    sections: [],
  });

  const history = useHistory();

  const { id } = useParams();

  const [updateStocksheet, _stocksheet, updating, error] = useRequest();

  const addSection = () => {

    const section = {
      name: '',
      description: '',
      items: [],
    };

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.concat(section),
    }));
  };

  const moveUp = (item, items) => {
    const index = items.indexOf(item);
    const newIndex = Math.max(index - 1, 0);

    if (newIndex < 0) {
      return;
    }

    items[index] = items[newIndex];
    items[newIndex] = item;

    return items;
  };

  const moveSectionUp = (section) => {
    setState((prevState) => ({
      ...prevState,
      sections: moveUp(section, prevState.sections).map(s => s),
    }));
  };

  const removeSection = (section) => {
    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.filter(s => s != section),
    }));
  };

  const addItem = (section) => {

    section.items.push({
      subsection: '',
      description: '',
      name: '',
    });

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.map(s => s),
    }));
  }

  const moveItemUp = (item, section) => {
    section.items = moveUp(item, section.items).map(s => s);

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.map(s => s),
    }));
  };

  const removeItem = (item, section) => {
    section.items.splice(section.items.indexOf(item), 1);

    setState((prevState) => ({
      ...prevState,
      sections: prevState.sections.map(section => section),
    }));
  }

  const saveForm = () => {
    if (!state.name) {
      toast.error('Your form requires a name.');
      return;
    }

    if (state.sections.length === 0) {
      toast.error('You need to have at least 1 section.');

      return;
    }

    const invalidItems = {
      subsection: '',
    }

    for (const section of state.sections) {

      if (section.items.length === 0) {
        toast.error(`You need to have at least 1 item in section "${section.name}"`)
        return;
      }

      for (const item of section.items) {
        if (item?.description === '') {

          item.description = 'default';
        }
      }

      for (const [key, value] of Object.entries(section)) {

        if (value === invalidItems[key]) {

          toast.error('All fields need to be completed');
          return;
        }
      }
    }

    const params = {
      id: id,
      name: state.name,
      sections: state.sections,
      label1: 'OS',
      label2: 'REC',
      label3: 'SOLD',
      label4: 'CS',
      label5: 'VAR',
    };
    const promise = !id
      ? updateStocksheet({ url: '/api/v1/stocksheets/templates/', data: params, method: 'post' }) // new stocksheet template
      : updateStocksheet({ url: `/api/v1/stocksheets/templates/${id}/`, data: params, method: 'put' }); // existing stocksheet template

    promise.then((res) => res.successful && history.push('/stocksheets/templates/'));
  }

  const renderItems = (section) => section.items.map((item, index) => (
    <Items
      key={index}
      section={section}
      item={item}
      onMoveUpClick={moveItemUp}
      onRemoveClick={removeItem}
      onPropertyValueChanged={(data) => {

        Object.assign(item, data);

        setState({
          ...state,
          sections: state.sections.map(s => s)
        });
      }} />
  ));

  const renderSections = () => state.sections.map((section, index) => (

    <React.Fragment key={index}>
      <Table.Row active>
        <Table.Cell colSpan='2'>
          <Form.Field
            control={Input}
            placeholder='Section Name'
            error={!section.name}
            value={section.name}
            onChange={(event, data) => {

              Object.assign(section, { name: data.value, description: data.value });

              setState({
                ...state,
                sections: state.sections.map(section => section)
              })
            }} />

        </Table.Cell>
        <Table.Cell singleLine colSpan='8'>
          <Button.Group
            size='tiny'
            floated='right'>
            <Button
              icon='angle double up'
              color='grey'
              title='Move section up'
              onClick={() => moveSectionUp(section)} />
            <Button
              color='red'
              title='Remove section'
              floated='right'
              icon='trash alternate outline'
              size='tiny'
              onClick={() => removeSection(section)} />
          </Button.Group>
        </Table.Cell>
      </Table.Row>
      <Table.Row>
        <Table.Cell colSpan='2'>Sub Section</Table.Cell>
        <Table.Cell>{'Opening Stock (OS)'}</Table.Cell>
        <Table.Cell>{'Received Stock (REC)'}</Table.Cell>
        <Table.Cell>{'Sold/Issuing (SOLD)'}</Table.Cell>
        <Table.Cell>{'Closing Stock (CS)'}</Table.Cell>
        <Table.Cell>{'Variance (VAR)'}</Table.Cell>
        <Table.Cell>
          <Button
            color='green'
            icon='add'
            size='tiny'
            floated='right'
            title='Create New Item'
            onClick={() => addItem(section)} />
        </Table.Cell>
      </Table.Row>

      {renderItems(section)}

    </React.Fragment>
  ));

  const addDefaults = () => {
    // add a default section with a single item
    setTimeout(addSection, 0);
    // hiding a potential race condition bug here but I won't bother looking into react's state management
    setTimeout(() => state.sections.length && addItem(state.sections[0]),);
  }

  useEffect(function () {
    function getStocksheet() {

      axios.get(`/api/v1/stocksheets/templates/${id}/`)
        .then((response) => {
          setState(prevState => ({
            ...prevState,
            name: response.data.name,
            sections: response?.data?.sections
          }));

        })
        .catch(function (error) {
          alert(error);
        });
    }

    if (id) {
      getStocksheet();
    } else {
      addDefaults();
    }
  }, []);

  return (
    <Form
      size='tiny'
      loading={updating}
      error={!!error}>
      <Table color={'purple'}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell colSpan='5'>{id ? 'Edit' : 'Create'} Stocksheet Template</Table.HeaderCell>
            <Table.HeaderCell colSpan='3'>
              <Form.Field
                control={Input}
                placeholder='Template name'
                value={state.name}
                labelPosition='left'
                onChange={(event, data) => {
                  setState({
                    ...state,
                    name: data.value
                  })
                }}>
                <Label>Stocksheet template name: </Label>
                <input />
              </Form.Field>
            </Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            <Table.HeaderCell colSpan='2'>
              {'Sub Section'}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {'Opening Stock (OS)'}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {'Received Stock (REC)'}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {'Sold/Issuing (SOLD)'}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {'Closing Stock (CS)'}
            </Table.HeaderCell>
            <Table.HeaderCell>
              {'Variance (VAR)'}
            </Table.HeaderCell>
            <Table.HeaderCell />
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {state.sections && renderSections()}
        </Table.Body>


        <Table.Footer >
          <Table.Row>
            <Table.HeaderCell colSpan='8'>
              <Button
                color='green'
                content='Create New Section'
                floated='left'
                icon='add'
                labelPosition='left'
                size='tiny'
                onClick={addSection} />

              <Button
                color='green'
                content='Submit Stocksheet'
                floated='right'
                icon='save'
                labelPosition='left'
                size='tiny'
                onClick={saveForm} />
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>
      <ErrorMessages error={error} />
    </Form >
  )
}
