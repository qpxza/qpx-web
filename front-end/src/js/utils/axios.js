import Cookies from 'js-cookie';
import axios from 'axios';

const instance = axios.create({
    headers: {
        'X-CsrfToken': Cookies.get('csrftoken'),
    },
    params: {
        'business_id': window.BUSINESS_ID
    }
});

instance.interceptors.response.use((response) => {
    response.successful = true;

    return response;
});

export default instance;
