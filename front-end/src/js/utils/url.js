
export function getQueryParam(name, query = window.location.search){
    return new URLSearchParams(query).get(name);
}
