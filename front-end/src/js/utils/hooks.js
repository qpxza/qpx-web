import { useState } from 'react';
import axios from './axios';
import { Sentry } from './sentry';
import { useLocation } from 'react-router-dom';

export function useRequest(initial = null) {
    const [data, setData] = useState(initial);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    /**
     * To determine the success of a request, use the res.successful
     * property that is set in a response interceptor in utils/axios.js
     *
     * @param {*} reqConfig See axios.request docs
     * @returns Promise
     */
    function makeRequest(reqConfig) {
        const scope = new Sentry.Scope();

        scope.setLevel(Sentry.Severity.Error);

        scope.addBreadcrumb({ message: 'Dispatching request...', data: { ...reqConfig } });

        const config = {
            ...reqConfig,
        };

        // reset our state before dispatching request
        setLoading(true);
        setData(null);
        setError(null);

        return axios.request(config)
            .then((response) => {
                setData(response.data);
                setError(null);

                return response;
            })
            .catch(function (error) {
                setError(error);
                Sentry.captureException(error, scope);

                return error;
            })
            .finally(() => {
                setLoading(false);
            });
    }

    return [makeRequest, data, loading, error];
}

export function useQuery() {
    return new URLSearchParams(useLocation().search);
}
