import * as _Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";

_Sentry.init({
  dsn: process.env.NODE_ENV === 'production' ? 'https://07ad522e7bc04da582150bb31a3ec365@o481847.ingest.sentry.io/5687648' : '',
  integrations: [new Integrations.BrowserTracing()],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

export const Sentry = _Sentry;
