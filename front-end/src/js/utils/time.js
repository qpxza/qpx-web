const moment = require('moment');

export const localTime = date => new Date(date).toLocaleDateString(undefined, {
  hour: '2-digit',
  minute: '2-digit',
});


export const getEndOfWeek = () => {
  const curr = new Date; // get current date
  const first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
  const last = first + 6; // last day is the first day + 6

  return new Date(curr.setDate(last));

}

export const getStartOfWeek = () => {
  const curr = new Date; // get current date
  const first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week

  return new Date(curr.setDate(first));
}

export const getFormattedDate = (date) => {
  return new Date(date).toISOString().split('T', 1)[0];
}

export const getLastMonth = () => {
  const startDate = moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
  const endDate = moment().subtract(1, 'months').endOf('month').format('YYYY-MM-DD');

  return [startDate, endDate];
}

export const getLastWeek = () => {

  return [moment().startOf('week').format('YYYY-MM-DD'), moment().endOf('week').format('YYYY-MM-DD')];
}

export const getLastSevenDays = () => {

  return [moment().startOf('week').subtract(7, 'days').format('YYYY-MM-DD'), moment().endOf('week').subtract(7, 'days').format('YYYY-MM-DD')];
}

export const getYearToDate = () => {

  return [moment().startOf('year').format('YYYY-MM-DD'), moment().format('YYYY-MM-DD')];
}

export const daysOfWeek = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday'
];