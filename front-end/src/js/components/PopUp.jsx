import React from 'react'
import { Button, Modal } from 'semantic-ui-react'

function exampleReducer(state, action) {
  switch (action.type) {
    case 'OPEN_MODAL':
      return { open: true, dimmer: action.dimmer }
    case 'CLOSE_MODAL':
      return { open: false }
    default:
      throw new Error()
  }
}

export const PopUp = (props) => {

  const [state, dispatch] = React.useReducer(exampleReducer, {
    open: false,
    dimmer: undefined,
  });

  const { open, dimmer } = state

  return (
    <div>
      <Button onClick={() => dispatch({ type: 'OPEN_MODAL' })}>Default</Button>

      <Modal
        dimmer={dimmer}
        open={props.showError}
        onClose={() => {
          dispatch({ type: 'CLOSE_MODAL' })
        }}
      >
        <Modal.Header>Ooopsie!</Modal.Header>
        <Modal.Content>
          {props.errorText}
        </Modal.Content>
        <Modal.Actions>
          <Button negative onClick={() => dispatch({ type: 'CLOSE_MODAL' })}>
            Close
          </Button>
        </Modal.Actions>
      </Modal>
    </div>
  )
}