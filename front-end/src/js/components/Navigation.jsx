import React, { useEffect, useState } from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';

import {
  Grid,
  GridRow,
} from 'semantic-ui-react'

import { SidebarDimmed } from './Sidebar';
import { NavBar } from './NavBar';

import { Dashboard } from '../apps/dashboard/Dashboard';
import { CreateUser, EditUser, UsersHome, Invite } from '../apps/users';
import { BranchDetails, CreateBranch, BranchHome, EditBranch } from '../apps/businesses';
import { EditTemplate as EditChecklistTemplate, ChecklistHome, EditChecklist, CompleteChecklist, ViewChecklists, ChecklistViewDetails } from '../apps/checklists';
import { ReportsTemplateHome, ReportsHome, ReportsCreate, EditReportTemplate, CompleteReport, ViewCompletedReports, EditReport } from '../apps/reports';
import { SupplierList, EditSupplier } from '../apps/suppliers';
import { Catalog, EditProduct, AddProduct } from '../apps/products';
import { Orders, ViewOrder } from '../apps/orders';
import { NotificationsHome } from '../apps/notifications';
import { StocksheetTemplate, StocksheetCreate, StocksheetHome, StocksheetComplete, StocksheetView } from '../apps/stocksheets';

import { AuthContext, useRequest, getScreenSize } from '../utils'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const Navigation = () => {
  const [getUser, authUser, loading, error] = useRequest();

  useEffect(() => getUser({
    url: `/api/v1/users/${AUTH_USER_ID}/`,
    params: {
      'business_id': window.AUTH_USER_BUSINESS_ID,
    }
  }), [null]);

  const size = getScreenSize();

  const routes = () => {
    return (
      <Switch>
        <Route exact path='/'>
          <Dashboard screenSize={size} />
        </Route>
        <Route exact path='/users'>
          <UsersHome screenSize={size} />
        </Route>
        <Route exact path='/users/create'>
          <CreateUser screenSize={size} />
        </Route>
        <Route exact path='/users/:id/edit'>
          <EditUser screenSize={size} />
        </Route>
        <Route exact path='/users/invite'>
          <Invite screenSize={size} />
        </Route>
        <Route exact path='/branches'>
          <BranchHome screenSize={size} />
        </Route>
        <Route exact path='/branches/create'>
          <CreateBranch screenSize={size} />
        </Route>
        <Route exact path='/branches/:id/edit'>
          <EditBranch screenSize={size} />
        </Route>
        <Route exact path='/branches/:id'>
          <BranchDetails screenSize={size} />
        </Route>
        <Route exact path='/checklists/templates/create'>
          <EditChecklistTemplate screenSize={size} />
        </Route>
        <Route exact path='/checklists/templates/:id/edit'>
          <EditChecklistTemplate screenSize={size} />
        </Route>
        <Route exact path='/checklists/create'>
          <CompleteChecklist screenSize={size} />
        </Route>
        <Route exact path='/checklists'>
          <ViewChecklists screenSize={size} />
        </Route>
        <Route exact path='/checklists/templates'>
          <ChecklistHome screenSize={size} />
        </Route>
        <Route exact path='/checklists/:id'>
          <ChecklistViewDetails screenSize={size} />
        </Route>
        <Route exact path='/checklists/:id/edit'>
          <EditChecklist screenSize={size} />
        </Route>
        <Route exact path='/reports/template/create'>
          <ReportsCreate screenSize={size} />
        </Route>
        <Route exact path='/reports/create'>
          <CompleteReport screenSize={size} />
        </Route>
        <Route exact path='/reports/templates'>
          <ReportsTemplateHome screenSize={size} />
        </Route>
        <Route exact path='/reports/templates/:id/edit'>
          <EditReportTemplate screenSize={size} />
        </Route>
        <Route exact path='/reports/:id'>
          <ViewCompletedReports screenSize={size} />
        </Route>
        <Route exact path='/reports/:id/edit'>
          <EditReport screenSize={size} />
        </Route>
        <Route exact path='/reports'>
          <ReportsHome screenSize={size} />
        </Route>
        <Route exact path='/suppliers'>
          <SupplierList screenSize={size} />
        </Route>
        <Route exact path='/suppliers/:id/edit'>
          <EditSupplier screenSize={size} />
        </Route>
        <Route exact path='/suppliers/create'>
          <EditSupplier screenSize={size} />
        </Route>
        <Route exact path='/catalog'>
          <Catalog screenSize={size} />
        </Route>
        <Route exact path='/products/add'>
          <AddProduct screenSize={size} />
        </Route>
        <Route exact path='/products/:id/edit'>
          <EditProduct screenSize={size} />
        </Route>
        <Route exact path='/orders'>
          <Orders screenSize={size} />
        </Route>
        <Route exact path='/orders/:id'>
          <ViewOrder screenSize={size} />
        </Route>
        <Route exact path='/notifications'>
          <NotificationsHome screenSize={size} />
        </Route>
        <Route exact path='/stocksheets/templates'>
          <StocksheetTemplate screenSize={size} />
        </Route>
        <Route exact path='/stocksheets/templates/create'>
          <StocksheetCreate screenSize={size} />
        </Route>
        <Route exact path='/stocksheets/templates/:id/edit'>
          <StocksheetCreate screenSize={size} />
        </Route>
        <Route exact path='/stocksheets/create'>
          <StocksheetComplete screenSize={size} />
        </Route>
        <Route exact path='/stocksheets'>
          <StocksheetHome screenSize={size} />
        </Route>
        <Route exact path='/stocksheets/:id'>
          <StocksheetView screenSize={size} />
        </Route>
      </Switch>
    );
  }

  return (
    <>
      { authUser &&
        <Router basename={BASENAME}>
          <AuthContext.Provider value={{ authUser }}>

            {size === 'desktop' &&
              <Grid style={{ minHeight: '100vh', width: '100%', backgroundColor: 'rgb(244, 244, 244)', margin: '0' }} columns={14}>
                <GridRow>
                  <Grid.Column width={2}>
                    <SidebarDimmed screenSize={size} />
                  </Grid.Column>
                  <Grid.Column width={12}>
                    <NavBar />
                    <ToastContainer
                      position='top-right'
                      hideProgressBar
                      pauseOnHover />

                    {routes()}
                  </Grid.Column>
                </GridRow>
              </Grid>
            }

            {size !== 'desktop' &&
              <div style={{ minHeight: '100vh', width: '100%', backgroundColor: 'rgb(244, 244, 244)', padding: '10px 10px 0' }} >
                <NavBar />
                <ToastContainer
                  position='top-right'
                  hideProgressBar
                  pauseOnHover />

                {routes()}
              </div>
            }
          </AuthContext.Provider>
        </Router> || 'Loading...'
      }
    </>
  )
};
