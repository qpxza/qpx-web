import styled from 'styled-components';

export const StyledLink = styled.button`
  background: none;
  border: none;
  padding: 0 0 0 3px;
  font-family: arial, sans-serif;
  color: #069;
  text-decoration: underline;
  cursor: pointer;
`;