import React, { useContext, useEffect, useState } from 'react';
import { Breadcrumb, Button, Dropdown, Header, Icon, Menu, Segment } from 'semantic-ui-react';
import styled from 'styled-components';
import { Link, NavLink, useLocation } from 'react-router-dom';
import { AuthContext, getScreenSize, useRequest } from '../utils';
import { StyledLink } from './StyledLink';
import { SidebarDimmed } from './Sidebar';

const NavItems = styled.div`
  width: 100vw;
  height: 3em;
  background-color: rgb(27, 28, 29);
  position: fixed;
  text-align: right;
  z-index: 100;
`;

function ucfirst(str) {
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

const DropdownTrigger = (props) => (
  <Header as='span'>
    {props.name}
  </Header>
);

export const NavBar = (props) => {
  
  const location = useLocation();
  const segments = location.pathname.split('/').slice(1);
  const { authUser } = useContext(AuthContext);
  const business = authUser.businesses.find((b) => b.id == BUSINESS_ID);

  const [makeRequest, notificationsData, loading, error] = useRequest();
  const [removeNotification, , , notificationError] = useRequest();

  const [hasUnread, setRead] = useState(false);

  useEffect(
    () => makeRequest({ url: `/api/v1/notifications/` }),
    []
  );

  const NotificationsIcon = () => {

    return (
      <Icon.Group size={'large'}>
        <Icon name='bell outline' color={notificationsData?.results?.length !== 0 ? 'red' : 'grey'} />
        <Icon corner name={notificationsData?.results?.length !== 0 ? 'envelope outline' : 'envelope open outline'} />
      </Icon.Group>
    );
  };

  const markAsRead = (item) => {
    const params = {
      url: `/api/v1/notifications/${item.id}/`,
      method: 'PUT',
      data: {
        id: item.id,
        read: true
      }
    };

    removeNotification(params)
      .then((res) => res.successful && makeRequest({ url: `/api/v1/notifications/` }))
  }

  const getDropdownItems = (items) => {

    if (!items || items.results.length === 0) {

      return (
        <>
          <Dropdown.Item key={0}>
            You have no notifications.
            
          </Dropdown.Item>
          <Dropdown.Item key={999}>
            <StyledLink as={Link} to={'/notifications'}>...View all</StyledLink>
          </Dropdown.Item>
        </>
      );
    }

    let messages = [];

    for (const item of items.results) {

      if (item.read) {
        continue;
      }

      messages.push(
        <Dropdown.Item key={item.id} onClick={() => markAsRead(item)}>
          <Icon name={'times'} className={'right floated'} />
          {item.body}
        </Dropdown.Item>
      );
    }

    if (messages.length === 0) {

      return (
        <>
          <Dropdown.Item key={0}>
            You have no notifications.
            
          </Dropdown.Item>
          <Dropdown.Item key={999}>
            <StyledLink as={Link} to={'/notifications'}>...View all</StyledLink>
          </Dropdown.Item>
        </>
      );
    }

    messages.push(
      <Dropdown.Item key={999}>
        <StyledLink as={Link} to={'/notifications'}>...View more</StyledLink>
      </Dropdown.Item>
    );
    
    return messages;
  }

  const size = getScreenSize();
  
  return (
    <Segment>
      <SidebarDimmed screenSize={size} />
      <div style={size !== 'desktop' ? { display: 'flex', flex: '1', flexDirection: 'row', float: 'right' } : {}}>
      <Dropdown trigger={<DropdownTrigger name={business.name} />}>
        <Dropdown.Menu>
          {authUser.businesses.map((b, index) => (
            <Dropdown.Item key={index}>
              <Icon name='building outline' /> <a target='_blank' href={`/dashboard/${b.id}/${b.slug}`}>{b.name}</a>
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
      </div>
      <br /><br />
      <Breadcrumb>
        <Breadcrumb.Section as={NavLink} to='/'>Dashboard</Breadcrumb.Section>
        {segments.map((s, i) => (
          <React.Fragment key={`breadcrumb-${i}`}>
            <Breadcrumb.Divider icon='right angle' />
            <Breadcrumb.Section as={NavLink} to={'/' + segments.slice(0, i + 1).join('/')}>{ucfirst(s)}</Breadcrumb.Section>
          </React.Fragment>
        ))}
      </Breadcrumb>

      <div style={{ display: 'flex', flex: '1', flexDirection: 'row', float: 'right' }}>
        <Dropdown icon={<NotificationsIcon/>}>
          <Dropdown.Menu direction={'left'}>
            {getDropdownItems(notificationsData)}
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </Segment>
  );
}
