import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

import {
  Button,
  Icon,
  Menu,
  Sidebar,
} from 'semantic-ui-react';
import { toast } from 'react-toastify';
import { useRequest } from '../utils/hooks';

export const SidebarDimmed = (props) => {

  const [showMenuItems, setMenuItems] = useState(false);

  const [logout] = useRequest();

  const handleLogout = () => {
    toast.info('Signing you out...');

    logout({ url: '/api/v1/auth/logout/', 'method': 'POST' })
      .then((res) => {
        if (res.successful) {
          window.location.replace('/accounts/login/');
        } else {
          toast.error('Request failed. Please refresh the page');
        }
      });
  };

  const menuItems = () => {
    return (
      <>
        <Menu.Item>
          <Menu.Header name='home' as={NavLink} to='/'>
            <Icon name='home' /> Home
      </Menu.Header>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Security</Menu.Header>
          <Menu.Menu>
            <Menu.Item name='users' as={NavLink} to='/users' />
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Branches</Menu.Header>
          <Menu.Menu>
            <Menu.Item name='branches' as={NavLink} to='/branches'>
              Management
          </Menu.Item>
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Checklists</Menu.Header>
          <Menu.Menu position="left">
            <Menu.Item name='Templates' as={NavLink} to='/checklists/templates' exact />
            <Menu.Item name='Checklists' as={NavLink} to='/checklists' exact />
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Reports</Menu.Header>
          <Menu.Menu>
            <Menu.Item name='Templates' as={NavLink} to='/reports/templates' exact />
          </Menu.Menu>
          <Menu.Menu>
            <Menu.Item name='Reports' as={NavLink} to='/reports' exact />
          </Menu.Menu>
          <Menu.Menu>
            <Menu.Item name='Upload' as={NavLink} to='/reports/create' exact />
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Suppliers</Menu.Header>
          <Menu.Menu>
            <Menu.Item name='Suppliers' as={NavLink} to='/suppliers' exact />
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Products</Menu.Header>
          <Menu.Menu>
            <Menu.Item name='Catalog' as={NavLink} to='/catalog' exact />
          </Menu.Menu>
          <Menu.Menu>
            <Menu.Item name='Orders' as={NavLink} to='/orders' exact />
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Stocksheets</Menu.Header>
          <Menu.Menu>
            <Menu.Item name='Templates' as={NavLink} to='/stocksheets/templates' exact />
          </Menu.Menu>
          <Menu.Menu>
            <Menu.Item name='Stocksheets' as={NavLink} to='/stocksheets' exact />
          </Menu.Menu>
        </Menu.Item>
        <Menu.Item>
          <Menu.Header>Account</Menu.Header>
          <Menu.Menu>
            <Menu.Item onClick={handleLogout}>Logout</Menu.Item>
          </Menu.Menu>
        </Menu.Item>
      </>
    );
  };


  return (
    <>
      {props.screenSize === 'desktop' &&

        <Sidebar
          as={Menu}
          animation='overlay'
          icon='labeled'
          inverted
          vertical
          visible={true}
          width='thin'>

          {menuItems()}

        </Sidebar>

      }

      {props.screenSize !== 'desktop' &&
        <>
          <Button secondary icon={'bars'} onClick={() => setMenuItems(true)} />

          <Sidebar
            as={Menu}
            animation='overlay'
            icon='labeled'
            inverted
            onHide={() => setMenuItems(false)}
            vertical
            visible={showMenuItems}
            width='thin'>

            <Menu.Item onClick={() => setMenuItems(false)}>
              <Menu.Header>
                <Icon fitted name='bars' />
              </Menu.Header>
            </Menu.Item>

            {showMenuItems && menuItems()}

          </Sidebar>
        </>
      }
    </>
  );

}
