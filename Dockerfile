FROM python:3.8.3

ENV APP_ROOT /app
ENV PYTHONPATH ${APP_ROOT}/server
RUN mkdir ${APP_ROOT}
RUN mkdir ${APP_ROOT}/front-end

ADD server /app/server
ADD front-end/dist /app/front-end/dist

RUN pip install --upgrade pip && pip install -r ${APP_ROOT}/server/requirements.txt

EXPOSE 8000

ENTRYPOINT gunicorn -b0.0.0.0:8004 server.wsgi
