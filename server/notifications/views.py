from rest_framework import viewsets
from notifications.models import Notification
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from notifications.filters import BelongsToUserFilter
from notifications.filters import IsReadFilter
from notifications.serializers import NotificationSerializer
from rest_framework import permissions

class NotificationViewset(viewsets.ModelViewSet):
    queryset = Notification.objects.filter(deleted_at__isnull=True).order_by('-read', '-created_at')
    filter_backends = [BelongsToBusinessFilter, BelongsToUserFilter, UndeletedFilter, IsReadFilter]
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = NotificationSerializer
