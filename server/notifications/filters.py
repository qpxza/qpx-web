from rest_framework import filters

class BelongsToUserFilter(filters.BaseFilterBackend):
    """
    Filter that only allows users to see records belonging to the current user
    """
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(user=request.user)

class IsReadFilter(filters.BaseFilterBackend):
    """
    Filter that only allows users to see records belonging to the current user
    """
    def filter_queryset(self, request, queryset, view):
        if request.query_params.get('read', '0') == '0':
            return queryset.filter(read=False)

        return queryset
