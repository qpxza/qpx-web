from django.db import models
from common.models import TimestampsMixin
from users.models import OwnerMixin
from businesses.models import BusinessMixin

class Notification(TimestampsMixin, OwnerMixin, BusinessMixin):
    title = models.CharField(max_length=100)
    body = models.CharField(max_length=200)
    url = models.URLField()
    read = models.BooleanField(default=False)
