from rest_framework import serializers
from rest_framework import fields
from rest_framework import validators
from django.db import models
from notifications.models import Notification

class NotificationSerializer(serializers.ModelSerializer):
    title = fields.CharField(max_length=100,required=False)
    body = fields.CharField(max_length=200, default='')
    url = fields.URLField(required=False)
    read = fields.BooleanField(default=False)

    class Meta:
        model = Notification
        fields = [
            'id',
            'title',
            'body',
            'url',
            'read',
            'created_at',
            'updated_at',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        notification = Notification.objects.create(**validated_data, user=user, business=business)

        return notification

    def update(self, notification, validated_data):
        user = self.context['request'].user

        if notification.user.id != user.id:
            raise validators.ValidationError('Invalid user id')

        notification.read = validated_data.get('read')
        notification.save()

        return notification
