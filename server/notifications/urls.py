from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from notifications.views import NotificationViewset
from notifications.models import Notification

notification_list =  NotificationViewset.as_view({
    'get': 'list',
    'post': 'create',
})

notification_detail =  NotificationViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

urlpatterns = [
    path('api/v1/notifications/', notification_list, name='notification-list'),
    path('api/v1/notifications/<int:pk>/', notification_detail, name='notification-detail'),
]
