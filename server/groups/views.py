from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import permissions
from groups.serializers import GroupSerializer
from django.contrib.auth import models


class GroupPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('auth.view_group')

class GroupViewset(viewsets.ModelViewSet):
    queryset = models.Group.objects.all().order_by('name')
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated, GroupPermission]
