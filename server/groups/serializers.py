from django.contrib.auth import models
from rest_framework import serializers
from rest_framework import fields

class PermissionSerializer(serializers.ModelSerializer):
    name = serializers.CharField(min_length=2, max_length=50)

    class Meta:
        model = models.Permission
        fields = [
            'id',
            'name',
        ]

class GroupSerializer(serializers.ModelSerializer):
    name = serializers.CharField(min_length=2, max_length=50)
    permissions = PermissionSerializer(many=True)

    class Meta:
        model = models.Group
        fields = [
            'id',
            'name',
            'permissions',
        ]
