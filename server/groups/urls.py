from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from groups.views import GroupViewset

user_list =  GroupViewset.as_view({
    'get': 'list',
    'post': 'create',
})

user_detail =  GroupViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
})

urlpatterns = [
    path('api/v1/groups/', user_list, name='group-list'),
    path('api/v1/groups/<int:pk>/',  user_detail, name='group-detail'),
]
