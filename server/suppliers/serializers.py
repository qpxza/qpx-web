from rest_framework import serializers
from rest_framework import fields
from rest_framework import validators
from django.db import transaction
from django.db import models
from users.models import User
from businesses.models import Business
from suppliers.models import Supplier
from suppliers.models import Contact


class ContactSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100)

    class Meta:
        model = Contact
        fields = [
            'id',
            'name',
            'surname',
            'email',
            'phone',
            'cellphone',
            'created_at',
            'updated_at',
        ]


class SupplierSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100)
    description = fields.CharField(max_length=100, default='')
    contacts = ContactSerializer(many=True, read_only=True)

    class Meta:
        model = Supplier
        fields = [
            'id',
            'name',
            'description',
            'address_line_1',
            'address_line_2',
            'city',
            'country',
            'postal_code',
            'email',
            'phone',
            'cellphone',
            'created_at',
            'updated_at',
            'contacts',
            'address',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        supplier = Supplier.objects.create(**validated_data, user=user, business=business)

        return supplier
