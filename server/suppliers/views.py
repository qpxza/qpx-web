from rest_framework import viewsets
from suppliers.models import Supplier
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from suppliers.serializers import SupplierSerializer
from rest_framework import permissions


class SupplierPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('suppliers.view_supplier')


class SupplierViewset(viewsets.ModelViewSet):
    queryset = Supplier.objects.filter(deleted_at__isnull=True)\
        .order_by('name')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, SupplierPermission]
    serializer_class = SupplierSerializer
