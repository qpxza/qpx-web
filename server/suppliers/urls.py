from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from suppliers.views import SupplierViewset
from suppliers.models import Supplier

supplier_list =  SupplierViewset.as_view({
    'get': 'list',
    'post': 'create',
})

supplier_detail =  SupplierViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

urlpatterns = [
    path('api/v1/suppliers/', supplier_list, name='supplier-list'),
    path('api/v1/suppliers/<int:pk>/', supplier_detail, name='supplier-detail'),
]
