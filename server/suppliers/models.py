from django.db import models
from users.models import User
from businesses.models import Business
from branches.models import Branch
from common.models import TimestampsMixin
from users.models import OwnerMixin


class Supplier(TimestampsMixin, OwnerMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    address_line_1 = models.CharField(max_length=100)
    address_line_2 = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    postal_code = models.CharField(max_length=10)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    cellphone = models.CharField(max_length=50)
    user = models.ForeignKey(
        User, related_name="suppliers", on_delete=models.CASCADE)
    business = models.ForeignKey(
        Business, related_name="suppliers", on_delete=models.CASCADE)

    soft_delete = True

    @property
    def address(self):
        return "%s, %s, %s, %s, %s" % (self.address_line_1, self.address_line_2, self.city, self.postal_code, self.country)


class Contact(TimestampsMixin, OwnerMixin):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=50)
    cellphone = models.CharField(max_length=50)
    supplier = models.ForeignKey(
        Supplier, related_name="contacts", on_delete=models.CASCADE)
    user = models.ForeignKey(
        User, related_name="contacts", on_delete=models.CASCADE)
    business = models.ForeignKey(
        Business, related_name="contacts", on_delete=models.CASCADE)
