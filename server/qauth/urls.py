from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView

urlpatterns = [
    re_path(r'^api/v1/auth/', include('dj_rest_auth.urls')),
    re_path(r'^api/v1/auth/registration/', include('dj_rest_auth.registration.urls')),
]
