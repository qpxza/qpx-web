# Generated by Django 3.1.3 on 2021-03-31 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checklists', '0011_auto_20201229_0559'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='item',
            options={'ordering': ['sequence', 'subsection', 'name']},
        ),
        migrations.AlterModelOptions(
            name='section',
            options={'ordering': ['sequence', 'name']},
        ),
        migrations.AlterModelOptions(
            name='templateitem',
            options={'ordering': ['sequence', 'subsection', 'name']},
        ),
        migrations.AlterModelOptions(
            name='templatesection',
            options={'ordering': ['sequence', 'name']},
        ),
        migrations.AddField(
            model_name='item',
            name='sequence',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='section',
            name='sequence',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='templateitem',
            name='sequence',
            field=models.SmallIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='templatesection',
            name='sequence',
            field=models.SmallIntegerField(default=0),
        ),
    ]
