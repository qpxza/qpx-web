from django.shortcuts import render
from django.utils import timezone
from django.http import Http404
from rest_framework import viewsets
from django_weasyprint import WeasyTemplateResponseMixin, WeasyTemplateView
from checklists.models import Template
from checklists.models import Checklist
from checklists.serializers import TemplateSerializer
from checklists.serializers import ChecklistSerializer
from checklists.serializers import UpdateChecklistSerializer
from checklists.serializers import ViewChecklistSerializer
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from rest_framework import permissions

class TemplatePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('checklists.view_template')

class ChecklistPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('checklists.view_checklist')


class TemplateViewset(viewsets.ModelViewSet):
    queryset = Template.objects.prefetch_related('sections__items')\
        .filter(deleted_at__isnull=True).order_by('name')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, TemplatePermission]
    serializer_class = TemplateSerializer


class ChecklistViewset(viewsets.ModelViewSet):
    queryset = Checklist.objects.prefetch_related('branch', 'sections__items')\
        .all().order_by('-created_at')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, ChecklistPermission]
    serializer_class = ChecklistSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return ViewChecklistSerializer

        if self.action == 'update':
            return UpdateChecklistSerializer

        if self.action == 'retrieve':
            return ViewChecklistSerializer

        return ChecklistSerializer


class ChecklistPdfViewset(WeasyTemplateView):
    template_name = 'checklists/pdf.html'
    pdf_attachment = True

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            # business
            checklist = Checklist.objects.get(
                business=self.request.business, pk=kwargs.get('pk'))
            context['checklist'] = checklist
            self.pdf_filename
            return context
        except Checklist.DoesNotExist:
            raise Http404('asdf')

    # dynamically generate filename
    def get_pdf_filename(self):
        return 'Checklist-{business}-{at}.pdf'.format(
            business=self.request.business.slug,
            at=timezone.now().strftime('%Y%m%d-%H%M')
        )
