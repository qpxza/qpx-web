from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from checklists.views import TemplateViewset
from checklists.views import ChecklistViewset
from checklists.views import ChecklistPdfViewset

checklist_list =  ChecklistViewset.as_view({
    'get': 'list',
    'post': 'create',
})

checklist_detail =  ChecklistViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

checklist_pdf =  ChecklistPdfViewset.as_view()

checklist_template_list =  TemplateViewset.as_view({
    'get': 'list',
    'post': 'create',
})

checklist_template_detail =  TemplateViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})


urlpatterns = [
    path('api/v1/checklists/', checklist_list, name='checklist-list'),
    path('api/v1/checklists/<int:pk>/', checklist_detail, name='checklist-detail'),
    path('api/v1/checklists/templates/', checklist_template_list, name='checklist-template-list'),
    path('api/v1/checklists/templates/<int:pk>/', checklist_template_detail, name='checklist-template-detail'),

    path('checklists/<int:pk>/pdf/', checklist_pdf, name='checklist-pdf'),
]
