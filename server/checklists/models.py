from django.db import models
from common.models import TimestampsMixin
from businesses.models import Business
from businesses.models import BusinessMixin
from branches.models import Branch
from users.models import User, OwnerMixin
from django.db.models import Sum


class Template(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    pass_score = models.DecimalField(
        max_digits=10, decimal_places=2, default=0.0)


class TemplateSection(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    template = models.ForeignKey(
        Template,
        related_name='sections',
        on_delete=models.CASCADE
    )
    sequence = models.SmallIntegerField(default=0)

    class Meta:
        ordering = ['sequence', 'name']


class TemplateItem(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    subsection = models.CharField(max_length=100)
    min = models.SmallIntegerField()
    max = models.SmallIntegerField()
    item_type = models.CharField(max_length=10)
    template = models.ForeignKey(
        Template,
        related_name='items',
        on_delete=models.CASCADE
    )
    section = models.ForeignKey(
        TemplateSection,
        related_name='items',
        on_delete=models.CASCADE
    )
    sequence = models.SmallIntegerField(default=0)

    class Meta:
        ordering = ['sequence', 'subsection', 'name']

    @property
    def value(self):
        '''
            Merely there to ensure that checklist items have a default
        '''
        if abs(self.min) < abs(self.max):
            return self.min
        else:
            return self.max

class Checklist(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    comment = models.CharField(max_length=255, default='')
    start_date = models.DateTimeField(blank=True,  null=True)
    date_completed = models.DateTimeField(blank=True,  null=True)
    signee = models.CharField(blank=True, default='', max_length=100)
    template = models.ForeignKey(
        Template,
        related_name='checklists',
        null=True,
        on_delete=models.SET_NULL
    )
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)
    score = models.DecimalField(max_digits=10, decimal_places=1, default=0.0)
    passed = models.BooleanField(default=False)
    pass_score = models.DecimalField(
        max_digits=10, decimal_places=1, default=0.0)

    @property
    def max(self):
        return sum([item.max for item in self.items.all()])

    @property
    def min(self):
        return sum([item.min for item in self.items.all()])

    @property
    def max(self):
        return sum([item.max for item in self.items.all()])

    @property
    def min(self):
        return sum([item.min for item in self.items.all()])

    @property
    def total(self):
        return sum([item.value for item in self.items.all()])

    def set_score(self):
        total_score = self.items.all().aggregate(
            total=Sum('value'))['total'] or 0
        total_max = self.items.all().aggregate(total=Sum('max'))['total'] or 0

        # get around division by 0
        if total_max == 0:
            self.score = 0
            self.passed = False
        else:
            self.score = round(total_score/total_max * 100, 1)
            self.passed = self.score >= self.pass_score


class Section(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    comment = models.CharField(max_length=255, default='')
    checklist = models.ForeignKey(
        Checklist,
        related_name='sections',
        on_delete=models.CASCADE
    )
    sequence = models.SmallIntegerField(default=0)

    class Meta:
        ordering = ['sequence', 'name']

    @property
    def max(self):
        return sum([max(0, item.max) for item in self.items.all()])

    @property
    def min(self):
        return sum([item.min for item in self.items.all()])

    @property
    def score(self):
        return sum([item.value for item in self.items.all()])

    @property
    def passed(self):
        return self.max != 0 and self.score/self.max >= self.checklist.pass_score/100


class Item(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=255)
    comment = models.CharField(max_length=100, default='')
    subsection = models.CharField(max_length=100)
    min = models.SmallIntegerField()
    max = models.SmallIntegerField()
    value = models.SmallIntegerField()
    item_type = models.CharField(max_length=10)
    checklist = models.ForeignKey(
        Checklist,
        related_name='items',
        on_delete=models.CASCADE
    )
    section = models.ForeignKey(
        Section,
        related_name='items',
        on_delete=models.CASCADE
    )
    date_completed = models.DateTimeField(blank=True,  null=True)
    sequence = models.SmallIntegerField(default=0)

    class Meta:
        ordering = ['sequence', 'subsection', 'name']

    @property
    def passed(self):
        return self.value > 0 and self.max > 0 and self.value >= self.checklist.pass_score/100*self.max
