from django.contrib.auth import models
from rest_framework import serializers
from rest_framework import fields
from rest_framework import validators
from django.db import transaction
from checklists.models import Template
from checklists.models import TemplateSection
from checklists.models import TemplateItem
from checklists.models import Checklist
from checklists.models import Section
from checklists.models import Item
from branches.models import Branch
from branches.serializers import BranchSerializer


class TemplateItemSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100)
    description = fields.CharField(max_length=255, default='')
    subsection = fields.CharField(max_length=100)
    min = fields.IntegerField(min_value=-128, max_value=127)
    max = fields.IntegerField(min_value=-128, max_value=127)
    item_type = fields.CharField(max_length=10)

    class Meta:
        model = TemplateItem
        fields = [
            'id',
            'name',
            'description',
            'subsection',
            'min',
            'max',
            'item_type',
            'sequence',
            'value',
            'created_at',
            'updated_at',
        ]


class TemplateSectionSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100)
    description = fields.CharField(max_length=255, default='')
    items = TemplateItemSerializer(many=True, required=True)

    class Meta:
        model = TemplateSection
        fields = [
            'id',
            'name',
            'description',
            'sequence',
            'created_at',
            'updated_at',
            'items',
        ]


class TemplateSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100)
    description = fields.CharField(max_length=255, default='')
    sections = TemplateSectionSerializer(many=True, required=True)
    pass_score = fields.FloatField()

    class Meta:
        model = Template
        fields = [
            'id',
            'name',
            'description',
            'pass_score',
            'created_at',
            'updated_at',
            'sections',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        sections = validated_data.pop('sections')

        with transaction.atomic():
            template = Template.objects.create(
                **validated_data, business=business, user=user)

            for i, section in enumerate(sections):
                items = section.pop('items')
                section.update({'sequence': i})
                section = TemplateSection.objects.create(
                    **section,
                    template=template,
                    user=user
                )

                for j, item in enumerate(items):
                    item.update({'sequence': j})

                    TemplateItem.objects.create(
                        **item,
                        section=section,
                        template=template,
                        user=user
                    )

            return template

    def update(self, template, validated_data):
        user = self.context['request'].user
        sections = validated_data.pop('sections')

        with transaction.atomic():
            template.name = validated_data.get('name')
            template.description = validated_data.get('description')
            template.user = user
            template.save()

            deleted_section_ids = set([s.get('id') for s in sections]) \
                ^ set([s.id for s in template.sections.all()])

            for section in TemplateSection.objects.filter(pk__in=deleted_section_ids).all():
                for item in section.items.all():
                    item.delete()

                section.delete()

            for i, section in enumerate(sections):
                items = section.pop('items')
                section, _ = TemplateSection.objects.update_or_create(
                    pk=section.get('id'),
                    defaults={
                        **section,
                        'sequence': i,
                        'template': template,
                        'user': user
                    }
                )

                deleted_item_ids = set([i.get('id') for i in items])\
                    ^ set([i.id for i in section.items.all()])

                for item in TemplateItem.objects.filter(pk__in=deleted_item_ids).all():
                    item.delete()

                for j, item in enumerate(items):
                    TemplateItem.objects.update_or_create(
                        pk=item.get('id'),
                        defaults={
                            **item,
                            'sequence': j,
                            'section': section,
                            'template': template,
                            'user': user
                        }
                    )

            return template


class ItemSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100)
    description = fields.CharField(max_length=255)
    subsection = fields.CharField(max_length=100)
    min = fields.IntegerField()
    max = fields.IntegerField()
    item_type = fields.CharField(max_length=10)
    comment = fields.CharField(allow_blank=True, max_length=255, default='')
    value = fields.IntegerField()

    class Meta:
        model = Item
        fields = [
            'id',
            'name',
            'description',
            'subsection',
            'min',
            'max',
            'item_type',
            'comment',
            'value',
            'sequence',
            'created_at',
            'updated_at',
        ]


class SectionSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100)
    description = fields.CharField(max_length=255)
    comment = fields.CharField(allow_blank=True, max_length=255, default='')
    items = ItemSerializer(many=True)

    class Meta:
        model = Section
        fields = [
            'id',
            'name',
            'description',
            'comment',
            'items',
            'sequence',
            'created_at',
            'updated_at',
        ]


class ChecklistSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100)
    comment = fields.CharField(max_length=255, default='')
    start_date = fields.DateTimeField()
    date_completed = fields.DateTimeField(required=False)
    pass_score = fields.FloatField()
    sections = SectionSerializer(many=True)

    class Meta:
        model = Checklist
        fields = [
            'id',
            'name',
            'description',
            'comment',
            'start_date',
            'date_completed',
            'sections',
            'template',
            'branch',
            'score',
            'pass_score',
            'passed',
            'created_at',
            'updated_at',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        sections = validated_data.pop('sections')
        template = validated_data.pop('template')

        if template is None:
            raise validators.ValidationError('Invalid template supplied')

        branch = validated_data.pop('branch')

        if branch is None or branch.business.id != business.id:
            raise validators.ValidationError('Invalid branch provided')

        with transaction.atomic():
            checklist = Checklist.objects.create(
                **validated_data,
                template=template,
                user=user,
                branch=branch,
                business=business
            )

            for i, section in enumerate(sections):
                section.pop('id', None)
                section.update({'sequence': i})
                items = section.pop('items')
                section = Section.objects.create(
                    **section, checklist=checklist, user=user)

                for j, item in enumerate(items):
                    item.pop('id', None)
                    item.update({'sequence': j})
                    Item.objects.create(
                        **item,
                        section=section,
                        checklist=checklist,
                        user=user,
                        business=business
                    )

            # calculate the checklist total score and set passed flag
            checklist.set_score()
            checklist.save()

            return checklist


class UpdateChecklistSerializer(ChecklistSerializer):

    class Meta(ChecklistSerializer.Meta):
        read_only_fields = ['business']

    def update(self, checklist, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business

        sections = validated_data.pop('sections')
        branch = validated_data.pop('branch')

        if branch is None or branch.business.id != business.id:
            raise validators.ValidationError('Invalid branch provided')

        with transaction.atomic():
            checklist.name = validated_data.get('name')
            checklist.description = validated_data.get('description')
            checklist.comment = validated_data.get('comment')
            checklist.start_date = validated_data.get('start_date')
            checklist.date_completed = validated_data.get('date_completed')
            checklist.branch = branch
            checklist.user = user
            checklist.save()

            deleted_section_ids = set([s.get('id') for s in sections]) \
                ^ set([s.id for s in checklist.sections.all()])

            for section in Section.objects.filter(pk__in=deleted_section_ids, business=business).all():
                for item in section.items.all():
                    item.delete()

                section.delete()

            for i, section in enumerate(sections):
                items = section.pop('items')
                section, _ = Section.objects.update_or_create(
                    pk=section.get('id'),
                    defaults={
                        **section,
                        'sequence': i,
                        'checklist': checklist,
                        'user': user,
                        'business': business
                    }
                )

                deleted_item_ids = set([i.get('id') for i in items])\
                    ^ set([i.id for i in section.items.all()])

                for item in Item.objects.filter(pk__in=deleted_item_ids, business=business).all():
                    item.delete()

                for j, item in enumerate(items):
                    Item.objects.update_or_create(
                        pk=item.get('id'),
                        defaults={
                            **item,
                            'sequence': j,
                            'section': section,
                            'checklist': checklist,
                            'user': user,
                            'business': business
                        }
                    )

            # calculate the checklist total score and set passed flag
            checklist.set_score()
            checklist.save()

            return checklist


class ViewChecklistSerializer(ChecklistSerializer):
    branch = BranchSerializer()
