module.exports = {
  apps : [{
    name: 'qpx',
    script: 'gunicorn server.wsgi',
    watch: true,
    env: {
    }
  }],
};
