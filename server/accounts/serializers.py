from dj_rest_auth.registration.serializers import RegisterSerializer as DefaultRegisterSerializer
from dj_rest_auth.serializers import PasswordResetSerializer as DefaultPasswordResetSerializer
from rest_framework import serializers

class RegisterSerializer(DefaultRegisterSerializer):
    first_name = serializers.CharField(min_length=2, max_length=150)
    last_name = serializers.CharField(min_length=2, max_length=150)

    def get_cleaned_data(self):
        cleaned_data = super(RegisterSerializer, self).get_cleaned_data()
        return {**cleaned_data, **{
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'is_superuser': True,
        }}

class PasswordResetSerializer(DefaultPasswordResetSerializer):
    def get_email_options(self):
    
        return {
            'subject_template_name': 'accounts/email/password_reset_subject.txt',
            'email_template_name': 'accounts/email/password_reset_email.txt',
            'html_email_template_name': 'accounts/email/password_reset_email.html',
        }
