from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from django.contrib.auth.decorators import login_required
from dj_rest_auth.registration.views import VerifyEmailView


urlpatterns = [
    path('login/', TemplateView.as_view(template_name='accounts/login.html'), name='account-login'),
    path('registration/', TemplateView.as_view(template_name='accounts/registration.html'), name='account-registration'),
    path('password-reset/', TemplateView.as_view(template_name='accounts/password_reset.html'), name='account-password-reset'),
    path('password-reset/confirm', TemplateView.as_view(template_name='accounts/password_reset_confirm.html'), name='account-reset-confirm'),
    path('setup/', login_required(TemplateView.as_view(template_name='accounts/setup.html')), name='account-setup'),
    path('confirm-email/', VerifyEmailView.as_view(), name='account_email_verification_sent'),
]
