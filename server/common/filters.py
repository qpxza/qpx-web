from rest_framework import filters

class BelongsToBusinessFilter(filters.BaseFilterBackend):
    """
    Filter that only allows users to see records belonging to the same business as them
    """
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(business=request.business, business__isnull=False)

class UndeletedFilter(filters.BaseFilterBackend):
    """
    Filter that only allows users to see 'undeleted' - records with deleted_at=null
    """
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(deleted_at__isnull=True)
