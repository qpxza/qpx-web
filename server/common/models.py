from django.db import models
from django.utils import timezone

class TimestampsMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    soft_delete = False

    class Meta:
        abstract = True

    def delete(self, *args, **kwargs):
        # if self.soft_delete or True:
        #     self.deleted_at = timezone.now()
        #     self.save()
        #     return
        
        return super(TimestampsMixin, self).delete(*args, **kwargs)
