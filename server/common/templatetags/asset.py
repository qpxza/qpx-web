from django import template
from server import settings

register = template.Library()

@register.filter
def asset(value):
    return '%s?v=%s' % (value, settings.APP_VERSION)
