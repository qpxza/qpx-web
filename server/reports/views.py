from django.shortcuts import render
from django.utils.dateparse import parse_date
from dateutil import rrule
from datetime import date
from datetime import timedelta
from collections import OrderedDict
from django.db.models import Sum, Value as V, CharField
from django.db.models.functions import Extract, Concat, Cast, LPad, Trunc
from rest_framework import viewsets
from rest_framework import views
from rest_framework import response
from rest_framework import validators
from rest_framework import status
from reports.models import ReportTemplate
from reports.models import Report
from reports.models import ReportItem
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from reports.serializers import ReportTemplateSerializer
from reports.serializers import ReportSerializer
from reports.serializers import ReportItemSerializer
from reports.serializers import ReportItemNameSerializer
from reports.serializers import ViewReportSerializer
from reports.serializers import ReportParseSerializer
from reports.utils import parse_daily_pay_type_report
from base64 import b64encode
from rest_framework import permissions


class TemplatePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('reports.view_reporttemplate')


class ReportPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('reports.view_report')

class ReportTemplateViewset(viewsets.ModelViewSet):
    queryset = ReportTemplate.objects.filter(deleted_at__isnull=True)\
        .order_by('name')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, TemplatePermission]
    serializer_class = ReportTemplateSerializer


class ReportViewset(viewsets.ModelViewSet):
    queryset = Report.objects.prefetch_related('branch', 'items')\
        .filter(deleted_at__isnull=True).order_by('name')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, ReportPermission]
    serializer_class = ReportSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return ViewReportSerializer

        if self.action == 'parse':
            return ReportParseSerializer

        return ReportSerializer

    def parse(self, request):
        serializer = self.get_serializer(data=request.data)

        if not serializer.is_valid():
            return response.Response(serializer.errors, status.HTTP_400_BAD_REQUEST)

        report = request.data.get('report')

        # if not isinstance(report, list) or len(report) == 0:
        #     raise validators.ValidationError('Unable to read file')

        data = {
            'name': "",
        }
        lines = []

        with report.open() as f:
            for i, l in enumerate(f):
                lines.append(l.decode("utf-8").rstrip())

        if len(lines) == 0:
            raise validators.ValidationError('Unable to read file')

        if lines[0] == "Daily Paytype Report":
            try:
                data = parse_daily_pay_type_report(lines)
            except e:
                raise validators.ValidationError(
                    'Unable to read Daily Paytype Report. Please check that it is formatted correctly')

        return response.Response(data)


class ReportItemViewset(viewsets.ModelViewSet):
    queryset = ReportItem.objects.filter(
        deleted_at__isnull=True).order_by('name')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated]

    def names(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def get_queryset(self):
        if self.action == 'names':
            return self.queryset.only('name').distinct('name').order_by('name')

        return super(ReportItemViewset, self).get_queryset()

    def paginate_queryset(self, queryset):
        if self.action == 'names':
            return None

        return super(ReportItemViewset, self).paginate_queryset(queryset)

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'names':
            return ReportItemNameSerializer

        return ReportItemSerializer


class ReportItemSummaryAPIView(views.APIView):
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):
        names = request.query_params.getlist('name')
        from_date = parse_date(request.query_params.get('from_date'))
        to_date = parse_date(request.query_params.get('to_date'))
        interval = self.get_interval(from_date, to_date)
        branches = request.query_params.getlist('branches', [])

        # django orm doesn't like non-numeric branch_ids hence why we strip them out
        branches = [b for b in branches if b.strip()]
        params = {
            'business': request.business,
            'date__range': (from_date, to_date),
        }

        if len(branches) > 0:
            params['branch__in'] = branches

        data = {}

        for name in names:    
            values = ReportItem.objects.filter(**params, name=name)\
                .annotate(interval=Trunc('date', interval))\
                .values('interval')\
                .annotate(total=Sum('value'))\
                .values_list('interval', 'total')

            values = self.format_results(values, from_date, to_date, interval)
            data[name] = values

        return response.Response({'data': data, 'interval': interval})

    def get_interval(self, from_date, to_date):
        '''
            Less than one week: day
            More than a week and less than a month: week
            More than a month and less than a year: month
            More than a year: year
        '''
        delta = to_date - from_date

        if delta.days < 0:
            raise validators.ValidationError('Invalid date range')

        if delta.days > 365.25*2:
            return 'year'

        if delta.days > 31:
            return 'month'

        if delta.days > 7:
            return 'week'

        return 'day'

    def format_results(self, values, from_date, to_date, interval):
        '''
            Round down all dates to the beginning of each interval
        '''
        freq = rrule.DAILY
        date_format = '%Y-%m-%d'

        if interval == 'week':
            from_date = from_date - timedelta(days=from_date.weekday() % 7)
            to_date = to_date - timedelta(days=to_date.weekday() % 7)
            freq = rrule.WEEKLY

        if interval == 'month':
            from_date = from_date.replace(day=1)
            to_date = to_date.replace(day=1)
            freq = rrule.MONTHLY
            date_format = '%Y-%m'

        if interval == 'year':
            from_date = from_date.replace(month=1)
            to_date = to_date.replace(month=1)
            freq = rrule.YEARLY
            date_format = '%Y'

        intervals = list(rrule.rrule(
            freq=freq, dtstart=from_date, until=to_date))
        intervals = OrderedDict([(i.strftime(date_format), 0)
                                 for i in intervals])

        for i, v in values:
            intervals[i.strftime(date_format)] = v

        return intervals
