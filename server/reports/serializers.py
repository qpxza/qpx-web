from rest_framework import serializers
from rest_framework import fields
from rest_framework import validators
from django.db import transaction
from django.db import models
from users.models import User
from businesses.models import Business
from reports.models import Report
from reports.models import ReportTemplate
from reports.models import ReportItem
from branches.models import Branch
from branches.serializers import BranchSerializer

class ReportTemplateSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100)
    items = fields.JSONField()

    class Meta:
        model = ReportTemplate
        fields = [
            'id',
            'name',
            'items',
            'created_at',
            'updated_at',
        ]

    def create(self, validated_data):
        return ReportTemplate.objects.create(
                business=self.context['request'].business,
                **validated_data
            )

class ReportItemSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100)
    value = fields.DecimalField(15, 6)
    date = fields.DateField(required=False)

    class Meta:
        model = ReportTemplate
        fields = [
            'id',
            'name',
            'value',
            'date',
            'created_at',
            'updated_at',
        ]

class ReportItemNameSerializer(serializers.ModelSerializer):
    class Meta(ReportItemSerializer.Meta):
        fields = ['name',]

class ReportSerializer(serializers.ModelSerializer):
    name = fields.CharField(min_length=1, max_length=100)
    date = fields.DateField()
    items = ReportItemSerializer(many=True)

    class Meta:
        model = Report
        fields = [
            'id',
            'name',
            'date',
            'items',
            'branch',
            'created_at',
            'updated_at',
        ]
        read_only_fields = ['business']

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        items = validated_data.pop('items')
        branch = validated_data.get('branch')
        date = validated_data.get('date')

        if branch is None or branch.business.id != business.id:
            raise validators.ValidationError('Invalid branch provided')

        with transaction.atomic():
            report =  Report.objects.create(**validated_data, business=business, user=user)

            for item in items:
                ReportItem.objects.create( **item,  date=date, report=report, branch=branch, business=business, user=user)

        return report

    def update(self, report, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        items = validated_data.pop('items')
        branch = validated_data.get('branch')
        date = validated_data.get('date')

        if branch is None or branch.business.id != business.id:
            raise validators.ValidationError('Invalid branch provided')

        with transaction.atomic():
            report.name = validated_data.get('name')
            report.date = validated_data.get('date')
            report.branch = validated_data.get('branch')
            report.user = user
            report.save()

            deleted_item_ids = set([i.get('id') for i in items])\
                    ^ set([i.id for i in report.items.all()])

            for item in ReportItem.objects.filter(pk__in=deleted_item_ids).all():
                item.delete()

            for item in items:
                ReportItem.objects.update_or_create(
                    pk=item.get('id'),
                    business=business,
                    defaults={
                        **item,
                        'report': report,
                        'branch': report.branch,
                        'user': user
                    }
                )
    
        return report

class ViewReportSerializer(ReportSerializer):
    branch = BranchSerializer()

class ReportParseSerializer(serializers.Serializer):
    report = fields.FileField(max_length=1024*1024)
