from django.db import models
from users.models import User
from businesses.models import Business
from branches.models import Branch
from common.models import TimestampsMixin
from users.models import OwnerMixin

class Report(TimestampsMixin, OwnerMixin):
    name = models.CharField(max_length=100)
    user = models.ForeignKey(User, related_name="reports", on_delete=models.CASCADE)
    branch = models.ForeignKey(Branch, related_name="reports", on_delete=models.CASCADE)
    business = models.ForeignKey(Business, related_name="reports", on_delete=models.CASCADE)
    date = models.DateField()

class ReportItem(TimestampsMixin, OwnerMixin):
    name = models.CharField(max_length=100)
    value = models.DecimalField(max_digits=15, decimal_places=6)
    user = models.ForeignKey(User, related_name="report_items", on_delete=models.CASCADE)
    report = models.ForeignKey(Report, related_name="items", on_delete=models.CASCADE)
    branch = models.ForeignKey(Branch, related_name="report_items", on_delete=models.CASCADE)
    business = models.ForeignKey(Business, related_name="report_items", on_delete=models.CASCADE)
    date = models.DateField()

class ReportTemplate(TimestampsMixin, OwnerMixin):
    name = models.CharField(max_length=100)
    items = models.JSONField()
    business = models.ForeignKey(Business, related_name="report_templates", on_delete=models.CASCADE)
