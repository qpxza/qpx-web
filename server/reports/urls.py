from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from reports.views import ReportTemplateViewset
from reports.views import ReportViewset
from reports.views import ReportItemViewset
from reports.views import ReportItemSummaryAPIView

report_template_list =  ReportTemplateViewset.as_view({
    'get': 'list',
    'post': 'create',
})

report_template_detail =  ReportTemplateViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

report_list =  ReportViewset.as_view({
    'get': 'list',
    'post': 'create',
})

report_detail =  ReportViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

report_parse =  ReportViewset.as_view({
    'post': 'parse',
})

report_item_list =  ReportItemViewset.as_view({
    'get': 'list',
    'post': 'create',
})

report_item_names =  ReportItemViewset.as_view({
    'get': 'names',
})

report_item_summary =  ReportItemSummaryAPIView.as_view()

urlpatterns = [
    path('api/v1/reports/', report_list, name='report-list'),
    path('api/v1/reports/<int:pk>/', report_detail, name='report-detail'),
    path('api/v1/reports/parse/', report_parse, name='report-parse'),
    path('api/v1/reports/templates/', report_template_list, name='report-template-list'),
    path('api/v1/reports/templates/<int:pk>/', report_template_detail, name='report-template-detail'),
    path('api/v1/reports/items/', report_item_list, name='report-item-list'),
    path('api/v1/reports/items/names/', report_item_names, name='report-item-names'),
    path('api/v1/reports/items/summary/', report_item_summary, name='report-item-summary'),
]
