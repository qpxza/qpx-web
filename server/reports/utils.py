import re


def parse_daily_pay_type_report(report):
    '''
     I am not proud of this but it does the job...somewhat :-)
    '''
    data = {
        'name': "Daily Paytype Report",
        'items': []
    }

    keys = [
        "Average per Head",
        "No of Covers",
        "Average per Invoice",
        "No of Invoices",
    ]

    in_sales = False

    # info at the bottom of the report
    for i, line in enumerate(report):
        if "Report Date" in line:
            data['date'] = line[line.index(":")+1:].strip()

        for key in keys:
            if key in line:
                data['items'].append({
                    'name': key,
                    'value': float(line[line.index(":")+1:].strip())
                })

        # Sales info at the top of the report
        if re.match("^\s+----------$", line):
            if in_sales and i < len(report) - 2:
                # peek to get the total sales and turn off sales capturing
                data['items'].append({
                    'name': "Total sales",
                    'value': float(report[i+1][-10:].strip())
                })

            in_sales = False

        key = line[5:20].strip()

        if in_sales and key:
            data['items'].append({
                'name': key,
                'value': float(line[-10:].strip())
            })

        if re.match(r"^Code Name.+Sales$", line):
            in_sales = True

    return data
