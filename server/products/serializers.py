from rest_framework import serializers
from rest_framework import fields
from rest_framework import validators
from django.db import transaction
from django.db import models
from users.models import User
from businesses.models import Business
from products.models import Product
from suppliers.serializers import SupplierSerializer

class ProductSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100)
    description = fields.CharField(max_length=100, default='')
    qty = fields.DecimalField(max_digits=10, decimal_places=2)
    unit = fields.CharField(max_length=100, default='')
    sku = fields.CharField(max_length=100, default='')
    price = fields.DecimalField(max_digits=10, decimal_places=2)
    description = fields.CharField(max_length=100, default='')

    class Meta:
        model = Product
        fields = [
            'id',
            'name',
            'description',
            'qty',
            'unit',
            'sku',
            'price',
            'created_at',
            'updated_at',
            'supplier',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        product = Product.objects.create(**validated_data, user=user, business=business)

        return product

class ViewProductSerializer(ProductSerializer):
    supplier = SupplierSerializer()
