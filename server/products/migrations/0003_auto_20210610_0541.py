# Generated by Django 3.1.3 on 2021-06-10 05:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20210609_1930'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='qty',
            field=models.DecimalField(decimal_places=2, max_digits=10),
        ),
    ]
