from django.db import models
from users.models import User
from suppliers.models import Supplier
from businesses.models import Business
from branches.models import Branch
from common.models import TimestampsMixin
from users.models import OwnerMixin
from businesses.models import BusinessMixin

class Product(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    qty = models.DecimalField(max_digits=10, decimal_places=2)
    unit = models.CharField(max_length=100)
    sku = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    supplier = models.ForeignKey(
        Supplier, related_name="products", on_delete=models.CASCADE)
