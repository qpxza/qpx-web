from rest_framework import viewsets
from products.models import Product
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from products.filters import BelongsToSupplierFilter
from products.serializers import ProductSerializer, ViewProductSerializer
from rest_framework import permissions


class ProductPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('products.view_product')

class ProductViewset(viewsets.ModelViewSet):
    queryset = Product.objects.filter(deleted_at__isnull=True).order_by('name')
    filter_backends = [BelongsToBusinessFilter, BelongsToSupplierFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, ProductPermission]
    serializer_class = ProductSerializer

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return ViewProductSerializer

        return ProductSerializer

    def paginate_queryset(self, queryset):
        return None
