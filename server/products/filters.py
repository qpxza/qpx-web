from rest_framework import filters

class BelongsToSupplierFilter(filters.BaseFilterBackend):
    """
    Filter that only allows users to see records belonging to a particular supplier
    given that the supplier is provided in the query string
    """
    def filter_queryset(self, request, queryset, view):
        if not request.query_params.get('supplier_id', None):
            return queryset

        return queryset.filter(supplier=request.query_params.get('supplier_id'))
