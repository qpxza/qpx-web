from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from products.views import ProductViewset
from products.models import Product

product_list =  ProductViewset.as_view({
    'get': 'list',
    'post': 'create',
})

product_detail =  ProductViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

urlpatterns = [
    path('api/v1/products/', product_list, name='product-list'),
    path('api/v1/products/<int:pk>/', product_detail, name='product-detail'),
]
