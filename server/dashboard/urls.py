from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from django.contrib.auth.decorators import login_required
from dashboard.views import DashboardView

urlpatterns = [
    path('dashboard/',  login_required(DashboardView.as_view()), name="dashboard"),
    re_path(r'^dashboard/(?P<business_id>\d+)/(?P<slug>[\-a-zA-Z0-9_]+)/',  login_required(DashboardView.as_view()), name="dashboard-business"),
]
