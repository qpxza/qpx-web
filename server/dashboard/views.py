from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.http import Http404
from django.views import View
from django.views.generic.base import TemplateView

class DashboardView(TemplateView):
    template_name = 'dashboard/index.html'

    def get(self, request, *args, **kwargs):
        if not request.user.businesses.exists():
            return HttpResponseRedirect(reverse('account-setup'))

        business_id = kwargs.get('business_id')
        slug = kwargs.get('slug')

        if business_id is None or slug is None:
            business = request.user.businesses.all().first()

            return HttpResponseRedirect(business.base_path)

        if not request.user.businesses.filter(pk=business_id).exists():
            raise Http404('Dashboard not found')

        kwargs.update({
            'basename': reverse('dashboard-business', args=(int(business_id), slug)),
            'business_id': business_id,
            'auth_user_id': request.user.id,
            'auth_user_business_id': request.user.business.id,
        })

        return super(DashboardView, self).get(request, *args, **kwargs)
