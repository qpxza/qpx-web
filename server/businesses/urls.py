from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from businesses.views import BusinessViewSet

business_list = BusinessViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

business_detail = BusinessViewSet.as_view({
    'get': 'retrieve',
    'put': 'update'
})

urlpatterns = [
    path('api/v1/businesses/', business_list, name='business-list'),
    path('api/v1/businesses/<int:pk>/', business_detail, name='business-detail'),
]
