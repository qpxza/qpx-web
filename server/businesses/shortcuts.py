from businesses.models import Business
from django.db.models import Q


def get_current_business(request):
    if not request.user.is_authenticated:
        return None

    return Business.objects.filter(users=request.user, pk=request.GET.get('business_id'))\
        .first()
