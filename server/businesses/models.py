from django.db import models
from django.contrib.auth import models as auth_models
from django.utils.text import slugify
from django.urls import reverse
from common.models import TimestampsMixin


class Business(TimestampsMixin):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    groups = models.ManyToManyField(auth_models.Group)

    @property
    def slug(self):
        return slugify(self.name)

    @property
    def base_path(self):
        business_id = self.id
        slug = self.slug

        return reverse('dashboard-business', args=(business_id, slug))

    def __str__(self):
        return self.name


class BusinessMixin(models.Model):
    business = models.ForeignKey(
        Business, default=None, related_name='+', null=True, on_delete=models.DO_NOTHING)

    class Meta:
        abstract = True
