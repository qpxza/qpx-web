from businesses.shortcuts import get_current_business


class CurrentBusinessMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if request.GET.get('business_id') is not None:
            request.business = get_current_business(request)

        return self.get_response(request)
