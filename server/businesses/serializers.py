from businesses.models import Business
from rest_framework import serializers
from rest_framework import fields
from users.models import User



class BusinessSerializer(serializers.ModelSerializer):
    name = serializers.CharField(min_length=2, max_length=50)
    email = serializers.EmailField()
    

    class Meta:
        model = Business
        fields = [
            'id',
            'name',
            'email',
            'slug',
            'created_at',
            'updated_at',
            'deleted_at',
        ]

    def create(self, validated_data):
        # if they are already linked to a business with the same name, return the existing business
        business = Business.objects.filter(users__in=[self.context['request'].user], name=validated_data.get('name')).first()

        if business is not None:
            return business

        return Business.objects.create(**validated_data)
