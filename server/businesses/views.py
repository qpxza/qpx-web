from django.shortcuts import render
from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import serializers
from rest_framework import response
from rest_framework import status
from businesses.models import Business
from businesses.serializers import BusinessSerializer
from common.filters import UndeletedFilter

class BusinessViewSet(viewsets.ModelViewSet):
    serializer_class = BusinessSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = [UndeletedFilter]

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        owner = self.request.user
        business = serializer.save()
        owner.businesses.add(business)
        owner.business = business
        owner.save()

        serializer = self.get_serializer_class()(business)
        headers = self.get_success_headers(serializer.data)

        return response.Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
