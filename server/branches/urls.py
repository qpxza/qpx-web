from django.urls import path
from branches.views import BranchViewset
from users.views import UserViewset

branch_list =  BranchViewset.as_view({
    'get': 'list',
    'post': 'create',
})

branch_detail =  BranchViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'post': 'add_user',
    'delete': 'remove_user',
})

branch_users =  UserViewset.as_view({
    'get': 'list',
})

urlpatterns = [
    path('api/v1/branches/', branch_list, name='branch-list'),
    path('api/v1/branches/<int:pk>/',  branch_detail, name='branch-detail'),
    path('api/v1/branches/<int:branch>/users/',  branch_users, name='branch-users'),
    path('api/v1/branches/<int:pk>/users/add/',  branch_detail, name='branch-users-add'),
    path('api/v1/branches/<int:pk>/users/remove/',  branch_detail, name='branch-users-remove'),
]
