from django.db import models
from users.models import User
from users.models import OwnerMixin
from businesses.models import Business
from common.models import TimestampsMixin

class Branch(TimestampsMixin, OwnerMixin):
    name = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    suburb = models.CharField(max_length=50)
    street_address = models.CharField(max_length=200)
    postal_code = models.CharField(max_length=10) # some countries might need longer postal codes
    country = models.CharField(max_length=20)
    phone = models.CharField(max_length=20)
    email = models.EmailField()
    hq = models.BooleanField(default=False)
    users = models.ManyToManyField(User)
    business = models.ForeignKey(Business, related_name="branches", on_delete=models.deletion.CASCADE)

    def __str__(self):
        return self.name

