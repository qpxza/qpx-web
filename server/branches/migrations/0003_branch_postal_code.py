# Generated by Django 3.1.3 on 2020-11-26 20:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('branches', '0002_auto_20201126_2014'),
    ]

    operations = [
        migrations.AddField(
            model_name='branch',
            name='postal_code',
            field=models.CharField(max_length=10),
            preserve_default=False,
        ),
    ]
