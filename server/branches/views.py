from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import response
from rest_framework import status
from rest_framework import exceptions
from branches.serializers import BranchSerializer, UpdateBranchSerializer
from branches.models import Branch
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from users.views import UserViewset
from users.models import User

class BranchPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        # everyone can list branches
        if view.action == 'list':
            return True

        return request.user.has_perm('branches.view_branch')

class BranchViewset(viewsets.ModelViewSet):
    queryset = Branch.objects.all().order_by('name')
    permission_classes = [permissions.IsAuthenticated, BranchPermission]
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    serializer_class = BranchSerializer

    def get_serializer_class(self):
        if self.action in ['update']:
            return UpdateBranchSerializer

        return BranchSerializer

    def add_user(self, *args, **kwargs):
        branch = Branch.objects.get(pk=kwargs.get('pk'))
        users = User.objects.filter(pk__in=self.request.data).all()

        for u in users:
            branch.users.add(u)

        return response.Response(status=status.HTTP_201_CREATED)

    def remove_user(self, *args, **kwargs):
        branch = Branch.objects.get(pk=kwargs.get('pk'))
        users = User.objects.filter(
            businesses=branch.business, pk__in=self.request.data).all()

        for u in users:
            branch.users.remove(u)

        return response.Response(status=status.HTTP_201_CREATED)
