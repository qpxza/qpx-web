from branches.models import Branch
from rest_framework import serializers
from rest_framework import validators
from django.db import transaction

class BranchSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=50)
    city = serializers.CharField(max_length=50)
    suburb = serializers.CharField(max_length=50)
    street_address = serializers.CharField(max_length=200)
    postal_code = serializers.CharField(max_length=10)
    country = serializers.CharField(max_length=20)
    phone = serializers.CharField(max_length=20)
    email = serializers.EmailField(required=True)
    hq = serializers.BooleanField(default=False)

    class Meta:
        model = Branch
        fields = [
            'id',
            'name',
            'city',
            'suburb',
            'street_address',
            'postal_code',
            'country',
            'phone',
            'email',
            'hq',
            'created_at',
            'updated_at',
            'deleted_at',
            'business',
        ]
        read_only_fields = ['business']

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business

        with transaction.atomic():
            if validated_data.get('hq'):
                Branch.objects.filter(business=business).update(hq=False)

            return Branch.objects.create(**validated_data, user=user, business=business)
        
class CreateBranchSerializer(BranchSerializer):
    class Meta(BranchSerializer.Meta):
        read_only_fields = []

class UpdateBranchSerializer(BranchSerializer):
    def update(self, branch, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business

        if branch.business.id != business.id:
            raise validators.ValidationError('Invalid branch')

        with transaction.atomic():
            if validated_data.get('hq'):
                Branch.objects.filter(business=business).update(hq=False)

            return super(UpdateBranchSerializer, self).update(branch, validated_data)
        

