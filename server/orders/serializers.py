from rest_framework import serializers
from rest_framework import fields
from rest_framework import validators
from django.db import transaction
from django.db import models
from django.db import IntegrityError, transaction
from users.models import User
from businesses.models import Business
from products.models import Product
from orders.models import Order
from orders.models import Item
from products.serializers import ProductSerializer
from suppliers.serializers import SupplierSerializer
from branches.serializers import BranchSerializer

class ItemSerializer(serializers.ModelSerializer):
    qty = fields.DecimalField(max_digits=10, decimal_places=2)

    class Meta:
        model = Item
        fields = [
            'id',
            'qty',
            'created_at',
            'updated_at',
            'product',
        ]


class ViewItemSerializer(ItemSerializer):
    product = ProductSerializer()


class OrderSerializer(serializers.ModelSerializer):
    description = fields.CharField(max_length=100, default='')
    status = fields.IntegerField(default=Order.STATUS_PROCESSING) 
    items = ItemSerializer(many=True)

    class Meta:
        model = Order
        fields = [
            'id',
            'description',
            'status',
            'created_at',
            'updated_at',
            'supplier',
            'items',
            'branch',
            'total',
            'get_status_display',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        items = validated_data.pop('items')

        if validated_data.get('supplier').business.id != business.id:
            raise validators.ValidationError('Invalid supplier provided')


        if validated_data.get('branch').business.id != business.id:
            raise validators.ValidationError('Invalid branch provided')

        with transaction.atomic():
            order = Order.objects.create(
                **validated_data, user=user, business=business)

            for item in items:
                Item.objects.create(
                    qty=item.get('qty'),
                    product=item.get('product'),
                    order=order,
                    user=user,
                    business=business
                )

            return order

    def update(self, order, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        status = validated_data.get('status')

        if order.status != status:
            if order.status in [Order.STATUS_COMPLETED, Order.STATUS_CANCELLED]:
                raise validators.ValidationError('A completed or cancelled order can not be modified. Please create a new order!')

        with transaction.atomic():
            order.description = validated_data.get('description')
            order.user = user
            order.update_status(status)
            order.save()

        return order


class ViewOrderSerializer(OrderSerializer):
    supplier = SupplierSerializer()
    branch = BranchSerializer()
    items = ViewItemSerializer(many=True)



class UpdateOrderSerializer(OrderSerializer):
    items = ItemSerializer(many=True, required=False)
    branch = BranchSerializer(required=False)
