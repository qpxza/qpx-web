from django.db import models
from django.dispatch import receiver
from django.db import transaction
from users.models import User
from suppliers.models import Supplier
from businesses.models import Business
from branches.models import Branch
from common.models import TimestampsMixin
from users.models import OwnerMixin
from businesses.models import BusinessMixin
from products.models import Product
from notifications.models import Notification



class Order(TimestampsMixin, OwnerMixin, BusinessMixin):
    STATUS_PROCESSING = 1
    STATUS_COMPLETED = 2
    STATUS_CANCELLED = 4
    STATUS = [
        (STATUS_PROCESSING, "Processing"),
        (STATUS_COMPLETED, "Completed"),
        (STATUS_CANCELLED, "Cancelled"),
    ]

    description = models.CharField(max_length=100)
    status = models.IntegerField(choices=STATUS)
    supplier = models.ForeignKey(
        Supplier, related_name="orders", on_delete=models.CASCADE)
    branch = models.ForeignKey(
        Branch, related_name="orders", on_delete=models.CASCADE)

    @property
    def total(self):
        return sum([i.product.price * i.qty for i in self.items.all()])

    def update_status(self, status):
        if self.status != status and status == self.STATUS_CANCELLED:
            #todo: use signal
            for item in self.items.all():
                item.product.qty += item.qty
                item.product.save()

        self.status = status


class Item(TimestampsMixin, OwnerMixin, BusinessMixin):
    qty = models.DecimalField(max_digits=10, decimal_places=2)
    product = models.ForeignKey(
        Product, related_name="items", on_delete=models.CASCADE)
    order = models.ForeignKey(
        Order, related_name="items", on_delete=models.CASCADE)


# todo: move into separate module
@receiver(models.signals.post_save, sender=Order)
def notify(sender, **kwargs):
    order = kwargs.get("instance")

    #  users can belong to multiple branches
    #  use set ensure we don't send duplicate notifications to our users
    recipients = set(order.branch.users.all()) | set([order.user])
    hq = Branch.objects.filter(business=order.business, hq=True).first()

    if hq is not None:
        recipients |= set(hq.users.all())

    title = "Order #%d updated" % order.id
    body = "There is an update to order #%d" % order.id
    url = "/orders/%d" % (order.id)

    if kwargs.get('created'):
        title = "New order created"
        body = "Order #%d has been created" % order.id

    with transaction.atomic():
        for r in recipients:
            Notification.objects.create(business=order.business, user=r, **{
                'title': title,
                'body': body,
                'url': url,
            })


@receiver(models.signals.post_save, sender=Item)
def item_created(sender, **kwargs):
    item = kwargs.get("instance")
    item.product.qty -= item.qty
    item.product.save()
