from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from orders.views import OrderViewset
from orders.models import Order

order_list =  OrderViewset.as_view({
    'get': 'list',
    'post': 'create',
})

order_detail =  OrderViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

urlpatterns = [
    path('api/v1/orders/', order_list, name='order-list'),
    path('api/v1/orders/<int:pk>/', order_detail, name='order-detail'),
]
