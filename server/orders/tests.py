from django.test import TestCase
from orders.models import Order

class OrderTestCase(TestCase):
    def setUp():
        pass

    def test_complete_order_updates_product_quantity(self):
        """Completed orders should reduce product count"""
        self.assertEqual(1, 1)
