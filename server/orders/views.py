from rest_framework import viewsets
from products.models import Product
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from orders.filters import BelongsToSupplierFilter
from orders.serializers import OrderSerializer, ViewOrderSerializer, UpdateOrderSerializer
from orders.serializers import Order
from rest_framework import permissions

class OrderPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('orders.view_order')

class OrderViewset(viewsets.ModelViewSet):
    queryset = Order.objects.prefetch_related('items__product', 'supplier').filter(deleted_at__isnull=True).order_by('id')
    filter_backends = [BelongsToBusinessFilter, BelongsToSupplierFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, OrderPermission]
    serializer_class = OrderSerializer

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return ViewOrderSerializer

        if self.action in ['update']:
            return UpdateOrderSerializer

        return OrderSerializer

