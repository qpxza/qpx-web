from rest_framework import viewsets
from widgets.models import Widget
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from widgets.serializers import WidgetSerializer
from rest_framework import permissions
from rest_framework import response
from django.db import models
class WidgetPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('widgets.view_widget')


class WidgetViewset(viewsets.ModelViewSet):
    queryset = Widget.objects.filter(deleted_at__isnull=True)\
        .order_by('-created_at', 'sequence')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, WidgetPermission]
    serializer_class = WidgetSerializer

    def reorder(self, request):
        for i, id in enumerate(request.data):
            try:
                widget = self.get_queryset().get(pk=id)
                widget.sequence = i + 1
                widget.save()
            except models.ObjectDoesNotExist:
                pass

        return response.Response({})
