from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from widgets.views import WidgetViewset
from widgets.models import Widget

widget_list =  WidgetViewset.as_view({
    'get': 'list',
    'post': 'create',
})

widget_detail =  WidgetViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

widget_reorder= WidgetViewset.as_view({
    'put': 'reorder',
})

urlpatterns = [
    path('api/v1/widgets/', widget_list, name='widget-list'),
    path('api/v1/widgets/<int:pk>/', widget_detail, name='widget-detail'),
    path('api/v1/widgets/reorder/', widget_reorder, name='widget-reorder'),
]
