from rest_framework import serializers
from rest_framework import fields
from rest_framework import validators
from django.db import transaction
from django.db import models
from users.models import User
from businesses.models import Business
from widgets.models import Widget

class WidgetSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100)
    widget_type = fields.CharField(max_length=20)
    definition = fields.JSONField()

    class Meta:
        model = Widget
        fields = [
            'id',
            'name',
            'widget_type',
            'definition',
            'sequence',
            'created_at',
            'updated_at',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business

        return Widget.objects.create(**validated_data, user=user, business=business)
