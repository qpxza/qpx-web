from django.db import models
from users.models import User
from businesses.models import Business
from branches.models import Branch
from common.models import TimestampsMixin
from users.models import OwnerMixin

class Widget(TimestampsMixin, OwnerMixin):
    name = models.CharField(max_length=100)
    widget_type = models.CharField(max_length=20)
    definition = models.JSONField()
    user = models.ForeignKey(User, related_name="widgets", on_delete=models.CASCADE)
    business = models.ForeignKey(Business, related_name="widgets", on_delete=models.CASCADE)
    sequence = models.IntegerField(default=0)
