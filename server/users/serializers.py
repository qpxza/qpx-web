from users.models import User
from groups.serializers import GroupSerializer
from businesses.serializers import BusinessSerializer
from rest_framework import serializers
from django.utils.translation import gettext as _
from django.db import IntegrityError, transaction

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, required=True)

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'id_number',
            'cell_number',
            'dob',
            'employee_number',
            'is_active',
            'date_joined',
            'last_login',
            'groups',
            'created_at',
            'updated_at',
            'deleted_at',
            'business',
        ]
        read_only_fields = ['email', 'username', 'business']

class CreateUserSerializer(UserSerializer):
    groups = None

    class Meta(UserSerializer.Meta):
        read_only_fields = []

    def create(self, validated_data):

        with transaction.atomic():
            business = self.context['request'].business
            groups = validated_data.pop('groups')
            user = User.objects.create(**validated_data, business=business)

            for group in groups:
                user.groups.add(group)

            return user
        
class UpdateUserSerializer(UserSerializer):
    groups = None

    def update(self, user, validated_data):
        with transaction.atomic():
            business = self.context['request'].business
            groups = validated_data.pop('groups')
            user = super(UpdateUserSerializer, self).update(user, validated_data)
            user.business = business
            user.groups.clear()
            
            for group in groups:
                user.groups.add(group)

            user.save()

            return user

class ViewUserSerializer(UserSerializer):
    businesses = BusinessSerializer(many=True)

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ['businesses']

class InviteSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    business = BusinessSerializer()

    class Meta:
        fields = [
            'id',
            'user',
            'business',
            'token',
        ]
