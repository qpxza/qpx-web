from django.core.management.base import BaseCommand, CommandError
from django.db import DatabaseError, transaction
from users.models import User


class Command(BaseCommand):
    help = 'Migrates users from linking to single business to multiple businesses'

    def handle(self, *args, **kwargs):
        users = User.objects.all()

        try:
            with transaction.atomic():
                for user in users:
                    if user.business is not None:
                        user.businesses.add(user.business)
        except DatabaseError as e:
            raise CommandError('Failed to migrate users: ' + (e))

        self.stdout.write(self.style.SUCCESS('Users successfully migrated'))
