from django.db import models
from django.contrib.auth.models import AbstractUser
from businesses.models import Business
from common.models import TimestampsMixin
from django.db.models.signals import post_save
from django.dispatch import receiver


class User(AbstractUser, TimestampsMixin):
    email = models.EmailField(unique=True)
    id_number = models.CharField(max_length=20, default="")
    employee_number = models.CharField(max_length=20, default="")
    dob = models.DateField(default="1970-01-01")
    cell_number = models.CharField(max_length=20, default="")
    business = models.ForeignKey(
        Business, related_name="+", on_delete=models.CASCADE, null=True, blank=True)
    businesses = models.ManyToManyField(Business, related_name="users")


class OwnerMixin(models.Model):
    user = models.ForeignKey(
        User, default=None, null=True, related_name="+", on_delete=models.DO_NOTHING)

    class Meta:
        abstract = True


class Invite(models.Model):
    user = models.ForeignKey(
        User, related_name='invites', on_delete=models.CASCADE)
    business = models.ForeignKey(
        Business, related_name='invites', on_delete=models.CASCADE)
    token = models.CharField(max_length=255)


@receiver(post_save, sender=User)
def init_user(sender, **kwargs):
    if not kwargs.get('created'):
        return

    user = kwargs.get('instance')

    if user.business is None:
        user.groups.add(1)
