from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework import validators
from rest_framework import response
from rest_framework import status
from rest_framework import pagination
from django import http
from django.views.generic import base as views
from users.serializers import UserSerializer
from users.serializers import CreateUserSerializer
from users.serializers import UpdateUserSerializer
from users.serializers import ViewUserSerializer
from users.models import User
from users.models import Invite
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from django.utils.crypto import get_random_string
from django.core.mail import send_mail
from django.core import exceptions
from django.urls import reverse
from django.contrib.auth.hashers import make_password
from django.template.loader import get_template
from django.core.validators import validate_email
from anymail import exceptions as mail_exceptions

class UserPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        # user must be able to view their own record
        # ideally, the user retrieve endpoint must be in it's own view to simplify permissions
        if view.action == 'retrieve' and request.user.id == view.kwargs.get('pk'):
            return True

        return request.user.has_perm('users.view_user')

#todo: fix this hack
class LargeResultsSetPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000

class UserViewset(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, UserPermission]
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    pagination_class = LargeResultsSetPagination
    html_email_template = get_template('users/emails/credentials.html')
    text_email_template = get_template('users/emails/credentials.txt')

    def get_serializer_class(self):
        if self.action == 'create':
            return CreateUserSerializer

        if self.action == 'update':
            return UpdateUserSerializer

        if self.action == 'retrieve':
            return ViewUserSerializer

        return UserSerializer

    def get_queryset(self):
        queryset = User.objects.order_by('first_name', 'last_name')

        exclude_branch = self.request.query_params.get('branch_not_in', None)

        if exclude_branch is not None:
            queryset = queryset.exclude(branch=exclude_branch)

        if self.kwargs.get('branch') is not None:
            queryset = queryset.filter(branch=self.kwargs.get('branch'))

        return queryset

    def perform_create(self, serializer):
        if self.request.business is None:
            raise validators.ValidationError('business_id field is required')

        # todo: check if user has a business setup
        password = get_random_string(8)
        subject = 'Your QPX account details'
        login_url = self.request.build_absolute_uri(reverse('account-login'))
        email = serializer.validated_data.get('email')
        context = {
            'email': email,
            'password': password,
            'login_url': login_url,
        }
        html_message = self.html_email_template.render(context)
        text_message = self.text_email_template.render(context)

        try:
            # todo: offload mail to SQS/Lambda/SNS
            send_mail(
                subject,
                text_message,
                None,
                [email],
                fail_silently=False,
                html_message=html_message
            )

            user = serializer.save()
            user.password = make_password(password)
            user.business = self.request.business
            user.save()

            self.request.business.users.add(user)
        except mail_exceptions.AnymailRequestsAPIError:
            raise validators.ValidationError('Invalid email address provided')

    def deactivate(self, request, *args, **kwargs):
        user = User.objects.get(pk=kwargs.get('pk'))

        if self.request.business is None:
            raise validators.ValidationError('business_id field is required')

        user.is_active = False
        user.save()

        return response.Response(None, status.HTTP_204_NO_CONTENT)

    def activate(self, request, *args, **kwargs):
        user = User.objects.get(pk=kwargs.get('pk'))

        if self.request.business is None:
            raise validators.ValidationError('business_id field is required')

        user.is_active = True
        user.save()

        return response.Response(self.get_serializer(user).data, status.HTTP_200_OK)

class InvitePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('users.view_invite')

class InviteViewset(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, InvitePermission]
    filter_backends = [UndeletedFilter]
    html_email_template = get_template('users/emails/invite.html')
    text_email_template = get_template('users/emails/invite.txt')

    def create(self, request, *args, **kwargs):
        if request.business is None:
            raise validators.ValidationError('Invalid business_id')

        try:
            validate_email(request.data.get('email'))
        except exceptions.ValidationError:
            raise validators.ValidationError('Invalid email address provided')

        try:
            user = User.objects.get(email=request.data.get('email'))
            invite = Invite.objects.create(
                token=get_random_string(64),
                user=user,
                business=self.request.business
            )
            path = reverse('user-invite-accept')
            invite_url = "%s?token=%s" % (
                self.request.build_absolute_uri(path),
                invite.token
            )
            context = {
                'full_name': user.get_full_name(),
                'business_name': self.request.business.name,
                'invite_url': invite_url,
            }

            subject = 'Your %s invite' % (self.request.business.name)

            html_message = self.html_email_template.render(context)
            text_message = self.text_email_template.render(context)

            # todo: offload mail to SQS/Lambda
            send_mail(
                subject,
                text_message,
                None,
                [user.email],
                fail_silently=False,
                html_message=html_message
            )

            return response.Response({'invite_url': invite_url}, status.HTTP_201_CREATED)
        except exceptions.ObjectDoesNotExist:
            raise validators.ValidationError('User does not exist')
        except mail_exceptions.AnymailRequestsAPIError:
            raise validators.ValidationError('Invalid email address')


class InviteView(views.TemplateView):
    template_name = 'users/invites/accept.html'

    def get(self, request, *args, **kwargs):
        token = request.GET.get('token')

        if token is not None:
            invite = Invite.objects.filter(token=token).first()

            if invite is not None:
                business = invite.business
                invite.user.businesses.add(business)
                invite.delete()

                return http.HttpResponseRedirect(reverse('dashboard-business', args=(business.id, business.slug)))

        return super(InviteView, self).get(request, *args, **kwargs)
