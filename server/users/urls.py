from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from users.views import UserViewset
from users.views import InviteViewset
from users.views import InviteView

user_list =  UserViewset.as_view({
    'get': 'list',
    'post': 'create',
})

user_detail =  UserViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

user_activate =  UserViewset.as_view({
    'post': 'activate',
    'delete': 'deactivate',
})

user_invite =  InviteViewset.as_view({
    'post': 'create',
    'put': 'update'
})

user_invite_accept = InviteView.as_view()

urlpatterns = [
    path('api/v1/users/', user_list, name='user-list'),
    path('api/v1/users/<int:pk>/',  user_detail, name='user-detail'),
    path('api/v1/users/<int:pk>/activate/',  user_activate, name='user-activate'),
    path('api/v1/users/invite/',  user_invite, name='user-invite'),

    # template urls
    path('users/invite/accept/',  user_invite_accept, name='user-invite-accept'),
]
