"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import debug_toolbar
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('__debug__/', include(debug_toolbar.urls)),
    path('xqp-admin/', admin.site.urls),


    path('accounts/', include('accounts.urls')),
    path('', include('branches.urls')),
    path('', include('businesses.urls')),
    path('', include('checklists.urls')),
    path('', include('dashboard.urls')),
    path('', include('groups.urls')),
    path('', include('home.urls')),
    path('', include('notifications.urls')),
    path('', include('orders.urls')),
    path('', include('products.urls')),
    path('', include('qauth.urls')),
    path('', include('reports.urls')),
    path('', include('stocksheets.urls')),
    path('', include('suppliers.urls')),
    path('', include('users.urls')),
    path('', include('widgets.urls')),
]
