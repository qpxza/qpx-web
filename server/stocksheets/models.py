from django.db import models
from common.models import TimestampsMixin
from users.models import OwnerMixin
from businesses.models import BusinessMixin
from branches.models import Branch


class Template(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=200, default='')
    label1 = models.CharField(max_length=10, default='C1')
    label2 = models.CharField(max_length=10, default='C2')
    label3 = models.CharField(max_length=10, default='C3')
    label4 = models.CharField(max_length=10, default='C4')
    label5 = models.CharField(max_length=10, default='C5')

class TemplateSection(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=200, default='')
    sequence = models.SmallIntegerField(default=0)
    template = models.ForeignKey(
        Template,
        related_name='sections',
        on_delete=models.CASCADE
    )

class TemplateItem(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=200, default='')
    sequence = models.SmallIntegerField(default=0)
    section = models.ForeignKey(
        TemplateSection,
        related_name='items',
        on_delete=models.CASCADE
    )
    template = models.ForeignKey(
        Template,
        related_name='items',
        on_delete=models.CASCADE
    )

class Sheet(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=200, default='')
    label1 = models.CharField(max_length=10, default='C1')
    label2 = models.CharField(max_length=10, default='C2')
    label3 = models.CharField(max_length=10, default='C3')
    label4 = models.CharField(max_length=10, default='C4')
    label5 = models.CharField(max_length=10, default='C5')
    comment = models.CharField(max_length=255, default='')
    template = models.ForeignKey(
        Template,
        related_name='sheets',
        on_delete=models.CASCADE
    )
    branch = models.ForeignKey(Branch, on_delete=models.CASCADE)

class Section(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=200, default='')
    comment = models.CharField(max_length=255, default='')
    sheet = models.ForeignKey(
        Sheet,
        related_name='sections',
        on_delete=models.CASCADE
    )
    sequence = models.SmallIntegerField(default=0)

    class Meta:
        ordering = ['sequence', 'name']


class Item(TimestampsMixin, OwnerMixin, BusinessMixin):
    name = models.CharField(max_length=100, default='')
    description = models.CharField(max_length=200, default='')
    comment = models.CharField(max_length=255, default='')
    v1 = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    v2 = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    v3 = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    v4 = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    v5 = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    sequence = models.SmallIntegerField(default=0)
    section = models.ForeignKey(
        Section,
        related_name='items',
        on_delete=models.CASCADE
    )
    sheet = models.ForeignKey(
        Sheet,
        related_name='items',
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['sequence', 'name']


