from django.contrib import admin
from django.urls import path, re_path, include
from django.views.generic import RedirectView, TemplateView
from stocksheets.views import SheetViewset
from stocksheets.views import TemplateViewset

sheet_list =  SheetViewset.as_view({
    'get': 'list',
    'post': 'create',
})

sheet_detail =  SheetViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

sheet_template_list =  TemplateViewset.as_view({
    'get': 'list',
    'post': 'create',
})

sheet_template_detail =  TemplateViewset.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy',
})

urlpatterns = [
    path('api/v1/stocksheets/', sheet_list, name='stocksheet-list'),
    path('api/v1/stocksheets/<int:pk>/', sheet_detail, name='stocksheet-detail'),
    path('api/v1/stocksheets/templates/', sheet_template_list, name='stocksheet-template-list'),
    path('api/v1/stocksheets/templates/<int:pk>/', sheet_template_detail, name='stocksheet-template-detail'),
]
