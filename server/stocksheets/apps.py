from django.apps import AppConfig


class StocksheetsConfig(AppConfig):
    name = 'stocksheets'
