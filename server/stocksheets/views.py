from rest_framework import viewsets
from stocksheets.models import Template
from stocksheets.models import Sheet
from stocksheets.serializers import TemplateSerializer
from stocksheets.serializers import SheetSerializer
from stocksheets.serializers import UpdateSheetSerializer
from stocksheets.serializers import ViewSheetSerializer
from common.filters import BelongsToBusinessFilter
from common.filters import UndeletedFilter
from rest_framework import permissions


class TemplatePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('stocksheets.view_template')


class SheetPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.has_perm('stocksheets.view_sheet')


class TemplateViewset(viewsets.ModelViewSet):
    queryset = Template.objects.prefetch_related('sections__items')\
        .filter(deleted_at__isnull=True).order_by('name')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, TemplatePermission]
    serializer_class = TemplateSerializer


class SheetViewset(viewsets.ModelViewSet):
    queryset = Sheet.objects.prefetch_related('branch', 'sections__items')\
        .all().order_by('-created_at')
    filter_backends = [BelongsToBusinessFilter, UndeletedFilter]
    permission_classes = [permissions.IsAuthenticated, SheetPermission]
    serializer_class = SheetSerializer

    def get_serializer_class(self):
        if self.action == 'list':
            return ViewSheetSerializer

        if self.action == 'update':
            return UpdateSheetSerializer

        if self.action == 'retrieve':
            return ViewSheetSerializer

        return SheetSerializer
