from rest_framework import serializers
from rest_framework import fields
from django.db import transaction
from stocksheets.models import Template
from stocksheets.models import TemplateSection
from stocksheets.models import TemplateItem
from stocksheets.models import Sheet
from stocksheets.models import Section
from stocksheets.models import Item
from branches.models import Branch
from branches.serializers import BranchSerializer


class TemplateItemSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100, default='')
    description = fields.CharField(max_length=200, default='')

    class Meta:
        model = TemplateItem
        fields = [
            'id',
            'name',
            'description',
            'sequence',
            'created_at',
            'updated_at',
        ]


class TemplateSectionSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100, default='')
    description = fields.CharField(max_length=200, default='')
    items = TemplateItemSerializer(many=True)

    class Meta:
        model = TemplateSection
        fields = [
            'id',
            'name',
            'description',
            'sequence',
            'items',
            'created_at',
            'updated_at',
        ]


class TemplateSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100, default='')
    description = fields.CharField(max_length=200, default='')
    label1 = fields.CharField(max_length=10, default='C1', allow_blank=True)
    label2 = fields.CharField(max_length=10, default='C2', allow_blank=True)
    label3 = fields.CharField(max_length=10, default='C3', allow_blank=True)
    label4 = fields.CharField(max_length=10, default='C4', allow_blank=True)
    label5 = fields.CharField(max_length=10, default='C5', allow_blank=True)
    sections = TemplateSectionSerializer(many=True)

    class Meta:
        model = Template
        fields = [
            'id',
            'name',
            'description',
            'label1',
            'label2',
            'label3',
            'label4',
            'label5',
            'sections',
            'created_at',
            'updated_at',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        sections = validated_data.pop('sections')

        with transaction.atomic():
            template = Template.objects.create(
                **validated_data, business=business, user=user)

            for i, section in enumerate(sections):
                items = section.pop('items')
                section.update({'sequence': i})
                section = TemplateSection.objects.create(
                    **section,
                    template=template,
                    user=user
                )

                for j, item in enumerate(items):
                    item.update({'sequence': j})

                    TemplateItem.objects.create(
                        **item,
                        section=section,
                        template=template,
                        user=user,
                        business=business
                    )

            return template

    def update(self, template, validated_data):
        user = self.context['request'].user
        sections = validated_data.pop('sections')
        business = self.context['request'].business

        with transaction.atomic():
            template.name = validated_data.get('name')
            template.description = validated_data.get('description')
            template.label1 = validated_data.get('label1')
            template.label2 = validated_data.get('label2')
            template.label3 = validated_data.get('label3')
            template.label4 = validated_data.get('label4')
            template.label5 = validated_data.get('label5')
            template.user = user
            template.save()

            deleted_section_ids = set([s.get('id') for s in sections]) \
                ^ set([s.id for s in template.sections.all()])

            for section in TemplateSection.objects.filter(pk__in=deleted_section_ids).all():
                for item in section.items.all():
                    item.delete()

                section.delete()

            for i, section in enumerate(sections):
                items = section.pop('items')
                section, _ = TemplateSection.objects.update_or_create(
                    pk=section.get('id'),
                    defaults={
                        **section,
                        'sequence': i,
                        'template': template,
                        'user': user,
                        'business': business
                    }
                )

                deleted_item_ids = set([i.get('id') for i in items])\
                    ^ set([i.id for i in section.items.all()])

                for item in TemplateItem.objects.filter(pk__in=deleted_item_ids).all():
                    item.delete()

                for j, item in enumerate(items):
                    TemplateItem.objects.update_or_create(
                        pk=item.get('id'),
                        defaults={
                            **item,
                            'sequence': j,
                            'section': section,
                            'template': template,
                            'user': user,
                            'business': business
                        }
                    )

            return template


class ItemSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100, default='')
    description = fields.CharField(max_length=200, default='')
    comment = fields.CharField(max_length=255, default='', allow_blank=True)
    v1 = fields.DecimalField(max_digits=10, decimal_places=2, default=0)
    v2 = fields.DecimalField(max_digits=10, decimal_places=2, default=0)
    v3 = fields.DecimalField(max_digits=10, decimal_places=2, default=0)
    v4 = fields.DecimalField(max_digits=10, decimal_places=2, default=0)
    v5 = fields.DecimalField(max_digits=10, decimal_places=2, default=0)

    class Meta:
        model = Item
        fields = [
            'id',
            'name',
            'description',
            'comment',
            'v1',
            'v2',
            'v3',
            'v4',
            'v5',
            'created_at',
            'updated_at',
        ]


class SectionSerializer(serializers.ModelSerializer):
    id = fields.IntegerField(required=False)
    name = fields.CharField(max_length=100, default='')
    description = fields.CharField(max_length=200, default='')
    comment = fields.CharField(max_length=255, default='', allow_blank=True)
    items = ItemSerializer(many=True)

    class Meta:
        model = Section
        fields = [
            'id',
            'name',
            'description',
            'comment',
            'sequence',
            'items',
            'created_at',
            'updated_at',
        ]


class SheetSerializer(serializers.ModelSerializer):
    name = fields.CharField(max_length=100, default='')
    description = fields.CharField(max_length=200, default='')
    comment = fields.CharField(max_length=200, default='', allow_blank=True)
    sections = SectionSerializer(many=True)

    class Meta:
        model = Sheet
        fields = [
            'id',
            'name',
            'description',
            'label1',
            'label2',
            'label3',
            'label4',
            'label5',
            'comment',
            'sections',
            'template',
            'branch',
            'created_at',
            'updated_at',
        ]

    def create(self, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business
        sections = validated_data.pop('sections')
        template = validated_data.pop('template')

        if template is None:
            raise validators.ValidationError('Invalid template supplied')

        branch = validated_data.pop('branch')

        if branch is None or branch.business.id != business.id:
            raise validators.ValidationError('Invalid branch provided')

        with transaction.atomic():
            sheet = Sheet.objects.create(
                **validated_data,
                template=template,
                user=user,
                branch=branch,
                business=business
            )

            for i, section in enumerate(sections):
                section.pop('id', None)
                section.update({'sequence': i})
                items = section.pop('items')
                section = Section.objects.create(
                    **section,
                    sheet=sheet,
                    user=user,
                    business=business
                )

                for j, item in enumerate(items):
                    item.pop('id', None)
                    item.update({'sequence': j})
                    Item.objects.create(
                        **item,
                        section=section,
                        sheet=sheet,
                        user=user,
                        business=business
                    )

            return sheet


class UpdateSheetSerializer(SheetSerializer):

    class Meta(SheetSerializer.Meta):
        read_only_fields = ['business']

    def update(self, sheet, validated_data):
        user = self.context['request'].user
        business = self.context['request'].business

        sections = validated_data.pop('sections')
        branch = validated_data.pop('branch')

        if branch is None or branch.business.id != business.id:
            raise validators.ValidationError('Invalid branch provided')

        with transaction.atomic():
            sheet.name = validated_data.get('name')
            sheet.description = validated_data.get('description')
            sheet.label1 = validated_data.get('label1')
            sheet.label2 = validated_data.get('label2')
            sheet.label3 = validated_data.get('label3')
            sheet.label4 = validated_data.get('label4')
            sheet.label5 = validated_data.get('label5')
            sheet.comment = validated_data.get('comment')
            sheet.branch = branch
            sheet.user = user
            sheet.save()

            deleted_section_ids = set([s.get('id') for s in sections]) \
                ^ set([s.id for s in sheet.sections.all()])

            for section in Section.objects.filter(pk__in=deleted_section_ids, business=business).all():
                for item in section.items.all():
                    item.delete()

                section.delete()

            for i, section in enumerate(sections):
                items = section.pop('items')
                section, _ = Section.objects.update_or_create(
                    pk=section.get('id'),
                    defaults={
                        **section,
                        'sequence': i,
                        'sheet': sheet,
                        'user': user,
                        'business': business
                    }
                )

                deleted_item_ids = set([i.get('id') for i in items])\
                    ^ set([i.id for i in section.items.all()])

                for item in Item.objects.filter(pk__in=deleted_item_ids, business=business).all():
                    print("Deleting item %d" % item.id)
                    item.delete()

                for j, item in enumerate(items):
                    Item.objects.update_or_create(
                        pk=item.get('id'),
                        defaults={
                            **item,
                            'sequence': j,
                            'section': section,
                            'sheet': sheet,
                            'user': user,
                            'business': business
                        }
                    )

            return sheet


class ViewSheetSerializer(SheetSerializer):
    branch = BranchSerializer()
