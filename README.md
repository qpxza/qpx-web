#### Important notes on setting up the project

# Deployment checklist

 - activate python virtual environment
 - npm install --dev
 - npm run build:prod
 - pip install using requirements.txt
 - python manage.py migrate
 - pm2 restart


# Server notes

 - ip address 41.76.109.118
 - ssh port 3576 (changes whenever ssh bots find it)
 - project path /var/www/qpx-web/
 - static files path /var/www/qpx-web/front-end/static
 - gunicorn runs the app and it is supervised by pm2. See ecosystem.config.js in the route of the project
 - nginx acts as the gateway. It receives requests and forwards them to our django app
 - ssl/tls certs are all acquired from lets encrypt
